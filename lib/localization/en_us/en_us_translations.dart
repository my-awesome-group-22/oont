final Map<String, String> enUs = {
  // Number verification Screen
  "lbl_resend": "Resend",
  "lbl_verify": "Verify",
  "lbl_verify_now": "VERIFY NOW",
  "msg_did_you_don_t_get": "Did you don’t get code?",
  "msg_entry_your_4_digit": "Entry Your 4 digit code",

  // 16_Light_set your location-permission Screen
  "lbl_allow": "Allow",
  "lbl_allow_maps": "Allow maps",
  "lbl_skip_for_now": "Skip for now",
  "msg_allow_maps_to_access":
      "Allow maps to access your\nlocation whiloe you use the app?",
  "msg_to_access_your_location":
      "to access your\nlocation whiloe you use the app?",

  // 16_Light_set your location Screen
  "msg_london_united_kingdom": "London, United Kingdom",

  // New Item Screen
  "lbl_1_box": "1 Box",
  "lbl_500_gm": "500 Gm",
  "lbl_750_ml": "750 Ml",
  "lbl_800_gm": "800 Gm",
  "lbl_new_items": "New Items",
  "msg_classic_pantz_drypers": "Classic pantz\nDrypers",
  "msg_fresh_sea_tuna_fish": "Fresh Sea Tuna\nFish",
  "msg_perry_s_ice_cream_banana": "Perry’s Ice Cream\nBanana",
  "msg_treat_love_foundation": "Treat Love\nFoundation",
  "msg_vanilla_ice_cream": "Vanilla Ice Cream",

  // Popular Pack Screen
  "lbl_24": "\$24",
  "lbl_38": "\$38",
  "lbl_67": "\$67",
  "lbl_89": "\$89",
  "lbl_big_fruits_pack": "Big Fruits Pack",
  "lbl_big_spices_pack": "Big Spices Pack",
  "lbl_create_own_pack": "Create Own Pack",
  "msg_big_vegetable_pack": "Big Vegetable Pack",
  "msg_small_bundle_pack": "Small Bundle Pack",
  "msg_small_fruits_pack": "Small Fruits Pack",

  // Bundle Product detils Screen
  "lbl_172": "17",
  "lbl_25_kg": "25 Kg",
  "lbl_item_s": "Item’s",
  "lbl_medium": "Medium",
  "lbl_pack_details": "Pack Details",
  "lbl_size": "Size",
  "lbl_tuna_fish": "Tuna Fish",

  // Create My Pack Screen
  "lbl_35_05": "\$35.05",
  "lbl_6": "\$ 6",
  "lbl_7": "\$ 7",
  "lbl_x2": "x2",
  "lbl_x3": "x3",
  "msg_cauliflower_form2": "Cauliflower\nForm The Farmer",
  "msg_fresh_cabbage_bangladeshi": "Fresh Cabbage\nBangladeshi",

  // Create My Pack - Tab Container Screen
  "lbl_create_my_pack": "Create My Pack",
  "lbl_search_product": "Search Product",

  //  Bundle Details Page Screen
  "lbl_12_kg": "12 Kg",
  "lbl_18": "\$18",
  "lbl_22": "\$22",
  "lbl_4_kg": "4 Kg",
  "lbl_5_kg": "5 Kg",
  "lbl_8_kg": "8 Kg",
  "lbl_bundle_details": "Bundle Details",
  "msg_caulifiower_from": "Caulifiower\nFrom The Garden",
  "msg_create_bundle_pack": "Create Bundle Pack",
  "msg_meat_form_the_garden": "Meat\nForm The Garden",
  "msg_papaya_form_the": "Papaya\nForm The Farmer",
  "msg_potato_form_the": "Potato\nForm The Farmers",
  "msg_tomato_form_the": "Tomato Form\nThe Garden",
  "msg_tuna_fish_from_the": "Tuna Fish\nFrom The Sea",

  // Empty Cart Page Screen
  "lbl_start_browsing": "Start Browsing",
  "msg_sorry_you_have": "Sorry, you have no product in your cart.",

  // Checkout Screen
  "lbl_add_new": "Add New",
  "lbl_birmingham": "Birmingham",
  "lbl_home_address": "Home Address",
  "lbl_master_card": "Master Card",
  "lbl_office_address": "Office Address",
  "lbl_pay_now": "Pay Now",
  "lbl_paypal": "PayPal",
  "lbl_uk_london": "UK, London",
  "msg_309_071_9396_939": "(309) 071-9396-939",
  "msg_309_071_9396_9392": "(309)  071-9396-939",
  "msg_cash_on_delivery": "Cash On Delivery",
  "msg_remeber_my_card": "Remeber My Card Details",
  "msg_select_delivery": "Select delivery address",
  "msg_select_payment_system": "Select payment system",

  // Order Successfully Screen
  "lbl_track_order": "Track Order",
  "msg_order_placed_successfully": "Order Placed Successfully ",
  "msg_thanks_for_your":
      "Thanks for your order. Your order has placed\nsuccessfully. Please continue your order.",

  // Sorry Order is Failed  Screen
  "msg_sorry_order_has": "Sorry, Order has Failed",
  "msg_sorry_somethings":
      "Sorry, somethings went wrong.\nPlease try again continue your order",

  // No oreder yet Screen
  "lbl_go_back": "Go Back",
  "lbl_no_orders_yet": "No Orders Yet",
  "lbl_order": "Order",
  "msg_sorry_you_n_t_haven_t": "Sorry you n’t haven’t placed\nany order yet. ",

  // Category Screen
  "lbl_beauty": "Beauty ",
  "lbl_eye_wears": "Eye Wears",
  "lbl_gardening_tools": "Gardening Tools",
  "lbl_gym_equipment": "Gym Equipment",
  "lbl_pack": "Pack",
  "lbl_pet_care": "Pet Care",
  "msg_choose_a_category": "Choose a Category",

  // Vegetable Category Screen
  "lbl_16": "\$16",
  "lbl_vagetables": "Vagetables",
  "msg_cabbage_form_the": "Cabbage\nForm The Garden",
  "msg_cauliflower_from": "Cauliflower\nFrom The Garden",

  // Filter Popup Screen
  "lbl_aci_limited": "ACI Limited",

  // Filter Full Screen
  "lbl_apply_filter": "Apply Filter", "lbl_freshco": "Freshco",
  "lbl_rating_star": "Rating Star",

  // Search result Screen
  "lbl_1_liter": "1 Liter",
  "lbl_fresh_potato": "Fresh Potato",
  "lbl_nutella": "Nutella",
  "lbl_search_results": "Search Results",
  "msg_33_products_found": "33 Products Found",
  "msg_brit_premium_dog": "Brit Premium Dog",
  "msg_grocery_products": "Grocery products",
  "msg_king_s_sunflower_oil": "King’s Sunflower\nOil",
  "msg_perry_s_ice_cream": "Perry’s Ice Cream",
  "msg_tomato_form_the2": "Tomato\nForm The Garden",

  // Home Screen
  "lbl_352": "£35",
  "lbl_london_uk": "London, UK",
  "lbl_new_items2": "New items",
  "lbl_view_all": "View All",
  "msg_brit_premium_dry_dog": "Brit Premium Dry\nDog Food",
  "msg_current_location": "Current Location",
  "msg_girl_guide_biscuits": "Girl Guide\nBiscuits",
  "msg_harpic_toilet_clea": "Harpic Toilet Clea...",

  // Home - Container Screen
  "lbl_favourites": "Favourites", "lbl_home": "Home",

  // Product detils Screen
  "lbl_product_details": "Product Details",
  "lbl_weight_5kg": "Weight: 5kg",
  "msg_cauliflower_bangladeshi": "Cauliflower Bangladeshi",
  "msg_duis_aute_veniam":
      "Duis aute veniam veniam qui aliquip irure duis sint magna occaecat dolore nisi culpa do. Est nisi incididunt aliquip  commodo aliqua tempor.",

  // Cart page Screen
  "lbl_1": "1",
  "lbl_11": "\$11",
  "lbl_5": "5",
  "lbl_9": "9",
  "lbl_add_coupon": "Add Coupon",
  "lbl_apply": "Apply",
  "lbl_cauliflower": "Cauliflower",
  "msg_entry_voucher_code": "Entry Voucher Code",

  // Favorite list Screen
  "lbl_112": "£11",
  "lbl_132": "£13",
  "lbl_152": "£15",
  "lbl_182": "£18",
  "lbl_700_gm": "700 Gm",
  "lbl_arnott_s": "Arnott’s",
  "lbl_caulifiower": "Caulifiower ",
  "lbl_my_favourites": "My Favourites",
  "msg_fresh_cabbage_bangladeshi2": "Fresh Cabbage Bangladeshi",

  // Empty wishlist Page Screen
  "lbl_wishlist_page": "Wishlist Page",

  // Empty Cart Screen
  "lbl_empty_cart": "Empty Cart",
  "msg_your_cart_is_empty": "Your Cart is Empty",

  // Taxi/Menu Screen
  "lbl_10az100": "10az100",
  "lbl_help": "Help",
  "lbl_jeihun": "Jeihun",
  "lbl_payment_method": "Payment Method",
  "lbl_saving_pack": "Saving Pack",
  "lbl_version_4_43_1": "Version 4.43.1",
  "msg_about_the_application": "About the Application",

  // Taxi/Activities Screen
  "lbl_0": "0",
  "lbl_43_hang_bai": "43 Hang Bai",
  "lbl_car_4_seater": "Car 4-seater",
  "lbl_history": "HISTORY",
  "lbl_lorem_ipsum_100": "Lorem ipsum 100",
  "lbl_lorem_ipsum_48": "Lorem ipsum 48",
  "lbl_taxi_4_seater": "Taxi 4-seater",
  "lbl_upcoming": "UPCOMING",
  "msg_10_10_2021_07_03": "10/10/2021  |  07:03",
  "msg_11_10_2021_07_03": "11/10/2021  |  07:03",
  "msg_hai_ba_trung_ha": "Hai Ba Trung, Ha Noi, Viet Nam",
  "msg_hoan_kiem_ha_noi": "Hoan Kiem, Ha Noi, Viet Nam",
  "msg_times_city_urban": "Times City Urban Area",
  "msg_you_don_t_have_any": "You don't have any trips scheduled",

  // Notifications Screen
  "lbl_10_minute_ago": "10 Minute Ago",
  "lbl_15_minute_ago": "15 Minute Ago",
  "lbl_2_hours_ago": "2 Hours Ago",
  "lbl_congratulations": "Congratulations",
  "lbl_coupon_offer": "Coupon Offer",
  "lbl_gifts_offer": "Gifts Offer",
  "lbl_now": "Now",
  "msg_20_off_in_vegetable": "20% Off in Vegetable",
  "msg_great_winter_discount": "Great Winter Discount",
  "msg_hot_deal_buy_one": "Hot Deal Buy one get free one Offer Hery...",
  "msg_you_get_your_order": "You get your order",
  "msg_your_order_cancelled": "Your Order Cancelled",

  // Profile Two Screen
  "lbl_id_1540580": "ID: 1540580",
  "lbl_log_out": "Log Out",
  "lbl_my_profile": "My Profile",
  "lbl_notifications": "Notifications",
  "msg_payment_settings": "Payment Settings",

  // My Oreder Runing List Screen
  "lbl_shipped": "Shipped",

  // My Order Previous List Screen
  "lbl_delivery": "Delivery",

  // My Order Previous List - Tab Container Screen
  "lbl_142": "(14)",
  "lbl_44": "(44)",
  "lbl_58": "(58)",
  "lbl_all": "All",
  "lbl_in_progress": "In progress",
  "lbl_my_orders": "My Orders",
  "lbl_previous": "Previous",

  // Delivery Address Screen
  "lbl_216_c_east_road": "216/c East Road",
  "lbl_8801710071000": "+8801710071000",
  "msg_delivery_address": "Delivery Address",

  // New Addres Screen
  "lbl_303934": "303934",
  "lbl_address_link_1": "Address Link 1",
  "lbl_address_link_2": "Address Link 2",
  "lbl_city": "City",
  "lbl_county": "County",
  "lbl_full_name": "Full Name",
  "lbl_new_address": "New Address",
  "lbl_post_code": "Post Code",
  "lbl_save_address": "Save Address",
  "lbl_times_square": "Times Square",
  "msg_make_default_default": "Make Default Default Shipping Address",

  // Profile Screen
  "lbl_bilal": "Bilal", "lbl_first_name": "First Name",
  "lbl_last_name": "Last Name", "lbl_riaz": "Riaz", "lbl_save": "Save",

  // Notification Screen
  "msg_app_notification": "App Notification",
  "msg_phone_number_notification": "Phone Number Notification",

  // Setting Screen
  "lbl_change_password": "Change Password",
  "msg_deactivate_account": "Deactivate Account",
  "msg_edit_home_address": "Edit Home Address",
  "msg_profile_settings": "Profile Settings",

  // Change Password Screen
  "lbl_update_password": "Update Password",
  "msg_change_passsword": "Change Passsword",
  "msg_current_password": "Current Password",
  "msg_re_taype_password": "Re Taype Password",

  // Change phone Number Screen
  "lbl_8801710000": "+8801710000",
  "msg_change_phone_number": "Change Phone Number",
  "msg_new_phone_number": "New Phone Number",
  "msg_re_type_phone_number": "Re Type Phone Number",
  "msg_update_phone_number": "Update Phone Number",

  // Order Tracking Screen
  "lbl_30_min": "30 Min",
  "lbl_jondy_carlos": "Jondy Carlos",
  "lbl_your_shipper": "Your Shipper",
  "msg_your_delivery_address": "Your Delivery Address",
  "msg_your_delivery_time": "Your Delivery Time",

  // My Order Details Screen
  "lbl_01_00_pm": "01:00 PM",
  "lbl_03_00_pm": "03:00 PM",
  "lbl_05_00_pm": "05:00 Pm",
  "lbl_07_00_pm": "07:00 PM",
  "lbl_120": "\$120",
  "lbl_12_12_2023": "12.12.2023",
  "lbl_13_12_2023": "13.12.2023",
  "lbl_15_12_2023": "15.12.2023",
  "lbl_18_12_2023": "18.12.2023",
  "lbl_3kg": "3KG",
  "lbl_3x": "3x",
  "lbl_50": "\$50",
  "lbl_5x": "5x",
  "lbl_credit_card": "Credit Card",
  "lbl_details": "Details",
  "lbl_order_confirmed": "Order Confirmed",
  "lbl_order_ready": "Order Ready",
  "lbl_paid_from": "Paid From",
  "lbl_product_deatils": "Product Deatils",
  "lbl_total_amount": "Total Amount",
  "msg_fresh_meat_form": "Fresh Meat\nForm The Farmer",
  "msg_fresh_papaya_form": "Fresh Papaya\nForm The Farmer",
  "msg_fresh_tuna_fish_form": "Fresh Tuna Fish\nForm The Sea",
  "msg_order_id_30398505202": "Order id #30398505202",
  "msg_order_processing": "Order Processing",

  // Deliverred Successful Screen
  "msg_15425050_order": "#15425050 order successfully.",
  "msg_congratulations": "Congratulations!",
  "msg_hurrah_we_just":
      "Hurrah!!  We just deliverred your \n#15425050 order successfully.",
  "msg_hurrah_we_just2": "Hurrah!!  We just deliverred your \n",
  "msg_rate_the_product": "Rate The Product",

  // Error Page Screen
  "lbl_404": "404!!!", "msg_oppss_something": "oppss!! something wrong",
  "msg_sorry_something": "Sorry, something went wrong\nplease try again .",

  // Payment  Method One Screen
  "lbl_3_469_52": "\$3.469.52", "lbl_4756": "4756 ", "lbl_9018": "9018",
  "lbl_my_cards": "My Cards", "lbl_payment_options": "Payment Options",

  // Drive method Screen
  "lbl_bisycle": "Bisycle", "lbl_car": "Car", "lbl_motorcycle": "Motorcycle",
  "msg_which_transport": "Which transport method will you use?",

  // Bank details Screen
  "lbl_country": "Country",
  "msg_bank_account_name": "Bank Account name",
  "msg_bank_account_number": "Bank account number",
  "msg_bank_account_number2": "Bank Account number",
  "msg_in_order_to_work":
      "In order to work as a courier, we require you to upload a Driver Licence too. Don't worry, your data will stay safe and private",
  "msg_please_add_your": "Please add your share code",
  "msg_select_your_country": "Select your country",

  // Log In Two Screen
  "lbl_apple": "Apple", "lbl_google": "Google",

  // Login or signup Six Screen
  "lbl_or": "OR", "msg_login_with_email": "Login With Email",

  // Common String
  "lbl": "*******",
  "lbl2": "**********",
  "lbl_10": "\$10",
  "lbl_1000_gm": "1000 GM",
  "lbl_102": "£10",
  "lbl_12": "\$12",
  "lbl_12_25": "\$ 12.25",
  "lbl_13": "\$13",
  "lbl_14": "\$14",
  "lbl_15": "\$15",
  "lbl_17": "\$17",
  "lbl_19": "\$19",
  "lbl_1_kg": "1 Kg",
  "lbl_1_kg2": "1 KG",
  "lbl_1_kg3": "1 kg",
  "lbl_2": "2",
  "lbl_20": "\$20",
  "lbl_200_gm": "200 Gm",
  "lbl_202": "20\$",
  "lbl_2324252627": "2324252627",
  "lbl_25_may": "25 May",
  "lbl_2_kg": "2 Kg",
  "lbl_30": "\$30",
  "lbl_33_kg": "33 Kg",
  "lbl_35": "\$35",
  "lbl_50_32": "50.32",
  "lbl_570_ml": "570 Ml",
  "lbl_62": "6",
  "lbl_70_00": "\$ 70.00",
  "lbl_80": "80\$",
  "lbl_82_25": "\$ 82.25",
  "lbl_88017100000": "+88017100000",
  "lbl_8801710000000": "+8801710000000",
  "lbl_aci_lobon": "ACI Lobon",
  "lbl_activities": "Activities",
  "lbl_any": "Any",
  "lbl_baby_care": "Baby Care",
  "lbl_bilal_riaz": "Bilal Riaz",
  "lbl_brand": "Brand",
  "lbl_browse_home": "Browse Home",
  "lbl_bundle_pack": "Bundle Pack",
  "lbl_buy_now": "Buy Now",
  "lbl_cabbage": "Cabbage",
  "lbl_cancel": "Cancel",
  "lbl_canceled": "Canceled",
  "lbl_cart_page": "Cart Page",
  "lbl_categories": "Categories",
  "lbl_checkout": "Checkout",
  "lbl_chicken": "Chicken",
  "lbl_confirmed": "Confirmed",
  "lbl_continue": "Continue",
  "lbl_delivered": "Delivered",
  "lbl_discount": "Discount",
  "lbl_dog_dry_food": "Dog Dry Food",
  "lbl_done": "Done",
  "lbl_easydelivery": "#easydelivery",
  "lbl_fast_delivery": "Fast Delivery",
  "lbl_filter": "Filter",
  "lbl_forget_password": "Forget Password",
  "lbl_fresh_tomato": "Fresh Tomato",
  "lbl_fruit": "Fruit",
  "lbl_gardening": "Gardening",
  "lbl_gourd": "Gourd",
  "lbl_handwash": "Handwash",
  "lbl_location": "Location",
  "lbl_log_in2": "Log In",
  "lbl_login": "Login",
  "lbl_london": "London",
  "lbl_meat": "Meat",
  "lbl_meat_fish": "Meat & Fish",
  "lbl_medicine": "Medicine",
  "lbl_name": "Name",
  "lbl_new_password": "New Password",
  "lbl_notification": "Notification",
  "lbl_office_supplies": "Office Supplies",
  "lbl_oont_grocery": "OONT Grocery",
  "lbl_oont_transport": "OONT Transport",
  "lbl_oppss": "Oppss!",
  "lbl_oreder_id": "Oreder ID:",
  "lbl_oreo_biscut": "Oreo Biscut",
  "lbl_other": "Other",
  "lbl_papaya": "Papaya",
  "lbl_password": "Password",
  "lbl_phone_number": "Phone Number",
  "lbl_popular_packs": "Popular Packs",
  "lbl_popularity": "Popularity",
  "lbl_potato": "Potato",
  "lbl_price": "Price",
  "lbl_price_range": "Price range",
  "lbl_processing": "Processing",
  "lbl_profile": "Profile",
  "lbl_ready": "Ready",
  "lbl_recent_searches": "Recent Searches",
  "lbl_reset": "Reset",
  "lbl_retype_password": "Retype Password",
  "lbl_review": "Review",
  "lbl_search": "Search",
  "lbl_see_all": "See All",
  "lbl_send_me_link": "Send Me LInk",
  "lbl_settings": "Settings",
  "lbl_sign_up": "Sign up",
  "lbl_sign_up2": "Sign Up",
  "lbl_sort_by": "Sort By",
  "lbl_square": "Square",
  "lbl_start_adding": "Start adding",
  "lbl_status": "Status:",
  "lbl_tomato": "Tomato",
  "lbl_total_item": "Total Item",
  "lbl_total_price": "Total Price",
  "lbl_try_again": "Try Again",
  "lbl_vegetables": "Vegetables",
  "lbl_verified": "Verified!!",
  "lbl_weight": "Weight",
  "lbl_welcome": "Welcome!",
  "lbl_welcome_to_oont": "WELCOME TO OONT",
  "lbl_welcome_to_oont2": "Welcome to\nOONT",
  "msg": "*****************",
  "msg_add_new_password": "Add New password",
  "msg_already_have_account": "Already Have Account?",
  "msg_amazing_discounts": "Amazing Discounts & Offers",
  "msg_browse_different": "Browse different markets",
  "msg_carrot_form_the": "Carrot\nForm The Garden",
  "msg_cauliflower_form": "Cauliflower\nForm The Garden",
  "msg_continue_as_a_customer": "Continue as a Customer",
  "msg_continue_as_a_deliverer": "Continue as a Deliverer",
  "msg_continue_with_email": "Continue with Email or Phone",
  "msg_don_t_have_account": "Don’t Have Account?",
  "msg_forget_password": "Forget Password?",
  "msg_girl_guide_biscuits2": "Girl Guide Biscuits",
  "msg_hurrah_you_have":
      "Hurrah!!  You have successfully \nverified the account.",
  "msg_in_aliquip_aute":
      "In aliquip aute exercitation ut et nisi ut mollit. Deserunt dolor elit pariatur aute .",
  "msg_medium_spice_pack": "Medium Spice Pack",
  "msg_number_is_incorrect": "Number Is Incorrect",
  "msg_onion_oil_salt": "Onion,Oil,Salt...",
  "msg_oont_food_delivery": "OONT Food Delivery",
  "msg_order_your_daily": "Order your\nDaily Groceries",
  "msg_password_is_incorrect": "Password Is Incorrect",
  "msg_please_enter_your":
      "Please enter your number. We will send a code to your phone to reset your password.",
  "msg_raduni_lal_morich": "Raduni Lal Morich",
  "msg_request_a_change": "Request a change",
  "msg_reset_your_password": "Reset your password",
  "msg_sorry_you_have2": "Sorry, you have no product in your wishlist",
  "msg_sulphurfree_bura": "Sulphurfree Bura",

// Network Error String
  "msg_network_err": "Network Error",
  "msg_something_went_wrong": "Something Went Wrong!",

	// Validation Error String
  "err_msg_please_enter_valid_password": "Please enter valid password",
  "err_msg_please_enter_valid_number": "Please enter valid number",
  "err_msg_please_enter_valid_text": "Please enter valid text",
};