import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_ten_screen/models/onboarding_ten_model.dart';
part 'onboarding_ten_state.dart';

final onboardingTenNotifier =
    StateNotifierProvider<OnboardingTenNotifier, OnboardingTenState>(
  (ref) => OnboardingTenNotifier(OnboardingTenState(
    onboardingTenModelObj: OnboardingTenModel(),
  )),
);

/// A notifier that manages the state of a OnboardingTen according to the event that is dispatched to it.
class OnboardingTenNotifier extends StateNotifier<OnboardingTenState> {
  OnboardingTenNotifier(OnboardingTenState state) : super(state) {}
}
