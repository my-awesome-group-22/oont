// ignore_for_file: must_be_immutable

part of 'onboarding_ten_notifier.dart';

/// Represents the state of OnboardingTen in the application.
class OnboardingTenState extends Equatable {
  OnboardingTenState({this.onboardingTenModelObj});

  OnboardingTenModel? onboardingTenModelObj;

  @override
  List<Object?> get props => [
        onboardingTenModelObj,
      ];

  OnboardingTenState copyWith({OnboardingTenModel? onboardingTenModelObj}) {
    return OnboardingTenState(
      onboardingTenModelObj:
          onboardingTenModelObj ?? this.onboardingTenModelObj,
    );
  }
}
