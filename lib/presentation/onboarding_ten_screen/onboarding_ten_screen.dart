import 'notifier/onboarding_ten_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

class OnboardingTenScreen extends ConsumerStatefulWidget {
  const OnboardingTenScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingTenScreenState createState() => OnboardingTenScreenState();
}

class OnboardingTenScreenState extends ConsumerState<OnboardingTenScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 22.h,
            vertical: 60.v,
          ),
          child: Column(
            children: [
              SizedBox(height: 18.v),
              _buildWidgetStack(context),
              SizedBox(height: 80.v),
              Text(
                "msg_amazing_discounts".tr,
                style: CustomTextStyles.headlineSmall25,
              ),
              SizedBox(height: 24.v),
              Opacity(
                opacity: 0.6,
                child: Container(
                  width: 323.h,
                  margin: EdgeInsets.symmetric(horizontal: 23.h),
                  child: Text(
                    "msg_in_aliquip_aute".tr,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: CustomTextStyles.titleMediumGray900_2.copyWith(
                      height: 1.40,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 62.v),
              SizedBox(
                height: 88.adaptSize,
                width: 88.adaptSize,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img100,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerRight,
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: SizedBox(
                                        height: 31.v,
                                        width: 43.h,
                                        child: CircularProgressIndicator(
                                          value: 0.5,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildSevenHundredFiftyStack(
                                context,
                                dynamicImage1: ImageConstant.img250Amber700,
                                dynamicImage2:
                                    ImageConstant.imgItemsOutlineAmber700,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _buildSevenHundredFiftyStack(
                                context,
                                dynamicImage1: ImageConstant.img750Amber700,
                                dynamicImage2: ImageConstant.img625Amber700,
                              ),
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.topCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img500Amber700,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerLeft,
                                    ),
                                    CustomImageView(
                                      imagePath: ImageConstant.img250Amber700,
                                      height: 31.v,
                                      width: 43.h,
                                      alignment: Alignment.topCenter,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    CustomIconButton(
                      height: 80.adaptSize,
                      width: 80.adaptSize,
                      alignment: Alignment.center,
                      child: CustomImageView(
                        imagePath: ImageConstant.imgGroup317,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildWidgetStack(BuildContext context) {
    return SizedBox(
      height: 389.v,
      width: 360.h,
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          CustomImageView(
            imagePath: ImageConstant.imgGroup,
            height: 383.v,
            width: 352.h,
            alignment: Alignment.center,
          ),
          CustomImageView(
            imagePath: ImageConstant.imgGroupOrange100,
            height: 116.v,
            width: 134.h,
            alignment: Alignment.topRight,
          ),
        ],
      ),
    );
  }

  /// Common widget
  Widget _buildSevenHundredFiftyStack(
    BuildContext context, {
    required String dynamicImage1,
    required String dynamicImage2,
  }) {
    return SizedBox(
      height: 44.v,
      width: 43.h,
      child: Stack(
        alignment: Alignment.centerRight,
        children: [
          CustomImageView(
            imagePath: dynamicImage1,
            height: 31.v,
            width: 43.h,
            alignment: Alignment.topCenter,
          ),
          CustomImageView(
            imagePath: dynamicImage2,
            height: 44.v,
            width: 31.h,
            alignment: Alignment.centerRight,
          ),
        ],
      ),
    );
  }
}
