import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/k76_bottomsheet/models/k76_model.dart';
part 'k76_state.dart';

final k76Notifier = StateNotifierProvider<K76Notifier, K76State>(
  (ref) => K76Notifier(K76State(
    k76ModelObj: K76Model(),
  )),
);

/// A notifier that manages the state of a K76 according to the event that is dispatched to it.
class K76Notifier extends StateNotifier<K76State> {
  K76Notifier(K76State state) : super(state) {}
}
