// ignore_for_file: must_be_immutable

part of 'k76_notifier.dart';

/// Represents the state of K76 in the application.
class K76State extends Equatable {
  K76State({this.k76ModelObj});

  K76Model? k76ModelObj;

  @override
  List<Object?> get props => [
        k76ModelObj,
      ];

  K76State copyWith({K76Model? k76ModelObj}) {
    return K76State(
      k76ModelObj: k76ModelObj ?? this.k76ModelObj,
    );
  }
}
