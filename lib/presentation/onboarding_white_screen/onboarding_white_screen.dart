import 'notifier/onboarding_white_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingWhiteScreen extends ConsumerStatefulWidget {
  const OnboardingWhiteScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingWhiteScreenState createState() => OnboardingWhiteScreenState();
}

class OnboardingWhiteScreenState extends ConsumerState<OnboardingWhiteScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOontPrimary85x288,
                height: 85.v,
                width: 288.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
