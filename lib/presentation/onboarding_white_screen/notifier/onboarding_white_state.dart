// ignore_for_file: must_be_immutable

part of 'onboarding_white_notifier.dart';

/// Represents the state of OnboardingWhite in the application.
class OnboardingWhiteState extends Equatable {
  OnboardingWhiteState({this.onboardingWhiteModelObj});

  OnboardingWhiteModel? onboardingWhiteModelObj;

  @override
  List<Object?> get props => [
        onboardingWhiteModelObj,
      ];

  OnboardingWhiteState copyWith(
      {OnboardingWhiteModel? onboardingWhiteModelObj}) {
    return OnboardingWhiteState(
      onboardingWhiteModelObj:
          onboardingWhiteModelObj ?? this.onboardingWhiteModelObj,
    );
  }
}
