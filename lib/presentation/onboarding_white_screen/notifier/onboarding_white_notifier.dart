import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_white_screen/models/onboarding_white_model.dart';
part 'onboarding_white_state.dart';

final onboardingWhiteNotifier =
    StateNotifierProvider<OnboardingWhiteNotifier, OnboardingWhiteState>(
  (ref) => OnboardingWhiteNotifier(OnboardingWhiteState(
    onboardingWhiteModelObj: OnboardingWhiteModel(),
  )),
);

/// A notifier that manages the state of a OnboardingWhite according to the event that is dispatched to it.
class OnboardingWhiteNotifier extends StateNotifier<OnboardingWhiteState> {
  OnboardingWhiteNotifier(OnboardingWhiteState state) : super(state) {}
}
