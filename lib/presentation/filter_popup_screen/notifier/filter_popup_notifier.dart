import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/officesupplies_item_model.dart';
import 'package:oont/presentation/filter_popup_screen/models/filter_popup_model.dart';
part 'filter_popup_state.dart';

final filterPopupNotifier =
    StateNotifierProvider<FilterPopupNotifier, FilterPopupState>(
  (ref) => FilterPopupNotifier(FilterPopupState(
    searchController: TextEditingController(),
    searchController1: TextEditingController(),
    topleftController: TextEditingController(),
    aCILobonvalueController: TextEditingController(),
    meatvalueController: TextEditingController(),
    handwashvalueController: TextEditingController(),
    dogDryFoodvalueController: TextEditingController(),
    chickenvalueController: TextEditingController(),
    fruitvalueController: TextEditingController(),
    squarevalueController: TextEditingController(),
    filterPopupModelObj: FilterPopupModel(
        officesuppliesItemList:
            List.generate(4, (index) => OfficesuppliesItemModel())),
  )),
);

/// A notifier that manages the state of a FilterPopup according to the event that is dispatched to it.
class FilterPopupNotifier extends StateNotifier<FilterPopupState> {
  FilterPopupNotifier(FilterPopupState state) : super(state) {}

  void onSelectedChipView1(
    int index,
    bool value,
  ) {
    List<OfficesuppliesItemModel> newList = List<OfficesuppliesItemModel>.from(
        state.filterPopupModelObj!.officesuppliesItemList);
    newList[index] = newList[index].copyWith(isSelected: value);
    state = state.copyWith(
        filterPopupModelObj: state.filterPopupModelObj
            ?.copyWith(officesuppliesItemList: newList));
  }
}
