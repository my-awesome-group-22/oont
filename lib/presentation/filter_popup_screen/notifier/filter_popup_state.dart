// ignore_for_file: must_be_immutable

part of 'filter_popup_notifier.dart';

/// Represents the state of FilterPopup in the application.
class FilterPopupState extends Equatable {
  FilterPopupState({
    this.searchController,
    this.searchController1,
    this.topleftController,
    this.aCILobonvalueController,
    this.meatvalueController,
    this.handwashvalueController,
    this.dogDryFoodvalueController,
    this.chickenvalueController,
    this.fruitvalueController,
    this.squarevalueController,
    this.filterPopupModelObj,
  });

  TextEditingController? searchController;

  TextEditingController? searchController1;

  TextEditingController? topleftController;

  TextEditingController? aCILobonvalueController;

  TextEditingController? meatvalueController;

  TextEditingController? handwashvalueController;

  TextEditingController? dogDryFoodvalueController;

  TextEditingController? chickenvalueController;

  TextEditingController? fruitvalueController;

  TextEditingController? squarevalueController;

  FilterPopupModel? filterPopupModelObj;

  @override
  List<Object?> get props => [
        searchController,
        searchController1,
        topleftController,
        aCILobonvalueController,
        meatvalueController,
        handwashvalueController,
        dogDryFoodvalueController,
        chickenvalueController,
        fruitvalueController,
        squarevalueController,
        filterPopupModelObj,
      ];

  FilterPopupState copyWith({
    TextEditingController? searchController,
    TextEditingController? searchController1,
    TextEditingController? topleftController,
    TextEditingController? aCILobonvalueController,
    TextEditingController? meatvalueController,
    TextEditingController? handwashvalueController,
    TextEditingController? dogDryFoodvalueController,
    TextEditingController? chickenvalueController,
    TextEditingController? fruitvalueController,
    TextEditingController? squarevalueController,
    FilterPopupModel? filterPopupModelObj,
  }) {
    return FilterPopupState(
      searchController: searchController ?? this.searchController,
      searchController1: searchController1 ?? this.searchController1,
      topleftController: topleftController ?? this.topleftController,
      aCILobonvalueController:
          aCILobonvalueController ?? this.aCILobonvalueController,
      meatvalueController: meatvalueController ?? this.meatvalueController,
      handwashvalueController:
          handwashvalueController ?? this.handwashvalueController,
      dogDryFoodvalueController:
          dogDryFoodvalueController ?? this.dogDryFoodvalueController,
      chickenvalueController:
          chickenvalueController ?? this.chickenvalueController,
      fruitvalueController: fruitvalueController ?? this.fruitvalueController,
      squarevalueController:
          squarevalueController ?? this.squarevalueController,
      filterPopupModelObj: filterPopupModelObj ?? this.filterPopupModelObj,
    );
  }
}
