import '../filter_popup_screen/widgets/officesupplies_item_widget.dart';
import 'models/officesupplies_item_model.dart';
import 'notifier/filter_popup_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';
import 'package:oont/widgets/custom_outlined_button.dart';
import 'package:oont/widgets/custom_text_form_field.dart';

class FilterPopupScreen extends ConsumerStatefulWidget {
  const FilterPopupScreen({Key? key})
      : super(
          key: key,
        );

  @override
  FilterPopupScreenState createState() => FilterPopupScreenState();
}

class FilterPopupScreenState extends ConsumerState<FilterPopupScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          width: double.maxFinite,
          child: SingleChildScrollView(
            child: SizedBox(
              height: 852.v,
              width: double.maxFinite,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12.h,
                        vertical: 17.v,
                      ),
                      decoration: AppDecoration.fillBlack,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          _buildSearchRow(context),
                          SizedBox(height: 27.v),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "lbl_recent_searches".tr,
                              style: CustomTextStyles.titleMedium18,
                            ),
                          ),
                          SizedBox(height: 24.v),
                          _buildSearch1(context),
                          SizedBox(height: 11.v),
                          _buildTopleft(context),
                          SizedBox(height: 11.v),
                          _buildACILobonvalue(context),
                          SizedBox(height: 11.v),
                          _buildMeatvalue(context),
                          SizedBox(height: 11.v),
                          _buildHandwashvalue(context),
                          SizedBox(height: 13.v),
                          _buildDogDryFoodvalue(context),
                          SizedBox(height: 11.v),
                          _buildChickenvalue(context),
                          SizedBox(height: 11.v),
                          _buildFruitvalue(context),
                          SizedBox(height: 11.v),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      height: 609.v,
                      width: double.maxFinite,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: 533.v,
                              width: double.maxFinite,
                              decoration: BoxDecoration(
                                color: appTheme.whiteA700,
                                borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(30.h),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.h),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SizedBox(
                                    width: 50.h,
                                    child: Divider(
                                      color: appTheme.gray900,
                                    ),
                                  ),
                                  SizedBox(height: 26.v),
                                  _buildArrowRightRow(context),
                                  SizedBox(height: 28.v),
                                  _buildSortByRow(context),
                                  SizedBox(height: 32.v),
                                  _buildPriceRangeColumn(context),
                                  SizedBox(height: 32.v),
                                  _buildFilterButtomCategoriesColumn(context),
                                  SizedBox(height: 21.v),
                                  _buildAny(context),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildSearch(BuildContext context) {
    return Expanded(
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).searchController,
            hintText: "lbl_search".tr,
            hintStyle: CustomTextStyles.bodyLargeGray900_2,
            prefix: Container(
              margin: EdgeInsets.fromLTRB(15.h, 13.v, 10.h, 13.v),
              child: CustomImageView(
                imagePath: ImageConstant.imgRewind,
                height: 24.adaptSize,
                width: 24.adaptSize,
              ),
            ),
            prefixConstraints: BoxConstraints(
              maxHeight: 50.v,
            ),
            suffix: Container(
              margin: EdgeInsets.fromLTRB(30.h, 13.v, 13.h, 13.v),
              child: CustomImageView(
                imagePath: ImageConstant.imgTelevisionWhiteA70024x24,
                height: 24.adaptSize,
                width: 24.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 50.v,
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 16.v),
            borderDecoration: TextFormFieldStyleHelper.fillGray,
            filled: true,
            fillColor: appTheme.gray100,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildSearchRow(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 7.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildSearch(context),
          Padding(
            padding: EdgeInsets.only(
              left: 19.h,
              top: 13.v,
              bottom: 19.v,
            ),
            child: Text(
              "lbl_cancel".tr,
              style: CustomTextStyles.bodyLargeGray900_2,
            ),
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildSearch1(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).searchController1,
            hintText: "lbl_vegetables".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 2.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 30.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildTopleft(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).topleftController,
            hintText: "msg_raduni_lal_morich".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildACILobonvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).aCILobonvalueController,
            hintText: "lbl_aci_lobon".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildMeatvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).meatvalueController,
            hintText: "lbl_meat".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildHandwashvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).handwashvalueController,
            hintText: "lbl_handwash".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildDogDryFoodvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller:
                ref.watch(filterPopupNotifier).dogDryFoodvalueController,
            hintText: "lbl_dog_dry_food".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 2.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 30.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildChickenvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).chickenvalueController,
            hintText: "lbl_chicken".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildFruitvalue(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.h),
      child: Consumer(
        builder: (context, ref, _) {
          return CustomTextFormField(
            controller: ref.watch(filterPopupNotifier).fruitvalueController,
            hintText: "lbl_fruit".tr,
            hintStyle: theme.textTheme.bodyLarge!,
            suffix: Container(
              margin: EdgeInsets.only(
                left: 30.h,
                top: 4.v,
                bottom: 13.v,
              ),
              child: CustomImageView(
                imagePath: ImageConstant.imgTopLeft,
                height: 15.adaptSize,
                width: 15.adaptSize,
              ),
            ),
            suffixConstraints: BoxConstraints(
              maxHeight: 32.v,
            ),
            borderDecoration: TextFormFieldStyleHelper.underLineGray,
          );
        },
      ),
    );
  }

  /// Section Widget
  Widget _buildArrowRightRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomIconButton(
          height: 40.adaptSize,
          width: 40.adaptSize,
          padding: EdgeInsets.all(10.h),
          decoration: IconButtonStyleHelper.fillGray,
          child: CustomImageView(
            imagePath: ImageConstant.imgArrowRight,
          ),
        ),
        Spacer(
          flex: 53,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 6.v,
            bottom: 14.v,
          ),
          child: Text(
            "lbl_filter".tr,
            style: CustomTextStyles.titleMediumBold,
          ),
        ),
        Spacer(
          flex: 46,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 6.v,
            bottom: 14.v,
          ),
          child: Text(
            "lbl_reset".tr,
            style: CustomTextStyles.titleMedium18,
          ),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildSortByRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "lbl_sort_by".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        Spacer(),
        Text(
          "lbl_popularity".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        CustomImageView(
          imagePath: ImageConstant.imgArrowDown,
          height: 15.adaptSize,
          width: 15.adaptSize,
          margin: EdgeInsets.only(left: 2.h),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildPriceRangeColumn(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_price_range".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 20.v),
        SliderTheme(
          data: SliderThemeData(
            trackShape: RoundedRectSliderTrackShape(),
            activeTrackColor: theme.colorScheme.primary,
            inactiveTrackColor: appTheme.gray300,
            thumbColor: theme.colorScheme.primary,
            thumbShape: RoundSliderThumbShape(),
          ),
          child: RangeSlider(
            values: RangeValues(
              0,
              0,
            ),
            min: 0.0,
            max: 100.0,
            onChanged: (value) {},
          ),
        ),
        SizedBox(height: 6.v),
        Padding(
          padding: EdgeInsets.only(left: 17.h),
          child: Row(
            children: [
              Text(
                "lbl_202".tr,
                style: CustomTextStyles.titleSmallGray900_1,
              ),
              Padding(
                padding: EdgeInsets.only(left: 149.h),
                child: Text(
                  "lbl_80".tr,
                  style: CustomTextStyles.titleSmallGray900_1,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildFilterButtomCategoriesColumn(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_categories".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 23.v),
        Consumer(
          builder: (context, ref, _) {
            return Wrap(
              runSpacing: 13.v,
              spacing: 13.h,
              children: List<Widget>.generate(
                ref
                        .watch(filterPopupNotifier)
                        .filterPopupModelObj
                        ?.officesuppliesItemList
                        .length ??
                    0,
                (index) {
                  OfficesuppliesItemModel model = ref
                          .watch(filterPopupNotifier)
                          .filterPopupModelObj
                          ?.officesuppliesItemList[index] ??
                      OfficesuppliesItemModel();

                  return OfficesuppliesItemWidget(
                    model,
                    onSelectedChipView1: (value) {
                      ref
                          .read(filterPopupNotifier.notifier)
                          .onSelectedChipView1(index, value);
                    },
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildAnyButton(BuildContext context) {
    return Expanded(
      child: CustomOutlinedButton(
        height: 45.v,
        text: "lbl_any".tr,
        margin: EdgeInsets.only(right: 6.h),
      ),
    );
  }

  /// Section Widget
  Widget _buildSquarevalue(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 6.h),
        child: Consumer(
          builder: (context, ref, _) {
            return CustomTextFormField(
              controller: ref.watch(filterPopupNotifier).squarevalueController,
              hintText: "lbl_square".tr,
              hintStyle: theme.textTheme.titleSmall!,
              textInputAction: TextInputAction.done,
            );
          },
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildAciLimitedButton(BuildContext context) {
    return Expanded(
      child: CustomOutlinedButton(
        height: 45.v,
        text: "lbl_aci_limited".tr,
        margin: EdgeInsets.only(left: 6.h),
      ),
    );
  }

  /// Section Widget
  Widget _buildSeeAllButton(BuildContext context) {
    return CustomOutlinedButton(
      height: 45.v,
      width: 116.h,
      text: "lbl_see_all".tr,
      buttonStyle: CustomButtonStyles.outlinePrimaryTL8,
      buttonTextStyle: theme.textTheme.titleSmall!,
    );
  }

  /// Section Widget
  Widget _buildAny(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_brand".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 31.v),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildAnyButton(context),
            _buildSquarevalue(context),
            _buildAciLimitedButton(context),
          ],
        ),
        SizedBox(height: 15.v),
        _buildSeeAllButton(context),
      ],
    );
  }
}
