// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'officesupplies_item_model.dart';/// This class defines the variables used in the [filter_popup_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class FilterPopupModel extends Equatable {FilterPopupModel({this.officesuppliesItemList = const []}) {  }

List<OfficesuppliesItemModel> officesuppliesItemList;

FilterPopupModel copyWith({List<OfficesuppliesItemModel>? officesuppliesItemList}) { return FilterPopupModel(
officesuppliesItemList : officesuppliesItemList ?? this.officesuppliesItemList,
); } 
@override List<Object?> get props => [officesuppliesItemList];
 }
