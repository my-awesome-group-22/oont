// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class is used in the [officesupplies_item_widget] screen.
class OfficesuppliesItemModel extends Equatable {OfficesuppliesItemModel({this.officeSupplies, this.isSelected, }) { officeSupplies = officeSupplies  ?? "Office Supplies";isSelected = isSelected  ?? false; }

String? officeSupplies;

bool? isSelected;

OfficesuppliesItemModel copyWith({String? officeSupplies, bool? isSelected, }) { return OfficesuppliesItemModel(
officeSupplies : officeSupplies ?? this.officeSupplies,
isSelected : isSelected ?? this.isSelected,
); } 
@override List<Object?> get props => [officeSupplies,isSelected];
 }
