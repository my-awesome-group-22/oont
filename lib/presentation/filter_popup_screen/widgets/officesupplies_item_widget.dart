import '../models/officesupplies_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class OfficesuppliesItemWidget extends StatelessWidget {
  OfficesuppliesItemWidget(
    this.officesuppliesItemModelObj, {
    Key? key,
    this.onSelectedChipView1,
  }) : super(
          key: key,
        );

  OfficesuppliesItemModel officesuppliesItemModelObj;

  Function(bool)? onSelectedChipView1;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        canvasColor: Colors.transparent,
      ),
      child: RawChip(
        padding: EdgeInsets.symmetric(
          horizontal: 5.h,
          vertical: 15.v,
        ),
        showCheckmark: false,
        labelPadding: EdgeInsets.zero,
        label: Text(
          officesuppliesItemModelObj.officeSupplies!,
          style: TextStyle(
            color: (officesuppliesItemModelObj.isSelected ?? false)
                ? appTheme.whiteA700
                : theme.colorScheme.onError.withOpacity(1),
            fontSize: 14.fSize,
            fontFamily: 'Epilogue',
            fontWeight: FontWeight.w500,
          ),
        ),
        selected: (officesuppliesItemModelObj.isSelected ?? false),
        backgroundColor: Colors.transparent,
        selectedColor: theme.colorScheme.primary,
        shape: (officesuppliesItemModelObj.isSelected ?? false)
            ? RoundedRectangleBorder(
                side: BorderSide(
                  color: theme.colorScheme.primary,
                  width: 1.h,
                ),
                borderRadius: BorderRadius.circular(
                  8.h,
                ),
              )
            : RoundedRectangleBorder(
                side: BorderSide(
                  color: theme.colorScheme.primary,
                  width: 1.h,
                ),
                borderRadius: BorderRadius.circular(
                  8.h,
                ),
              ),
        onSelected: (value) {
          onSelectedChipView1?.call(value);
        },
      ),
    );
  }
}
