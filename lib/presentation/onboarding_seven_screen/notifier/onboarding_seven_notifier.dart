import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_seven_screen/models/onboarding_seven_model.dart';
part 'onboarding_seven_state.dart';

final onboardingSevenNotifier =
    StateNotifierProvider<OnboardingSevenNotifier, OnboardingSevenState>(
  (ref) => OnboardingSevenNotifier(OnboardingSevenState(
    onboardingSevenModelObj: OnboardingSevenModel(),
  )),
);

/// A notifier that manages the state of a OnboardingSeven according to the event that is dispatched to it.
class OnboardingSevenNotifier extends StateNotifier<OnboardingSevenState> {
  OnboardingSevenNotifier(OnboardingSevenState state) : super(state) {}
}
