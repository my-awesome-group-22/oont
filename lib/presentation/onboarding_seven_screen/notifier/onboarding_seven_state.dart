// ignore_for_file: must_be_immutable

part of 'onboarding_seven_notifier.dart';

/// Represents the state of OnboardingSeven in the application.
class OnboardingSevenState extends Equatable {
  OnboardingSevenState({this.onboardingSevenModelObj});

  OnboardingSevenModel? onboardingSevenModelObj;

  @override
  List<Object?> get props => [
        onboardingSevenModelObj,
      ];

  OnboardingSevenState copyWith(
      {OnboardingSevenModel? onboardingSevenModelObj}) {
    return OnboardingSevenState(
      onboardingSevenModelObj:
          onboardingSevenModelObj ?? this.onboardingSevenModelObj,
    );
  }
}
