import 'notifier/onboarding_seven_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

class OnboardingSevenScreen extends ConsumerStatefulWidget {
  const OnboardingSevenScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingSevenScreenState createState() => OnboardingSevenScreenState();
}

class OnboardingSevenScreenState extends ConsumerState<OnboardingSevenScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 23.h,
            vertical: 60.v,
          ),
          child: Column(
            children: [
              SizedBox(height: 57.v),
              CustomImageView(
                imagePath: ImageConstant.imgGroupGray30001,
                height: 350.v,
                width: 364.h,
              ),
              SizedBox(height: 79.v),
              Text(
                "lbl_fast_delivery".tr,
                style: CustomTextStyles.headlineSmall25,
              ),
              SizedBox(height: 24.v),
              Opacity(
                opacity: 0.5,
                child: Container(
                  width: 323.h,
                  margin: EdgeInsets.symmetric(horizontal: 21.h),
                  child: Text(
                    "msg_in_aliquip_aute".tr,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: CustomTextStyles.titleMediumGray900_1.copyWith(
                      height: 1.40,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 62.v),
              SizedBox(
                height: 88.adaptSize,
                width: 88.adaptSize,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img100Amber700,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerRight,
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: SizedBox(
                                        height: 31.v,
                                        width: 43.h,
                                        child: CircularProgressIndicator(
                                          value: 0.5,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildOneHundred(
                                context,
                                image1: ImageConstant.img250Amber700,
                                image2: ImageConstant.imgItemsOutlineAmber700,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _buildOneHundred(
                                context,
                                image1: ImageConstant.img750Amber700,
                                image2: ImageConstant.img625Amber700,
                              ),
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.topCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img500Amber700,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerLeft,
                                    ),
                                    CustomImageView(
                                      imagePath: ImageConstant.img250Amber700,
                                      height: 31.v,
                                      width: 43.h,
                                      alignment: Alignment.topCenter,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    CustomIconButton(
                      height: 80.adaptSize,
                      width: 80.adaptSize,
                      alignment: Alignment.center,
                      child: CustomImageView(
                        imagePath: ImageConstant.imgGroup317,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Common widget
  Widget _buildOneHundred(
    BuildContext context, {
    required String image1,
    required String image2,
  }) {
    return SizedBox(
      height: 44.v,
      width: 43.h,
      child: Stack(
        alignment: Alignment.centerRight,
        children: [
          CustomImageView(
            imagePath: image1,
            height: 31.v,
            width: 43.h,
            alignment: Alignment.topCenter,
          ),
          CustomImageView(
            imagePath: image2,
            height: 44.v,
            width: 31.h,
            alignment: Alignment.centerRight,
          ),
        ],
      ),
    );
  }
}
