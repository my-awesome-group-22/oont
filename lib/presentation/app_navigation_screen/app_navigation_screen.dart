import 'notifier/app_navigation_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class AppNavigationScreen extends ConsumerStatefulWidget {
  const AppNavigationScreen({Key? key})
      : super(
          key: key,
        );

  @override
  AppNavigationScreenState createState() => AppNavigationScreenState();
}

class AppNavigationScreenState extends ConsumerState<AppNavigationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0XFFFFFFFF),
        body: SizedBox(
          width: 375.h,
          child: Column(
            children: [
              _buildAppNavigation(context),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0XFFFFFFFF),
                    ),
                    child: Column(
                      children: [
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup Three".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupThreeScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Two".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingTwoScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Three".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingThreeScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Four".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingFourScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.loginOrSignupScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup Two".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupTwoScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup One".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Log In".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.logInScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Sign up".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.signUpScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Number verification".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.numberVerificationScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Log In One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.logInOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Error Page Password".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.errorPagePasswordScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Error Page Phone Number".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.errorPagePhoneNumberScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Forget Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.forgetPasswordScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "New Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.newPasswordScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "New Item".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.newItemScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Popular Pack".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.popularPackScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Bundle Product detils".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.bundleProductDetilsScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Create My Pack - Tab Container".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.createMyPackTabContainerScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: " Bundle Details Page".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.bundleDetailsPageScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Empty Cart Page".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.emptyCartPageScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Checkout".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.checkoutScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Order Successfully".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.orderSuccessfullyScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Sorry Order is Failed ".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.sorryOrderIsFailedScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "No oreder yet".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.noOrederYetScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Vegetable Category".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.vegetableCategoryScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Search One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.searchOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Search".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.searchScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Filter Popup".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.filterPopupScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Search result".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.searchResultScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Home - Container".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.homeContainerScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Product detils".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.productDetilsScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Cart page".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.cartPageScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Empty wishlist Page".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.emptyWishlistPageScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Empty Cart".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.emptyCartScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Taxi/Activities".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.taxiActivitiesScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Notifications".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.notificationsScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle:
                              "My Order Previous List - Tab Container".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.myOrderPreviousListTabContainerScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Delivery Address".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.deliveryAddressScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "New Addres".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.newAddresScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Profile".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.profileScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Notification".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.notificationScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Setting".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.settingScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Change Password".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.changePasswordScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Change phone Number".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.changePhoneNumberScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "My Order Details".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.myOrderDetailsScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Error Page".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.errorPageScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Payment  Method One".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.paymentMethodOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Five".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingFiveScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Nine".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingNineScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Drive method".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.driveMethodScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Bank details".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.bankDetailsScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Eight".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingEightScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding-white".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingWhiteScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Six".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingSixScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Ten".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingTenScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Onboarding Seven".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.onboardingSevenScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup Four".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupFourScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup Five".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupFiveScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Log In Two".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.logInTwoScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Login or signup Six".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.loginOrSignupSixScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Sign up One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.signUpOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Log In Three".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.logInThreeScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Error Page Password One".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.errorPagePasswordOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Error Page Phone Number One".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.errorPagePhoneNumberOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "Forget Password One".tr,
                          onTapScreenTitle: () => onTapScreenTitle(
                              AppRoutes.forgetPasswordOneScreen),
                        ),
                        _buildScreenTitle(
                          context,
                          screenTitle: "New Password One".tr,
                          onTapScreenTitle: () =>
                              onTapScreenTitle(AppRoutes.newPasswordOneScreen),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildAppNavigation(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0XFFFFFFFF),
      ),
      child: Column(
        children: [
          SizedBox(height: 10.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.h),
              child: Text(
                "App Navigation".tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0XFF000000),
                  fontSize: 20.fSize,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          SizedBox(height: 10.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 20.h),
              child: Text(
                "Check your app's UI from the below demo screens of your app."
                    .tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0XFF888888),
                  fontSize: 16.fSize,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          SizedBox(height: 5.v),
          Divider(
            height: 1.v,
            thickness: 1.v,
            color: Color(0XFF000000),
          ),
        ],
      ),
    );
  }

  /// Common widget
  Widget _buildScreenTitle(
    BuildContext context, {
    required String screenTitle,
    Function? onTapScreenTitle,
  }) {
    return GestureDetector(
      onTap: () {
        onTapScreenTitle!.call();
      },
      child: Container(
        decoration: BoxDecoration(
          color: Color(0XFFFFFFFF),
        ),
        child: Column(
          children: [
            SizedBox(height: 10.v),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.h),
                child: Text(
                  screenTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0XFF000000),
                    fontSize: 20.fSize,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.v),
            SizedBox(height: 5.v),
            Divider(
              height: 1.v,
              thickness: 1.v,
              color: Color(0XFF888888),
            ),
          ],
        ),
      ),
    );
  }

  /// Common click event
  void onTapScreenTitle(String routeName) {
    NavigatorService.pushNamed(routeName);
  }
}
