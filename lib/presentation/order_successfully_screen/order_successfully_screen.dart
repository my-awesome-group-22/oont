import 'notifier/order_successfully_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class OrderSuccessfullyScreen extends ConsumerStatefulWidget {
  const OrderSuccessfullyScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OrderSuccessfullyScreenState createState() => OrderSuccessfullyScreenState();
}

class OrderSuccessfullyScreenState
    extends ConsumerState<OrderSuccessfullyScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 30.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 260.adaptSize,
                width: 260.adaptSize,
                padding: EdgeInsets.symmetric(vertical: 55.v),
                decoration: AppDecoration.fillGray30001.copyWith(
                  borderRadius: BorderRadiusStyle.circleBorder130,
                ),
                child: CustomImageView(
                  imagePath: ImageConstant.imgGroup3826,
                  height: 150.v,
                  width: 125.h,
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(height: 40.v),
              Text(
                "msg_order_placed_successfully".tr,
                style: CustomTextStyles.headlineSmall25,
              ),
              SizedBox(height: 7.v),
              Container(
                width: 327.h,
                margin: EdgeInsets.symmetric(horizontal: 13.h),
                child: Text(
                  "msg_thanks_for_your".tr,
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.titleMediumOnError_1.copyWith(
                    height: 1.50,
                  ),
                ),
              ),
              SizedBox(height: 98.v),
              CustomElevatedButton(
                text: "lbl_continue".tr,
              ),
              SizedBox(height: 12.v),
              Text(
                "lbl_track_order".tr,
                style: CustomTextStyles.titleMediumPrimaryBold,
              ),
              SizedBox(height: 5.v),
            ],
          ),
        ),
      ),
    );
  }
}
