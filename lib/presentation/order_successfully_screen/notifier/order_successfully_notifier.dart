import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/order_successfully_screen/models/order_successfully_model.dart';
part 'order_successfully_state.dart';

final orderSuccessfullyNotifier =
    StateNotifierProvider<OrderSuccessfullyNotifier, OrderSuccessfullyState>(
  (ref) => OrderSuccessfullyNotifier(OrderSuccessfullyState(
    orderSuccessfullyModelObj: OrderSuccessfullyModel(),
  )),
);

/// A notifier that manages the state of a OrderSuccessfully according to the event that is dispatched to it.
class OrderSuccessfullyNotifier extends StateNotifier<OrderSuccessfullyState> {
  OrderSuccessfullyNotifier(OrderSuccessfullyState state) : super(state) {}
}
