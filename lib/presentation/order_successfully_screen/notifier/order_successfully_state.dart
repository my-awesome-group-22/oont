// ignore_for_file: must_be_immutable

part of 'order_successfully_notifier.dart';

/// Represents the state of OrderSuccessfully in the application.
class OrderSuccessfullyState extends Equatable {
  OrderSuccessfullyState({this.orderSuccessfullyModelObj});

  OrderSuccessfullyModel? orderSuccessfullyModelObj;

  @override
  List<Object?> get props => [
        orderSuccessfullyModelObj,
      ];

  OrderSuccessfullyState copyWith(
      {OrderSuccessfullyModel? orderSuccessfullyModelObj}) {
    return OrderSuccessfullyState(
      orderSuccessfullyModelObj:
          orderSuccessfullyModelObj ?? this.orderSuccessfullyModelObj,
    );
  }
}
