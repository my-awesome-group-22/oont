import 'notifier/error_page_password_one_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/core/utils/validation_functions.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_text_form_field.dart';

class ErrorPagePasswordOneScreen extends ConsumerStatefulWidget {
  const ErrorPagePasswordOneScreen({Key? key})
      : super(
          key: key,
        );

  @override
  ErrorPagePasswordOneScreenState createState() =>
      ErrorPagePasswordOneScreenState();
}

class ErrorPagePasswordOneScreenState
    extends ConsumerState<ErrorPagePasswordOneScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Container(
            width: double.maxFinite,
            padding: EdgeInsets.symmetric(horizontal: 16.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 14.h),
                  child: Text(
                    "lbl_welcome".tr,
                    style: theme.textTheme.headlineLarge,
                  ),
                ),
                SizedBox(height: 95.v),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 14.h,
                    vertical: 88.v,
                  ),
                  decoration: AppDecoration.fillWhiteA.copyWith(
                    borderRadius: BorderRadiusStyle.roundedBorder30,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildInputFieldPhoneNumber(context),
                      SizedBox(height: 28.v),
                      _buildInputFieldPassword(context),
                      SizedBox(height: 6.v),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 6.h),
                          child: Text(
                            "msg_password_is_incorrect".tr,
                            style: CustomTextStyles.titleSmallDeeporangeA400,
                          ),
                        ),
                      ),
                      SizedBox(height: 28.v),
                      CustomElevatedButton(
                        text: "lbl_log_in2".tr,
                        rightIcon: Container(
                          margin: EdgeInsets.only(left: 30.h),
                          child: CustomImageView(
                            imagePath: ImageConstant.imgArrowleft,
                            height: 17.v,
                            width: 22.h,
                          ),
                        ),
                        buttonStyle: CustomButtonStyles.fillAmber,
                      ),
                      SizedBox(height: 45.v),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 1.v),
                            child: Text(
                              "msg_don_t_have_account".tr,
                              style: theme.textTheme.titleSmall,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 1.h),
                            child: Text(
                              "lbl_sign_up".tr,
                              style: CustomTextStyles.titleSmallAmber700,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5.v),
                    ],
                  ),
                ),
                SizedBox(height: 5.v),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildInputFieldPhoneNumber(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Opacity(
            opacity: 0.8,
            child: Text(
              "lbl_phone_number".tr,
              style: CustomTextStyles.titleSmallOnError,
            ),
          ),
          SizedBox(height: 10.v),
          Consumer(
            builder: (context, ref, _) {
              return CustomTextFormField(
                controller: ref
                    .watch(errorPagePasswordOneNotifier)
                    .phoneNumberController,
                hintText: "lbl_88017100000".tr,
              );
            },
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildInputFieldPassword(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Opacity(
            opacity: 0.8,
            child: Text(
              "lbl_password".tr,
              style: CustomTextStyles.titleSmallOnError,
            ),
          ),
          SizedBox(height: 10.v),
          Consumer(
            builder: (context, ref, _) {
              return CustomTextFormField(
                controller:
                    ref.watch(errorPagePasswordOneNotifier).passwordController,
                hintText: "lbl".tr,
                textInputAction: TextInputAction.done,
                textInputType: TextInputType.visiblePassword,
                suffix: InkWell(
                  onTap: () {
                    ref
                        .read(errorPagePasswordOneNotifier.notifier)
                        .changePasswordVisibility();
                  },
                  child: Container(
                    margin: EdgeInsets.fromLTRB(30.h, 17.v, 30.h, 18.v),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgEye,
                      height: 15.adaptSize,
                      width: 15.adaptSize,
                    ),
                  ),
                ),
                suffixConstraints: BoxConstraints(
                  maxHeight: 50.v,
                ),
                validator: (value) {
                  if (value == null ||
                      (!isValidPassword(value, isRequired: true))) {
                    return "err_msg_please_enter_valid_password".tr;
                  }
                  return null;
                },
                obscureText:
                    ref.watch(errorPagePasswordOneNotifier).isShowPassword,
                contentPadding: EdgeInsets.only(
                  left: 30.h,
                  top: 16.v,
                  bottom: 16.v,
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
