// ignore_for_file: must_be_immutable

part of 'error_page_password_one_notifier.dart';

/// Represents the state of ErrorPagePasswordOne in the application.
class ErrorPagePasswordOneState extends Equatable {
  ErrorPagePasswordOneState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.errorPagePasswordOneModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  ErrorPagePasswordOneModel? errorPagePasswordOneModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        errorPagePasswordOneModelObj,
      ];

  ErrorPagePasswordOneState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    ErrorPagePasswordOneModel? errorPagePasswordOneModelObj,
  }) {
    return ErrorPagePasswordOneState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      errorPagePasswordOneModelObj:
          errorPagePasswordOneModelObj ?? this.errorPagePasswordOneModelObj,
    );
  }
}
