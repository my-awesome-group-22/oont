import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/error_page_password_one_screen/models/error_page_password_one_model.dart';
part 'error_page_password_one_state.dart';

final errorPagePasswordOneNotifier = StateNotifierProvider<
    ErrorPagePasswordOneNotifier, ErrorPagePasswordOneState>(
  (ref) => ErrorPagePasswordOneNotifier(ErrorPagePasswordOneState(
    phoneNumberController: TextEditingController(),
    passwordController: TextEditingController(),
    isShowPassword: false,
    errorPagePasswordOneModelObj: ErrorPagePasswordOneModel(),
  )),
);

/// A notifier that manages the state of a ErrorPagePasswordOne according to the event that is dispatched to it.
class ErrorPagePasswordOneNotifier
    extends StateNotifier<ErrorPagePasswordOneState> {
  ErrorPagePasswordOneNotifier(ErrorPagePasswordOneState state)
      : super(state) {}

  void changePasswordVisibility() {
    state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false));
  }
}
