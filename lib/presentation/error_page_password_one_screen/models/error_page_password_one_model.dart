// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [error_page_password_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class ErrorPagePasswordOneModel extends Equatable {ErrorPagePasswordOneModel() {  }

ErrorPagePasswordOneModel copyWith() { return ErrorPagePasswordOneModel(
); } 
@override List<Object?> get props => [];
 }
