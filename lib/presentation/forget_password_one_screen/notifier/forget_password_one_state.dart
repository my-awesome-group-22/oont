// ignore_for_file: must_be_immutable

part of 'forget_password_one_notifier.dart';

/// Represents the state of ForgetPasswordOne in the application.
class ForgetPasswordOneState extends Equatable {
  ForgetPasswordOneState({
    this.phoneNumberController,
    this.forgetPasswordOneModelObj,
  });

  TextEditingController? phoneNumberController;

  ForgetPasswordOneModel? forgetPasswordOneModelObj;

  @override
  List<Object?> get props => [
        phoneNumberController,
        forgetPasswordOneModelObj,
      ];

  ForgetPasswordOneState copyWith({
    TextEditingController? phoneNumberController,
    ForgetPasswordOneModel? forgetPasswordOneModelObj,
  }) {
    return ForgetPasswordOneState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      forgetPasswordOneModelObj:
          forgetPasswordOneModelObj ?? this.forgetPasswordOneModelObj,
    );
  }
}
