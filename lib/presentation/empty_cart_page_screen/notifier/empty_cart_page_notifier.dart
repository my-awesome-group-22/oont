import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/empty_cart_page_screen/models/empty_cart_page_model.dart';part 'empty_cart_page_state.dart';final emptyCartPageNotifier = StateNotifierProvider<EmptyCartPageNotifier, EmptyCartPageState>((ref) => EmptyCartPageNotifier(EmptyCartPageState(emptyCartPageModelObj: EmptyCartPageModel())));
/// A notifier that manages the state of a EmptyCartPage according to the event that is dispatched to it.
class EmptyCartPageNotifier extends StateNotifier<EmptyCartPageState> {EmptyCartPageNotifier(EmptyCartPageState state) : super(state);

 }
