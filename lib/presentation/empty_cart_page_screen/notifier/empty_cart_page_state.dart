// ignore_for_file: must_be_immutable

part of 'empty_cart_page_notifier.dart';

/// Represents the state of EmptyCartPage in the application.
class EmptyCartPageState extends Equatable {
  EmptyCartPageState({this.emptyCartPageModelObj});

  EmptyCartPageModel? emptyCartPageModelObj;

  @override
  List<Object?> get props => [
        emptyCartPageModelObj,
      ];

  EmptyCartPageState copyWith({EmptyCartPageModel? emptyCartPageModelObj}) {
    return EmptyCartPageState(
      emptyCartPageModelObj:
          emptyCartPageModelObj ?? this.emptyCartPageModelObj,
    );
  }
}
