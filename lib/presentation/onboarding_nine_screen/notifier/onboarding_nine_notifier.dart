import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_nine_screen/models/onboarding_nine_model.dart';
part 'onboarding_nine_state.dart';

final onboardingNineNotifier =
    StateNotifierProvider<OnboardingNineNotifier, OnboardingNineState>(
  (ref) => OnboardingNineNotifier(OnboardingNineState(
    onboardingNineModelObj: OnboardingNineModel(),
  )),
);

/// A notifier that manages the state of a OnboardingNine according to the event that is dispatched to it.
class OnboardingNineNotifier extends StateNotifier<OnboardingNineState> {
  OnboardingNineNotifier(OnboardingNineState state) : super(state) {}
}
