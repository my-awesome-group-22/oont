// ignore_for_file: must_be_immutable

part of 'onboarding_nine_notifier.dart';

/// Represents the state of OnboardingNine in the application.
class OnboardingNineState extends Equatable {
  OnboardingNineState({this.onboardingNineModelObj});

  OnboardingNineModel? onboardingNineModelObj;

  @override
  List<Object?> get props => [
        onboardingNineModelObj,
      ];

  OnboardingNineState copyWith({OnboardingNineModel? onboardingNineModelObj}) {
    return OnboardingNineState(
      onboardingNineModelObj:
          onboardingNineModelObj ?? this.onboardingNineModelObj,
    );
  }
}
