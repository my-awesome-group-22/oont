import 'notifier/onboarding_nine_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingNineScreen extends ConsumerStatefulWidget {
  const OnboardingNineScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingNineScreenState createState() => OnboardingNineScreenState();
}

class OnboardingNineScreenState extends ConsumerState<OnboardingNineScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOontPrimarycontainer,
                height: 85.v,
                width: 288.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
