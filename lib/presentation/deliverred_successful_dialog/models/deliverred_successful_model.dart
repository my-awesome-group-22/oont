// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [deliverred_successful_dialog],
/// and is typically used to hold data that is passed between different parts of the application.
class DeliverredSuccessfulModel extends Equatable {DeliverredSuccessfulModel() {  }

DeliverredSuccessfulModel copyWith() { return DeliverredSuccessfulModel(
); } 
@override List<Object?> get props => [];
 }
