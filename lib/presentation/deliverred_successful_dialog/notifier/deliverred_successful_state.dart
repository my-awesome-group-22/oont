// ignore_for_file: must_be_immutable

part of 'deliverred_successful_notifier.dart';

/// Represents the state of DeliverredSuccessful in the application.
class DeliverredSuccessfulState extends Equatable {
  DeliverredSuccessfulState({this.deliverredSuccessfulModelObj});

  DeliverredSuccessfulModel? deliverredSuccessfulModelObj;

  @override
  List<Object?> get props => [
        deliverredSuccessfulModelObj,
      ];

  DeliverredSuccessfulState copyWith(
      {DeliverredSuccessfulModel? deliverredSuccessfulModelObj}) {
    return DeliverredSuccessfulState(
      deliverredSuccessfulModelObj:
          deliverredSuccessfulModelObj ?? this.deliverredSuccessfulModelObj,
    );
  }
}
