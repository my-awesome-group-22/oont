import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/deliverred_successful_dialog/models/deliverred_successful_model.dart';
part 'deliverred_successful_state.dart';

final deliverredSuccessfulNotifier = StateNotifierProvider<
    DeliverredSuccessfulNotifier, DeliverredSuccessfulState>(
  (ref) => DeliverredSuccessfulNotifier(DeliverredSuccessfulState(
    deliverredSuccessfulModelObj: DeliverredSuccessfulModel(),
  )),
);

/// A notifier that manages the state of a DeliverredSuccessful according to the event that is dispatched to it.
class DeliverredSuccessfulNotifier
    extends StateNotifier<DeliverredSuccessfulState> {
  DeliverredSuccessfulNotifier(DeliverredSuccessfulState state)
      : super(state) {}
}
