import 'notifier/deliverred_successful_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_outlined_button.dart';

// ignore_for_file: must_be_immutable
class DeliverredSuccessfulDialog extends ConsumerStatefulWidget {
  const DeliverredSuccessfulDialog({Key? key})
      : super(
          key: key,
        );

  @override
  DeliverredSuccessfulDialogState createState() =>
      DeliverredSuccessfulDialogState();
}

class DeliverredSuccessfulDialogState
    extends ConsumerState<DeliverredSuccessfulDialog> {
  @override
  Widget build(BuildContext context) {
    return _buildVerifiedSection(context);
  }

  /// Section Widget
  Widget _buildVerifiedSection(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 0,
      margin: EdgeInsets.all(0),
      color: appTheme.whiteA700,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadiusStyle.roundedBorder30,
      ),
      child: Container(
        height: 660.v,
        width: 382.h,
        padding: EdgeInsets.symmetric(
          horizontal: 25.h,
          vertical: 51.v,
        ),
        decoration: AppDecoration.fillWhiteA.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder30,
        ),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            CustomImageView(
              imagePath: ImageConstant.imgGroup3832Green5001,
              height: 301.v,
              width: 276.h,
              alignment: Alignment.topCenter,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 8.v),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "msg_congratulations".tr,
                      style: theme.textTheme.headlineSmall,
                    ),
                    SizedBox(height: 19.v),
                    SizedBox(
                      width: 254.h,
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "msg_hurrah_we_just2".tr,
                              style: CustomTextStyles.titleMediumOnError_2,
                            ),
                            TextSpan(
                              text: "msg_15425050_order".tr,
                              style: theme.textTheme.titleMedium,
                            ),
                          ],
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 44.v),
                    CustomElevatedButton(
                      text: "msg_rate_the_product".tr,
                    ),
                    SizedBox(height: 25.v),
                    CustomOutlinedButton(
                      text: "lbl_browse_home".tr,
                      buttonStyle: CustomButtonStyles.outlinePrimaryTL15,
                      buttonTextStyle: CustomTextStyles.titleMediumPrimaryBold,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
