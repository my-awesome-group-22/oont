import 'notifier/home_container_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/presentation/category_page/category_page.dart';import 'package:oont/presentation/favorite_list_page/favorite_list_page.dart';import 'package:oont/presentation/home_page/home_page.dart';import 'package:oont/presentation/profile_two_page/profile_two_page.dart';import 'package:oont/widgets/custom_bottom_app_bar.dart';import 'package:oont/widgets/custom_floating_button.dart';class HomeContainerScreen extends ConsumerStatefulWidget {const HomeContainerScreen({Key? key}) : super(key: key);

@override HomeContainerScreenState createState() =>  HomeContainerScreenState();

 }

// ignore_for_file: must_be_immutable
class HomeContainerScreenState extends ConsumerState<HomeContainerScreen> {GlobalKey<NavigatorState> navigatorKey = GlobalKey();

@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(body: Navigator(key: navigatorKey, initialRoute: AppRoutes.homePage, onGenerateRoute: (routeSetting) => PageRouteBuilder(pageBuilder: (ctx, ani, ani1) => getCurrentPage(context, routeSetting.name!), transitionDuration: Duration(seconds: 0))), bottomNavigationBar: _buildBottomAppBar(context), floatingActionButton: CustomFloatingButton(height: 60, width: 60, backgroundColor: theme.colorScheme.primary, child: CustomImageView(imagePath: ImageConstant.imgTelevisionWhiteA700, height: 30.0.v, width: 30.0.h)), floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked)); } 
/// Section Widget
Widget _buildBottomAppBar(BuildContext context) { return CustomBottomAppBar(onChanged: (BottomBarEnum type) {Navigator.pushNamed(navigatorKey.currentContext!, getCurrentRoute(type));}); } 
///Handling route based on bottom click actions
String getCurrentRoute(BottomBarEnum type) { switch (type) {case BottomBarEnum.Home: return AppRoutes.homePage; case BottomBarEnum.Categories: return AppRoutes.categoryPage; case BottomBarEnum.Favourites: return AppRoutes.favoriteListPage; case BottomBarEnum.Profile: return AppRoutes.profileTwoPage; default: return "/";} } 
///Handling page based on route
Widget getCurrentPage(BuildContext context, String currentRoute, ) { switch (currentRoute) {case AppRoutes.homePage: return HomePage(); case AppRoutes.categoryPage: return CategoryPage(); case AppRoutes.favoriteListPage: return FavoriteListPage(); case AppRoutes.profileTwoPage: return ProfileTwoPage(); default: return DefaultWidget();} } 
 }
