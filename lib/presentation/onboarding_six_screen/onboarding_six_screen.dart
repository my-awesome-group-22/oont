import 'notifier/onboarding_six_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

class OnboardingSixScreen extends ConsumerStatefulWidget {
  const OnboardingSixScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingSixScreenState createState() => OnboardingSixScreenState();
}

class OnboardingSixScreenState extends ConsumerState<OnboardingSixScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(vertical: 60.v),
          child: Column(
            children: [
              SizedBox(height: 39.v),
              _buildNinetyOne(context),
              SizedBox(height: 77.v),
              Text(
                "msg_browse_different".tr,
                style: CustomTextStyles.headlineSmall25,
              ),
              SizedBox(height: 26.v),
              Opacity(
                opacity: 0.6,
                child: Container(
                  width: 323.h,
                  margin: EdgeInsets.symmetric(horizontal: 45.h),
                  child: Text(
                    "msg_in_aliquip_aute".tr,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: CustomTextStyles.titleMediumGray900_2.copyWith(
                      height: 1.40,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 62.v),
              SizedBox(
                height: 88.adaptSize,
                width: 88.adaptSize,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img100,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerRight,
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: SizedBox(
                                        height: 31.v,
                                        width: 43.h,
                                        child: CircularProgressIndicator(
                                          value: 0.5,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildSevenHundredFifty(
                                context,
                                dynamicImage1: ImageConstant.img250Amber700,
                                dynamicImage2:
                                    ImageConstant.imgItemsOutlineAmber700,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              _buildSevenHundredFifty(
                                context,
                                dynamicImage1: ImageConstant.img750,
                                dynamicImage2: ImageConstant.img625,
                              ),
                              SizedBox(
                                height: 44.v,
                                width: 43.h,
                                child: Stack(
                                  alignment: Alignment.topCenter,
                                  children: [
                                    CustomImageView(
                                      imagePath: ImageConstant.img500,
                                      height: 44.v,
                                      width: 31.h,
                                      alignment: Alignment.centerLeft,
                                    ),
                                    CustomImageView(
                                      imagePath: ImageConstant.img250Amber700,
                                      height: 31.v,
                                      width: 43.h,
                                      alignment: Alignment.topCenter,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    CustomIconButton(
                      height: 80.adaptSize,
                      width: 80.adaptSize,
                      alignment: Alignment.center,
                      child: CustomImageView(
                        imagePath: ImageConstant.imgGroup317,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildNinetyOne(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(right: 21.h),
        padding: EdgeInsets.symmetric(
          horizontal: 185.h,
          vertical: 166.v,
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              ImageConstant.imgGroup57,
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 12.v),
            Container(
              height: 24.v,
              width: 21.h,
              decoration: BoxDecoration(
                color: appTheme.orange200,
                borderRadius: BorderRadius.circular(
                  3.h,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Common widget
  Widget _buildSevenHundredFifty(
    BuildContext context, {
    required String dynamicImage1,
    required String dynamicImage2,
  }) {
    return SizedBox(
      height: 44.v,
      width: 43.h,
      child: Stack(
        alignment: Alignment.centerRight,
        children: [
          CustomImageView(
            imagePath: dynamicImage1,
            height: 31.v,
            width: 43.h,
            alignment: Alignment.topCenter,
          ),
          CustomImageView(
            imagePath: dynamicImage2,
            height: 44.v,
            width: 31.h,
            alignment: Alignment.centerRight,
          ),
        ],
      ),
    );
  }
}
