import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_six_screen/models/onboarding_six_model.dart';
part 'onboarding_six_state.dart';

final onboardingSixNotifier =
    StateNotifierProvider<OnboardingSixNotifier, OnboardingSixState>(
  (ref) => OnboardingSixNotifier(OnboardingSixState(
    onboardingSixModelObj: OnboardingSixModel(),
  )),
);

/// A notifier that manages the state of a OnboardingSix according to the event that is dispatched to it.
class OnboardingSixNotifier extends StateNotifier<OnboardingSixState> {
  OnboardingSixNotifier(OnboardingSixState state) : super(state) {}
}
