import '../models/productgrid_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class ProductgridItemWidget extends StatelessWidget {
  ProductgridItemWidget(
    this.productgridItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ProductgridItemModel productgridItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: EdgeInsets.all(11.h),
        decoration: AppDecoration.outlineGray20001.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder21,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(height: 7.v),
            CustomImageView(
              imagePath: productgridItemModelObj?.productImage,
              height: 71.v,
              width: 80.h,
              alignment: Alignment.center,
            ),
            SizedBox(height: 17.v),
            Container(
              width: 135.h,
              margin: EdgeInsets.only(left: 3.h),
              child: Text(
                productgridItemModelObj.productName!,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: theme.textTheme.titleMedium!.copyWith(
                  height: 1.38,
                ),
              ),
            ),
            SizedBox(height: 4.v),
            Opacity(
              opacity: 0.8,
              child: Padding(
                padding: EdgeInsets.only(left: 3.h),
                child: Text(
                  productgridItemModelObj.productWeight!,
                  style: CustomTextStyles.titleSmallOnError,
                ),
              ),
            ),
            SizedBox(height: 8.v),
            Padding(
              padding: EdgeInsets.only(left: 3.h),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: 2.v,
                      bottom: 8.v,
                    ),
                    child: Text(
                      productgridItemModelObj.productPrice!,
                      style: CustomTextStyles.titleMediumBold_2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 102.h),
                    child: CustomIconButton(
                      height: 28.adaptSize,
                      width: 28.adaptSize,
                      padding: EdgeInsets.all(7.h),
                      child: CustomImageView(
                        imagePath: ImageConstant.imgClose,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
