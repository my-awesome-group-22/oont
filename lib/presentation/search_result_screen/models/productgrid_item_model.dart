import '../../../core/app_export.dart';/// This class is used in the [productgrid_item_widget] screen.
class ProductgridItemModel {ProductgridItemModel({this.productImage, this.productName, this.productWeight, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.imgGroup3473;productName = productName  ?? "Cauliflower\nForm The Garden";productWeight = productWeight  ?? "1 Kg";productPrice = productPrice  ?? "13";id = id  ?? ""; }

String? productImage;

String? productName;

String? productWeight;

String? productPrice;

String? id;

 }
