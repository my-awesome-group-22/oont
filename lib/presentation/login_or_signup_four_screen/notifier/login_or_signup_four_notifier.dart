import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/login_or_signup_four_screen/models/login_or_signup_four_model.dart';
part 'login_or_signup_four_state.dart';

final loginOrSignupFourNotifier =
    StateNotifierProvider<LoginOrSignupFourNotifier, LoginOrSignupFourState>(
  (ref) => LoginOrSignupFourNotifier(LoginOrSignupFourState(
    loginOrSignupFourModelObj: LoginOrSignupFourModel(),
  )),
);

/// A notifier that manages the state of a LoginOrSignupFour according to the event that is dispatched to it.
class LoginOrSignupFourNotifier extends StateNotifier<LoginOrSignupFourState> {
  LoginOrSignupFourNotifier(LoginOrSignupFourState state) : super(state) {}
}
