// ignore_for_file: must_be_immutable

part of 'login_or_signup_four_notifier.dart';

/// Represents the state of LoginOrSignupFour in the application.
class LoginOrSignupFourState extends Equatable {
  LoginOrSignupFourState({this.loginOrSignupFourModelObj});

  LoginOrSignupFourModel? loginOrSignupFourModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupFourModelObj,
      ];

  LoginOrSignupFourState copyWith(
      {LoginOrSignupFourModel? loginOrSignupFourModelObj}) {
    return LoginOrSignupFourState(
      loginOrSignupFourModelObj:
          loginOrSignupFourModelObj ?? this.loginOrSignupFourModelObj,
    );
  }
}
