import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/change_password_screen/models/change_password_model.dart';part 'change_password_state.dart';final changePasswordNotifier = StateNotifierProvider<ChangePasswordNotifier, ChangePasswordState>((ref) => ChangePasswordNotifier(ChangePasswordState(passwordController: TextEditingController(), newpasswordController: TextEditingController(), passwordController1: TextEditingController(), isShowPassword: false, isShowPassword1: false, isShowPassword2: false, changePasswordModelObj: ChangePasswordModel())));
/// A notifier that manages the state of a ChangePassword according to the event that is dispatched to it.
class ChangePasswordNotifier extends StateNotifier<ChangePasswordState> {ChangePasswordNotifier(ChangePasswordState state) : super(state);

void changePasswordVisibility() { state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false)); } 
void changePasswordVisibility1() { state = state.copyWith(isShowPassword1: !(state.isShowPassword1 ?? false)); } 
void changePasswordVisibility2() { state = state.copyWith(isShowPassword2: !(state.isShowPassword2 ?? false)); } 
 }
