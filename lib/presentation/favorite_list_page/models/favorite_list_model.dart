// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'productlist1_item_model.dart';/// This class defines the variables used in the [favorite_list_page],
/// and is typically used to hold data that is passed between different parts of the application.
class FavoriteListModel extends Equatable {FavoriteListModel({this.productlist1ItemList = const []}) {  }

List<Productlist1ItemModel> productlist1ItemList;

FavoriteListModel copyWith({List<Productlist1ItemModel>? productlist1ItemList}) { return FavoriteListModel(
productlist1ItemList : productlist1ItemList ?? this.productlist1ItemList,
); } 
@override List<Object?> get props => [productlist1ItemList];
 }
