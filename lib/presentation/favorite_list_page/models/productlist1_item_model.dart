import '../../../core/app_export.dart';/// This class is used in the [productlist1_item_widget] screen.
class Productlist1ItemModel {Productlist1ItemModel({this.productImage, this.productName, this.productSize, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.img00018000117090A1c106003x80x58;productName = productName  ?? "Oreo Biscut";productSize = productSize  ?? "570 Ml";productPrice = productPrice  ?? "£10";id = id  ?? ""; }

String? productImage;

String? productName;

String? productSize;

String? productPrice;

String? id;

 }
