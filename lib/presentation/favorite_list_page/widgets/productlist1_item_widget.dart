import '../models/productlist1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Productlist1ItemWidget extends StatelessWidget {
  Productlist1ItemWidget(
    this.productlist1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Productlist1ItemModel productlist1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomImageView(
          imagePath: productlist1ItemModelObj?.productImage,
          height: 80.v,
          width: 58.h,
          margin: EdgeInsets.only(
            left: 11.h,
            top: 3.v,
            bottom: 17.v,
          ),
        ),
        Spacer(
          flex: 16,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 25.v),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                productlist1ItemModelObj.productName!,
                style: theme.textTheme.titleMedium,
              ),
              SizedBox(height: 5.v),
              Text(
                productlist1ItemModelObj.productSize!,
                style: theme.textTheme.titleSmall,
              ),
              SizedBox(height: 16.v),
              Text(
                productlist1ItemModelObj.productPrice!,
                style: theme.textTheme.titleLarge,
              ),
            ],
          ),
        ),
        Spacer(
          flex: 83,
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 2.v,
            bottom: 21.v,
          ),
          child: Column(
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgThumbsUpGray90024x24,
                height: 24.adaptSize,
                width: 24.adaptSize,
              ),
              SizedBox(height: 29.v),
              CustomImageView(
                imagePath: ImageConstant.imgSearchGray900,
                height: 24.adaptSize,
                width: 24.adaptSize,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
