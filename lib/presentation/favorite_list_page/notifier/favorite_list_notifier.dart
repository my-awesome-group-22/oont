import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/productlist1_item_model.dart';import 'package:oont/presentation/favorite_list_page/models/favorite_list_model.dart';part 'favorite_list_state.dart';final favoriteListNotifier = StateNotifierProvider<FavoriteListNotifier, FavoriteListState>((ref) => FavoriteListNotifier(FavoriteListState(favoriteListModelObj: FavoriteListModel(productlist1ItemList: [Productlist1ItemModel(productImage: ImageConstant.img00018000117090A1c106003x80x58, productName: "Oreo Biscut", productSize: "570 Ml", productPrice: "£10")]))));
/// A notifier that manages the state of a FavoriteList according to the event that is dispatched to it.
class FavoriteListNotifier extends StateNotifier<FavoriteListState> {FavoriteListNotifier(FavoriteListState state) : super(state);

 }
