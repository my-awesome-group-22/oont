// ignore_for_file: must_be_immutable

part of 'favorite_list_notifier.dart';

/// Represents the state of FavoriteList in the application.
class FavoriteListState extends Equatable {
  FavoriteListState({this.favoriteListModelObj});

  FavoriteListModel? favoriteListModelObj;

  @override
  List<Object?> get props => [
        favoriteListModelObj,
      ];

  FavoriteListState copyWith({FavoriteListModel? favoriteListModelObj}) {
    return FavoriteListState(
      favoriteListModelObj: favoriteListModelObj ?? this.favoriteListModelObj,
    );
  }
}
