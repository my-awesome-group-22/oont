import 'notifier/new_addres_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_radio_button.dart';import 'package:oont/widgets/custom_text_form_field.dart';class NewAddresScreen extends ConsumerStatefulWidget {const NewAddresScreen({Key? key}) : super(key: key);

@override NewAddresScreenState createState() =>  NewAddresScreenState();

 }
class NewAddresScreenState extends ConsumerState<NewAddresScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: SizedBox(width: SizeUtils.width, child: SingleChildScrollView(padding: EdgeInsets.only(top: 30.v), child: Container(margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 5.v), padding: EdgeInsets.symmetric(horizontal: 14.h, vertical: 29.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [_buildProfileSetting(context), SizedBox(height: 28.v), _buildPhoneNumber(context), SizedBox(height: 28.v), _buildAddressLink1(context), SizedBox(height: 28.v), _buildAddressLink2(context), SizedBox(height: 29.v), _buildCity1(context), SizedBox(height: 28.v), _buildKeyEleven(context), SizedBox(height: 29.v), _buildMakeDefaultDefault(context), SizedBox(height: 39.v), _buildSaveAddress(context)])))))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_new_address".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildFullName(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newAddresNotifier).fullNameController, hintText: "lbl_bilal_riaz".tr);}); } 
/// Section Widget
Widget _buildProfileSetting(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_full_name".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), _buildFullName(context)])); } 
/// Section Widget
Widget _buildPhoneNumber(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_phone_number".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newAddresNotifier).phoneNumberController, hintText: "lbl_8801710000000".tr);})])); } 
/// Section Widget
Widget _buildAddressLink1(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_address_link_1".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newAddresNotifier).addressController, hintText: "lbl_times_square".tr);})])); } 
/// Section Widget
Widget _buildAddressLink2(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_address_link_2".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newAddresNotifier).addressController1, hintText: "lbl_times_square".tr);})])); } 
/// Section Widget
Widget _buildCity(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newAddresNotifier).cityController, hintText: "lbl_london".tr);}); } 
/// Section Widget
Widget _buildCity1(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_city".tr, style: theme.textTheme.titleSmall), SizedBox(height: 11.v), _buildCity(context)])); } 
/// Section Widget
Widget _buildCounty(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(width: 161.h, controller: ref.watch(newAddresNotifier).countyController, hintText: "lbl_london".tr);}); } 
/// Section Widget
Widget _buildPostCode(BuildContext context) { return Expanded(child: Padding(padding: EdgeInsets.only(left: 10.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_post_code".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(width: 161.h, controller: ref.watch(newAddresNotifier).zipcodeController, hintText: "lbl_303934".tr, textInputAction: TextInputAction.done);})]))); } 
/// Section Widget
Widget _buildKeyEleven(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [Expanded(child: Padding(padding: EdgeInsets.only(top: 1.v, right: 10.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_county".tr, style: theme.textTheme.titleSmall), SizedBox(height: 11.v), _buildCounty(context)]))), _buildPostCode(context)])); } 
/// Section Widget
Widget _buildMakeDefaultDefault(BuildContext context) { return Consumer(builder: (context, ref, _) {return Padding(padding: EdgeInsets.only(left: 6.h, right: 10.h), child: CustomRadioButton(text: "msg_make_default_default".tr, value: "msg_make_default_default".tr, groupValue: ref.watch(newAddresNotifier).radioGroup, textStyle: theme.textTheme.titleMedium, onChange: (value) {ref.read(newAddresNotifier.notifier).changeRadioButton1(value);}));}); } 
/// Section Widget
Widget _buildSaveAddress(BuildContext context) { return CustomElevatedButton(text: "lbl_save_address".tr); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
