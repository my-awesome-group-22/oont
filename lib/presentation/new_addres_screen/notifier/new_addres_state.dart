// ignore_for_file: must_be_immutable

part of 'new_addres_notifier.dart';

/// Represents the state of NewAddres in the application.
class NewAddresState extends Equatable {
  NewAddresState({
    this.fullNameController,
    this.phoneNumberController,
    this.addressController,
    this.addressController1,
    this.cityController,
    this.countyController,
    this.zipcodeController,
    this.radioGroup = "",
    this.newAddresModelObj,
  });

  TextEditingController? fullNameController;

  TextEditingController? phoneNumberController;

  TextEditingController? addressController;

  TextEditingController? addressController1;

  TextEditingController? cityController;

  TextEditingController? countyController;

  TextEditingController? zipcodeController;

  NewAddresModel? newAddresModelObj;

  String radioGroup;

  @override
  List<Object?> get props => [
        fullNameController,
        phoneNumberController,
        addressController,
        addressController1,
        cityController,
        countyController,
        zipcodeController,
        radioGroup,
        newAddresModelObj,
      ];

  NewAddresState copyWith({
    TextEditingController? fullNameController,
    TextEditingController? phoneNumberController,
    TextEditingController? addressController,
    TextEditingController? addressController1,
    TextEditingController? cityController,
    TextEditingController? countyController,
    TextEditingController? zipcodeController,
    String? radioGroup,
    NewAddresModel? newAddresModelObj,
  }) {
    return NewAddresState(
      fullNameController: fullNameController ?? this.fullNameController,
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      addressController: addressController ?? this.addressController,
      addressController1: addressController1 ?? this.addressController1,
      cityController: cityController ?? this.cityController,
      countyController: countyController ?? this.countyController,
      zipcodeController: zipcodeController ?? this.zipcodeController,
      radioGroup: radioGroup ?? this.radioGroup,
      newAddresModelObj: newAddresModelObj ?? this.newAddresModelObj,
    );
  }
}
