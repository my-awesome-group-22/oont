import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/new_addres_screen/models/new_addres_model.dart';part 'new_addres_state.dart';final newAddresNotifier = StateNotifierProvider<NewAddresNotifier, NewAddresState>((ref) => NewAddresNotifier(NewAddresState(fullNameController: TextEditingController(), phoneNumberController: TextEditingController(), addressController: TextEditingController(), addressController1: TextEditingController(), cityController: TextEditingController(), countyController: TextEditingController(), zipcodeController: TextEditingController(), radioGroup: '', newAddresModelObj: NewAddresModel())));
/// A notifier that manages the state of a NewAddres according to the event that is dispatched to it.
class NewAddresNotifier extends StateNotifier<NewAddresState> {NewAddresNotifier(NewAddresState state) : super(state);

void changeRadioButton1(String value) { state = state.copyWith(radioGroup: value); } 
 }
