// ignore_for_file: must_be_immutable

part of 'sorry_order_is_failed_notifier.dart';

/// Represents the state of SorryOrderIsFailed in the application.
class SorryOrderIsFailedState extends Equatable {
  SorryOrderIsFailedState({this.sorryOrderIsFailedModelObj});

  SorryOrderIsFailedModel? sorryOrderIsFailedModelObj;

  @override
  List<Object?> get props => [
        sorryOrderIsFailedModelObj,
      ];

  SorryOrderIsFailedState copyWith(
      {SorryOrderIsFailedModel? sorryOrderIsFailedModelObj}) {
    return SorryOrderIsFailedState(
      sorryOrderIsFailedModelObj:
          sorryOrderIsFailedModelObj ?? this.sorryOrderIsFailedModelObj,
    );
  }
}
