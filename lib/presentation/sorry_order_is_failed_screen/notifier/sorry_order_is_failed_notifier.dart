import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/sorry_order_is_failed_screen/models/sorry_order_is_failed_model.dart';
part 'sorry_order_is_failed_state.dart';

final sorryOrderIsFailedNotifier =
    StateNotifierProvider<SorryOrderIsFailedNotifier, SorryOrderIsFailedState>(
  (ref) => SorryOrderIsFailedNotifier(SorryOrderIsFailedState(
    sorryOrderIsFailedModelObj: SorryOrderIsFailedModel(),
  )),
);

/// A notifier that manages the state of a SorryOrderIsFailed according to the event that is dispatched to it.
class SorryOrderIsFailedNotifier
    extends StateNotifier<SorryOrderIsFailedState> {
  SorryOrderIsFailedNotifier(SorryOrderIsFailedState state) : super(state) {}
}
