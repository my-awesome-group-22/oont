import 'notifier/sorry_order_is_failed_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class SorryOrderIsFailedScreen extends ConsumerStatefulWidget {
  const SorryOrderIsFailedScreen({Key? key})
      : super(
          key: key,
        );

  @override
  SorryOrderIsFailedScreenState createState() =>
      SorryOrderIsFailedScreenState();
}

class SorryOrderIsFailedScreenState
    extends ConsumerState<SorryOrderIsFailedScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 30.h,
            vertical: 126.v,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 260.adaptSize,
                width: 260.adaptSize,
                padding: EdgeInsets.symmetric(vertical: 55.v),
                decoration: AppDecoration.fillGray30001.copyWith(
                  borderRadius: BorderRadiusStyle.circleBorder130,
                ),
                child: CustomImageView(
                  imagePath: ImageConstant.imgGroup3828,
                  height: 150.v,
                  width: 125.h,
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(height: 40.v),
              Text(
                "msg_sorry_order_has".tr,
                style: CustomTextStyles.headlineSmall25,
              ),
              SizedBox(height: 20.v),
              Container(
                width: 290.h,
                margin: EdgeInsets.symmetric(horizontal: 31.h),
                child: Text(
                  "msg_sorry_somethings".tr,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.titleMediumOnError_1.copyWith(
                    height: 1.50,
                  ),
                ),
              ),
              Spacer(),
              SizedBox(height: 40.v),
              CustomElevatedButton(
                text: "lbl_try_again".tr,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
