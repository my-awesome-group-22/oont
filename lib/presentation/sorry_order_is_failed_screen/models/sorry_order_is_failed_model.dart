// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [sorry_order_is_failed_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class SorryOrderIsFailedModel extends Equatable {SorryOrderIsFailedModel() {  }

SorryOrderIsFailedModel copyWith() { return SorryOrderIsFailedModel(
); } 
@override List<Object?> get props => [];
 }
