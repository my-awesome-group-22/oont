// ignore_for_file: must_be_immutable

part of 'error_page_phone_number_one_notifier.dart';

/// Represents the state of ErrorPagePhoneNumberOne in the application.
class ErrorPagePhoneNumberOneState extends Equatable {
  ErrorPagePhoneNumberOneState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.errorPagePhoneNumberOneModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  ErrorPagePhoneNumberOneModel? errorPagePhoneNumberOneModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        errorPagePhoneNumberOneModelObj,
      ];

  ErrorPagePhoneNumberOneState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    ErrorPagePhoneNumberOneModel? errorPagePhoneNumberOneModelObj,
  }) {
    return ErrorPagePhoneNumberOneState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      errorPagePhoneNumberOneModelObj: errorPagePhoneNumberOneModelObj ??
          this.errorPagePhoneNumberOneModelObj,
    );
  }
}
