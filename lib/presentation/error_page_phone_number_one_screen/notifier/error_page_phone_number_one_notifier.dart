import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/error_page_phone_number_one_screen/models/error_page_phone_number_one_model.dart';
part 'error_page_phone_number_one_state.dart';

final errorPagePhoneNumberOneNotifier = StateNotifierProvider<
    ErrorPagePhoneNumberOneNotifier, ErrorPagePhoneNumberOneState>(
  (ref) => ErrorPagePhoneNumberOneNotifier(ErrorPagePhoneNumberOneState(
    phoneNumberController: TextEditingController(),
    passwordController: TextEditingController(),
    isShowPassword: false,
    errorPagePhoneNumberOneModelObj: ErrorPagePhoneNumberOneModel(),
  )),
);

/// A notifier that manages the state of a ErrorPagePhoneNumberOne according to the event that is dispatched to it.
class ErrorPagePhoneNumberOneNotifier
    extends StateNotifier<ErrorPagePhoneNumberOneState> {
  ErrorPagePhoneNumberOneNotifier(ErrorPagePhoneNumberOneState state)
      : super(state) {}

  void changePasswordVisibility() {
    state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false));
  }
}
