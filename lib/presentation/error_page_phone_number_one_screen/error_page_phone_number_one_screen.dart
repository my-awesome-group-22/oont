import 'notifier/error_page_phone_number_one_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/core/utils/validation_functions.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_text_form_field.dart';

class ErrorPagePhoneNumberOneScreen extends ConsumerStatefulWidget {
  const ErrorPagePhoneNumberOneScreen({Key? key})
      : super(
          key: key,
        );

  @override
  ErrorPagePhoneNumberOneScreenState createState() =>
      ErrorPagePhoneNumberOneScreenState();
}

class ErrorPagePhoneNumberOneScreenState
    extends ConsumerState<ErrorPagePhoneNumberOneScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        resizeToAvoidBottomInset: false,
        body: Form(
          key: _formKey,
          child: Container(
            width: double.maxFinite,
            padding: EdgeInsets.symmetric(horizontal: 16.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 14.h),
                  child: Text(
                    "lbl_welcome".tr,
                    style: theme.textTheme.headlineLarge,
                  ),
                ),
                SizedBox(height: 95.v),
                _buildErrorPagePhone(context),
                SizedBox(height: 5.v),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildInputField1(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Opacity(
            opacity: 0.8,
            child: Text(
              "lbl_phone_number".tr,
              style: CustomTextStyles.titleSmallOnError,
            ),
          ),
          SizedBox(height: 10.v),
          Consumer(
            builder: (context, ref, _) {
              return CustomTextFormField(
                controller: ref
                    .watch(errorPagePhoneNumberOneNotifier)
                    .phoneNumberController,
                hintText: "lbl_88017100000".tr,
              );
            },
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildInputField2(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Opacity(
            opacity: 0.8,
            child: Text(
              "lbl_password".tr,
              style: CustomTextStyles.titleSmallOnError,
            ),
          ),
          SizedBox(height: 10.v),
          Consumer(
            builder: (context, ref, _) {
              return CustomTextFormField(
                controller: ref
                    .watch(errorPagePhoneNumberOneNotifier)
                    .passwordController,
                hintText: "lbl".tr,
                textInputAction: TextInputAction.done,
                textInputType: TextInputType.visiblePassword,
                suffix: InkWell(
                  onTap: () {
                    ref
                        .read(errorPagePhoneNumberOneNotifier.notifier)
                        .changePasswordVisibility();
                  },
                  child: Container(
                    margin: EdgeInsets.fromLTRB(30.h, 17.v, 30.h, 18.v),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgEye,
                      height: 15.adaptSize,
                      width: 15.adaptSize,
                    ),
                  ),
                ),
                suffixConstraints: BoxConstraints(
                  maxHeight: 50.v,
                ),
                validator: (value) {
                  if (value == null ||
                      (!isValidPassword(value, isRequired: true))) {
                    return "err_msg_please_enter_valid_password".tr;
                  }
                  return null;
                },
                obscureText:
                    ref.watch(errorPagePhoneNumberOneNotifier).isShowPassword,
                contentPadding: EdgeInsets.only(
                  left: 30.h,
                  top: 16.v,
                  bottom: 16.v,
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildErrorPagePhone(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 14.h,
        vertical: 63.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder30,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 24.v),
          _buildInputField1(context),
          SizedBox(height: 6.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 6.h),
              child: Text(
                "msg_number_is_incorrect".tr,
                style: CustomTextStyles.titleSmallDeeporangeA400,
              ),
            ),
          ),
          SizedBox(height: 26.v),
          _buildInputField2(context),
          SizedBox(height: 60.v),
          CustomElevatedButton(
            text: "lbl_log_in2".tr,
            rightIcon: Container(
              margin: EdgeInsets.only(left: 30.h),
              child: CustomImageView(
                imagePath: ImageConstant.imgArrowleft,
                height: 17.v,
                width: 22.h,
              ),
            ),
            buttonStyle: CustomButtonStyles.fillAmber,
          ),
          SizedBox(height: 45.v),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 1.v),
                child: Text(
                  "msg_don_t_have_account".tr,
                  style: theme.textTheme.titleSmall,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 1.h),
                child: Text(
                  "lbl_sign_up".tr,
                  style: CustomTextStyles.titleSmallAmber700,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
