// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [verification_successful_one_dialog],
/// and is typically used to hold data that is passed between different parts of the application.
class VerificationSuccessfulOneModel extends Equatable {VerificationSuccessfulOneModel() {  }

VerificationSuccessfulOneModel copyWith() { return VerificationSuccessfulOneModel(
); } 
@override List<Object?> get props => [];
 }
