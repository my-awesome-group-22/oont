// ignore_for_file: must_be_immutable

part of 'verification_successful_one_notifier.dart';

/// Represents the state of VerificationSuccessfulOne in the application.
class VerificationSuccessfulOneState extends Equatable {
  VerificationSuccessfulOneState({this.verificationSuccessfulOneModelObj});

  VerificationSuccessfulOneModel? verificationSuccessfulOneModelObj;

  @override
  List<Object?> get props => [
        verificationSuccessfulOneModelObj,
      ];

  VerificationSuccessfulOneState copyWith(
      {VerificationSuccessfulOneModel? verificationSuccessfulOneModelObj}) {
    return VerificationSuccessfulOneState(
      verificationSuccessfulOneModelObj: verificationSuccessfulOneModelObj ??
          this.verificationSuccessfulOneModelObj,
    );
  }
}
