import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/verification_successful_one_dialog/models/verification_successful_one_model.dart';
part 'verification_successful_one_state.dart';

final verificationSuccessfulOneNotifier = StateNotifierProvider<
    VerificationSuccessfulOneNotifier, VerificationSuccessfulOneState>(
  (ref) => VerificationSuccessfulOneNotifier(VerificationSuccessfulOneState(
    verificationSuccessfulOneModelObj: VerificationSuccessfulOneModel(),
  )),
);

/// A notifier that manages the state of a VerificationSuccessfulOne according to the event that is dispatched to it.
class VerificationSuccessfulOneNotifier
    extends StateNotifier<VerificationSuccessfulOneState> {
  VerificationSuccessfulOneNotifier(VerificationSuccessfulOneState state)
      : super(state) {}
}
