// ignore_for_file: must_be_immutable

part of 'bundle_details_page_notifier.dart';

/// Represents the state of BundleDetailsPage in the application.
class BundleDetailsPageState extends Equatable {
  BundleDetailsPageState({this.bundleDetailsPageModelObj});

  BundleDetailsPageModel? bundleDetailsPageModelObj;

  @override
  List<Object?> get props => [
        bundleDetailsPageModelObj,
      ];

  BundleDetailsPageState copyWith(
      {BundleDetailsPageModel? bundleDetailsPageModelObj}) {
    return BundleDetailsPageState(
      bundleDetailsPageModelObj:
          bundleDetailsPageModelObj ?? this.bundleDetailsPageModelObj,
    );
  }
}
