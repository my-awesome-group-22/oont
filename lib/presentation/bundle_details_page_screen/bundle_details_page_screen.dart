import '../bundle_details_page_screen/widgets/productcardlist_item_widget.dart';import 'models/productcardlist_item_model.dart';import 'notifier/bundle_details_page_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';class BundleDetailsPageScreen extends ConsumerStatefulWidget {const BundleDetailsPageScreen({Key? key}) : super(key: key);

@override BundleDetailsPageScreenState createState() =>  BundleDetailsPageScreenState();

 }
class BundleDetailsPageScreenState extends ConsumerState<BundleDetailsPageScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: SizedBox(width: double.maxFinite, child: Column(children: [SizedBox(height: 29.v), Expanded(child: SingleChildScrollView(child: Padding(padding: EdgeInsets.only(left: 20.h, right: 20.h, bottom: 5.v), child: Column(children: [_buildProductCardList(context), SizedBox(height: 16.v), _buildTotalItemRow(context), SizedBox(height: 12.v), _buildPriceRow(context, priceLabel: "lbl_weight".tr, priceText: "lbl_33_kg".tr), SizedBox(height: 9.v), _buildPriceRow(context, priceLabel: "lbl_price".tr, priceText: "lbl_82_25".tr), SizedBox(height: 10.v), _buildPriceRow(context, priceLabel: "lbl_discount".tr, priceText: "lbl_12_25".tr), SizedBox(height: 15.v), _buildTotalPriceColumn(context)]))))])), bottomNavigationBar: _buildCreateBundlePackButton(context))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_bundle_details".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildProductCardList(BuildContext context) { return Consumer(builder: (context, ref, _) {return ListView.separated(physics: NeverScrollableScrollPhysics(), shrinkWrap: true, separatorBuilder: (context, index) {return Padding(padding: EdgeInsets.symmetric(vertical: 9.5.v), child: SizedBox(width: 374.h, child: Divider(height: 1.v, thickness: 1.v, color: appTheme.gray20001)));}, itemCount: ref.watch(bundleDetailsPageNotifier).bundleDetailsPageModelObj?.productcardlistItemList.length ?? 0, itemBuilder: (context, index) {ProductcardlistItemModel model = ref.watch(bundleDetailsPageNotifier).bundleDetailsPageModelObj?.productcardlistItemList[index] ?? ProductcardlistItemModel(); return ProductcardlistItemWidget(model);});}); } 
/// Section Widget
Widget _buildTotalItemRow(BuildContext context) { return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Opacity(opacity: 0.7, child: Text("lbl_total_item".tr, style: CustomTextStyles.titleMediumGray900)), Text("lbl_62".tr, style: CustomTextStyles.titleMediumBold_2)]); } 
/// Section Widget
Widget _buildTotalPriceColumn(BuildContext context) { return Column(children: [Divider(color: appTheme.gray900), SizedBox(height: 9.v), _buildPriceRow(context, priceLabel: "lbl_total_price".tr, priceText: "lbl_70_00".tr)]); } 
/// Section Widget
Widget _buildCreateBundlePackButton(BuildContext context) { return CustomElevatedButton(text: "msg_create_bundle_pack".tr, margin: EdgeInsets.only(left: 30.h, right: 30.h, bottom: 30.v), onPressed: () {onTapCreateBundlePackButton(context);}); } 
/// Common widget
Widget _buildPriceRow(BuildContext context, {required String priceLabel, required String priceText, }) { return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Opacity(opacity: 0.7, child: Text(priceLabel, style: CustomTextStyles.titleMediumGray900.copyWith(color: appTheme.gray900.withOpacity(0.6)))), Text(priceText, style: CustomTextStyles.titleMediumBold_2.copyWith(color: appTheme.gray900))]); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
/// Navigates to the checkoutScreen when the action is triggered.
onTapCreateBundlePackButton(BuildContext context) { NavigatorService.pushNamed(AppRoutes.checkoutScreen, ); } 
 }
