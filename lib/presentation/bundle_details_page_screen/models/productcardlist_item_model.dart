import '../../../core/app_export.dart';/// This class is used in the [productcardlist_item_widget] screen.
class ProductcardlistItemModel {ProductcardlistItemModel({this.productImage, this.productName, this.productWeight, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.imgGroup3473;productName = productName  ?? "Caulifiower\nFrom The Garden";productWeight = productWeight  ?? "2 Kg";productPrice = productPrice  ?? "14";id = id  ?? ""; }

String? productImage;

String? productName;

String? productWeight;

String? productPrice;

String? id;

 }
