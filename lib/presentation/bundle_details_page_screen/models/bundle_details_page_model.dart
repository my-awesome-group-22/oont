// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'productcardlist_item_model.dart';/// This class defines the variables used in the [bundle_details_page_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class BundleDetailsPageModel extends Equatable {BundleDetailsPageModel({this.productcardlistItemList = const []}) {  }

List<ProductcardlistItemModel> productcardlistItemList;

BundleDetailsPageModel copyWith({List<ProductcardlistItemModel>? productcardlistItemList}) { return BundleDetailsPageModel(
productcardlistItemList : productcardlistItemList ?? this.productcardlistItemList,
); } 
@override List<Object?> get props => [productcardlistItemList];
 }
