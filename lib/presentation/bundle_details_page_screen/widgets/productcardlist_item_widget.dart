import '../models/productcardlist_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class ProductcardlistItemWidget extends StatelessWidget {
  ProductcardlistItemWidget(
    this.productcardlistItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ProductcardlistItemModel productcardlistItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomImageView(
          imagePath: productcardlistItemModelObj?.productImage,
          height: 71.v,
          width: 80.h,
        ),
        Container(
          width: 136.h,
          margin: EdgeInsets.only(
            left: 20.h,
            top: 12.v,
            bottom: 22.v,
          ),
          child: Text(
            productcardlistItemModelObj.productName!,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: theme.textTheme.titleMedium,
          ),
        ),
        Spacer(),
        Padding(
          padding: EdgeInsets.only(
            top: 27.v,
            bottom: 28.v,
          ),
          child: Text(
            productcardlistItemModelObj.productWeight!,
            style: CustomTextStyles.titleSmallGray900,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 28.h,
            top: 24.v,
            bottom: 29.v,
          ),
          child: Text(
            productcardlistItemModelObj.productPrice!,
            style: CustomTextStyles.titleMediumBold_2,
          ),
        ),
      ],
    );
  }
}
