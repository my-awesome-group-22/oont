import '../models/staggeredcolumn_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class StaggeredcolumnItemWidget extends StatelessWidget {
  StaggeredcolumnItemWidget(
    this.staggeredcolumnItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  StaggeredcolumnItemModel staggeredcolumnItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(11.h),
      decoration: AppDecoration.outlineGray20001.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder21,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 3.v),
          Padding(
            padding: EdgeInsets.only(left: 3.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: staggeredcolumnItemModelObj?.biscuitImage,
                  height: 80.v,
                  width: 67.h,
                  alignment: Alignment.center,
                ),
                SizedBox(height: 13.v),
                SizedBox(
                  width: 76.h,
                  child: Text(
                    staggeredcolumnItemModelObj.biscuitName!,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: theme.textTheme.titleMedium!.copyWith(
                      height: 1.38,
                    ),
                  ),
                ),
                SizedBox(height: 3.v),
                Opacity(
                  opacity: 0.8,
                  child: Text(
                    staggeredcolumnItemModelObj.biscuitWeight!,
                    style: CustomTextStyles.titleSmallOnError,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 9.v),
          Padding(
            padding: EdgeInsets.only(left: 3.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: 2.v,
                    bottom: 8.v,
                  ),
                  child: Text(
                    staggeredcolumnItemModelObj.price!,
                    style: CustomTextStyles.titleMediumBold_2,
                  ),
                ),
                CustomIconButton(
                  height: 28.adaptSize,
                  width: 28.adaptSize,
                  padding: EdgeInsets.all(7.h),
                  child: CustomImageView(
                    imagePath: ImageConstant.imgClose,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
