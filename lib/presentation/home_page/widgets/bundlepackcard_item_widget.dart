import '../models/bundlepackcard_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class BundlepackcardItemWidget extends StatelessWidget {
  BundlepackcardItemWidget(
    this.bundlepackcardItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  BundlepackcardItemModel bundlepackcardItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(11.h),
      decoration: AppDecoration.outlineGray20001.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder21,
      ),
      width: 184.h,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(height: 7.v),
          CustomImageView(
            imagePath: bundlepackcardItemModelObj?.bundlePackImage,
            height: 84.adaptSize,
            width: 84.adaptSize,
            alignment: Alignment.center,
          ),
          SizedBox(height: 14.v),
          Padding(
            padding: EdgeInsets.only(left: 3.h),
            child: Text(
              bundlepackcardItemModelObj.bundlePackText!,
              style: theme.textTheme.titleMedium,
            ),
          ),
          SizedBox(height: 7.v),
          Opacity(
            opacity: 0.8,
            child: Padding(
              padding: EdgeInsets.only(left: 3.h),
              child: Text(
                bundlepackcardItemModelObj.ingredientsText!,
                style: CustomTextStyles.titleSmallOnError,
              ),
            ),
          ),
          SizedBox(height: 7.v),
          Padding(
            padding: EdgeInsets.only(left: 3.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: 6.v,
                    bottom: 4.v,
                  ),
                  child: Text(
                    bundlepackcardItemModelObj.priceText!,
                    style: CustomTextStyles.titleMediumBold_2,
                  ),
                ),
                Opacity(
                  opacity: 0.6,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: 13.h,
                      top: 6.v,
                      bottom: 4.v,
                    ),
                    child: Text(
                      bundlepackcardItemModelObj.discountedPriceText!,
                      style: CustomTextStyles.bodyLargeOnError.copyWith(
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 44.h),
                  child: CustomIconButton(
                    height: 28.adaptSize,
                    width: 28.adaptSize,
                    padding: EdgeInsets.all(7.h),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgClose,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
