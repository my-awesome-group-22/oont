// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'bundlepackcard_item_model.dart';import 'staggeredcolumn_item_model.dart';/// This class defines the variables used in the [home_page],
/// and is typically used to hold data that is passed between different parts of the application.
class HomeModel extends Equatable {HomeModel({this.bundlepackcardItemList = const [], this.staggeredcolumnItemList = const [], }) {  }

List<BundlepackcardItemModel> bundlepackcardItemList;

List<StaggeredcolumnItemModel> staggeredcolumnItemList;

HomeModel copyWith({List<BundlepackcardItemModel>? bundlepackcardItemList, List<StaggeredcolumnItemModel>? staggeredcolumnItemList, }) { return HomeModel(
bundlepackcardItemList : bundlepackcardItemList ?? this.bundlepackcardItemList,
staggeredcolumnItemList : staggeredcolumnItemList ?? this.staggeredcolumnItemList,
); } 
@override List<Object?> get props => [bundlepackcardItemList,staggeredcolumnItemList];
 }
