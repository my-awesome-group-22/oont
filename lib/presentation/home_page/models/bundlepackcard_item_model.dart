import '../../../core/app_export.dart';/// This class is used in the [bundlepackcard_item_widget] screen.
class BundlepackcardItemModel {BundlepackcardItemModel({this.bundlePackImage, this.bundlePackText, this.ingredientsText, this.priceText, this.discountedPriceText, this.id, }) { bundlePackImage = bundlePackImage  ?? ImageConstant.imgImage5;bundlePackText = bundlePackText  ?? "Bundle Pack";ingredientsText = ingredientsText  ?? "Onion,Oil,Salt...";priceText = priceText  ?? "£35";discountedPriceText = discountedPriceText  ?? "50.32";id = id  ?? ""; }

String? bundlePackImage;

String? bundlePackText;

String? ingredientsText;

String? priceText;

String? discountedPriceText;

String? id;

 }
