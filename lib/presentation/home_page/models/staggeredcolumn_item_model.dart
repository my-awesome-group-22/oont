import '../../../core/app_export.dart';/// This class is used in the [staggeredcolumn_item_widget] screen.
class StaggeredcolumnItemModel {StaggeredcolumnItemModel({this.biscuitImage, this.biscuitName, this.biscuitWeight, this.price, this.id, }) { biscuitImage = biscuitImage  ?? ImageConstant.img6804425Biscuit;biscuitName = biscuitName  ?? "Girl Guide\nBiscuits";biscuitWeight = biscuitWeight  ?? "1000 GM";price = price  ?? "£10";id = id  ?? ""; }

String? biscuitImage;

String? biscuitName;

String? biscuitWeight;

String? price;

String? id;

 }
