import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/bundlepackcard_item_model.dart';
import '../models/staggeredcolumn_item_model.dart';
import 'package:oont/presentation/home_page/models/home_model.dart';
part 'home_state.dart';

final homeNotifier = StateNotifierProvider<HomeNotifier, HomeState>(
  (ref) => HomeNotifier(HomeState(
    homeModelObj: HomeModel(bundlepackcardItemList: [
      BundlepackcardItemModel(
          bundlePackImage: ImageConstant.imgImage5,
          bundlePackText: "Bundle Pack",
          ingredientsText: "Onion,Oil,Salt...",
          priceText: "£35",
          discountedPriceText: "50.32"),
      BundlepackcardItemModel(
          bundlePackImage: ImageConstant.imgNicepngGrocery,
          bundlePackText: "Bundle Pack",
          ingredientsText: "Onion,Oil,Salt...",
          priceText: "£35",
          discountedPriceText: "50.32")
    ], staggeredcolumnItemList: [
      StaggeredcolumnItemModel(
          biscuitImage: ImageConstant.img6804425Biscuit,
          biscuitName: "Girl Guide\nBiscuits",
          biscuitWeight: "1000 GM",
          price: "£10")
    ]),
  )),
);

/// A notifier that manages the state of a Home according to the event that is dispatched to it.
class HomeNotifier extends StateNotifier<HomeState> {
  HomeNotifier(HomeState state) : super(state) {}
}
