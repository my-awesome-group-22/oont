import '../home_page/widgets/bundlepackcard_item_widget.dart';
import '../home_page/widgets/staggeredcolumn_item_widget.dart';
import 'models/bundlepackcard_item_model.dart';
import 'models/staggeredcolumn_item_model.dart';
import 'notifier/home_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/app_bar/appbar_leading_iconbutton.dart';
import 'package:oont/widgets/app_bar/appbar_subtitle_three.dart';
import 'package:oont/widgets/app_bar/appbar_subtitle_two.dart';
import 'package:oont/widgets/app_bar/appbar_title_image.dart';
import 'package:oont/widgets/app_bar/appbar_trailing_iconbutton.dart';
import 'package:oont/widgets/app_bar/appbar_trailing_image.dart';
import 'package:oont/widgets/app_bar/custom_app_bar.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';

// ignore_for_file: must_be_immutable
class HomePage extends ConsumerStatefulWidget {
  const HomePage({Key? key})
      : super(
          key: key,
        );

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends ConsumerState<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          decoration: AppDecoration.fillWhiteA,
          child: Column(
            children: [
              SizedBox(height: 11.v),
              _buildHorizontalScroll(context),
              SizedBox(height: 2.v),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.h),
                    child: Column(
                      children: [
                        _buildPopularPackColumn(context),
                        SizedBox(height: 27.v),
                        SizedBox(
                          height: 469.v,
                          width: 382.h,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              CustomImageView(
                                imagePath: ImageConstant.imgPlus,
                                height: 15.adaptSize,
                                width: 15.adaptSize,
                                alignment: Alignment.bottomRight,
                                margin: EdgeInsets.only(
                                  right: 30.h,
                                  bottom: 41.v,
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(left: 4.h),
                                      child: _buildNewItemsRow(context),
                                    ),
                                    SizedBox(height: 19.v),
                                    _buildStaggeredColumn(context),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildHorizontalScroll(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: IntrinsicWidth(
        child: SizedBox(
          height: 280.v,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgRectangle428,
                height: 180.v,
                width: 374.h,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 27.v),
              ),
              CustomImageView(
                imagePath: ImageConstant.imgRectangle2253,
                height: 180.v,
                width: 374.h,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 27.v),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: 41.v,
                  width: 60.h,
                  margin: EdgeInsets.only(
                    left: 20.h,
                    bottom: 24.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img43x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: 49.v,
                  width: 53.h,
                  margin: EdgeInsets.only(
                    left: 155.h,
                    bottom: 27.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img83x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 243.adaptSize,
                  width: 243.adaptSize,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img5973x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 64.v,
                  width: 85.h,
                  margin: EdgeInsets.only(
                    right: 120.h,
                    bottom: 20.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img537376GrainP,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 41.v,
                  width: 47.h,
                  margin: EdgeInsets.only(top: 73.v),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.imgAvocado3x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  height: 153.v,
                  width: 158.h,
                  margin: EdgeInsets.only(top: 9.v),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.imgFavpngFarmers,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              CustomAppBar(
                height: 47.v,
                leadingWidth: 58.h,
                leading: AppbarLeadingIconbutton(
                  imagePath: ImageConstant.imgTelevisionGray90040x40,
                  margin: EdgeInsets.only(
                    left: 18.h,
                    bottom: 7.v,
                  ),
                ),
                title: Padding(
                  padding: EdgeInsets.only(left: 58.h),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          AppbarTitleImage(
                            imagePath: ImageConstant.imgLinkedinOnprimary,
                            margin: EdgeInsets.only(bottom: 16.v),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: 5.h,
                              top: 3.v,
                            ),
                            child: Column(
                              children: [
                                AppbarSubtitleThree(
                                  text: "msg_current_location".tr,
                                ),
                                SizedBox(height: 5.v),
                                AppbarSubtitleTwo(
                                  text: "lbl_london_uk".tr,
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 18.h),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                actions: [
                  AppbarTrailingImage(
                    imagePath: ImageConstant.imgArrowDownGreen500,
                    margin: EdgeInsets.fromLTRB(4.h, 15.v, 7.h, 18.v),
                  ),
                  AppbarTrailingIconbutton(
                    imagePath: ImageConstant.imgSearchGray90024x24,
                    margin: EdgeInsets.only(
                      left: 70.h,
                      right: 25.h,
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 42.h,
                    bottom: 75.v,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 151.h,
                        child: Text(
                          "msg_order_your_daily".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style:
                              CustomTextStyles.titleLargeRubikBlack900.copyWith(
                            height: 1.25,
                          ),
                        ),
                      ),
                      SizedBox(height: 7.v),
                      Text(
                        "lbl_easydelivery".tr,
                        style: CustomTextStyles.titleSmallRubikGreen500,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildPopularPackColumn(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 4.h),
          child: _buildNewItemsRow(context),
        ),
        SizedBox(height: 18.v),
        SizedBox(
          height: 205.v,
          child: Consumer(
            builder: (context, ref, _) {
              return ListView.separated(
                scrollDirection: Axis.horizontal,
                separatorBuilder: (
                  context,
                  index,
                ) {
                  return SizedBox(
                    width: 14.h,
                  );
                },
                itemCount: ref
                        .watch(homeNotifier)
                        .homeModelObj
                        ?.bundlepackcardItemList
                        .length ??
                    0,
                itemBuilder: (context, index) {
                  BundlepackcardItemModel model = ref
                          .watch(homeNotifier)
                          .homeModelObj
                          ?.bundlepackcardItemList[index] ??
                      BundlepackcardItemModel();
                  return BundlepackcardItemWidget(
                    model,
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildStaggeredColumn(BuildContext context) {
    return Consumer(
      builder: (context, ref, _) {
        return StaggeredGridView.countBuilder(
          shrinkWrap: true,
          primary: false,
          crossAxisCount: 4,
          crossAxisSpacing: 14.h,
          mainAxisSpacing: 14.h,
          staggeredTileBuilder: (index) {
            return StaggeredTile.fit(2);
          },
          itemCount: ref
                  .watch(homeNotifier)
                  .homeModelObj
                  ?.staggeredcolumnItemList
                  .length ??
              0,
          itemBuilder: (context, index) {
            StaggeredcolumnItemModel model = ref
                    .watch(homeNotifier)
                    .homeModelObj
                    ?.staggeredcolumnItemList[index] ??
                StaggeredcolumnItemModel();
            return StaggeredcolumnItemWidget(
              model,
            );
          },
        );
      },
    );
  }

  /// Common widget
  Widget _buildNewItemsRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "lbl_new_items2".tr,
          style: CustomTextStyles.titleMedium18,
        ),
        Padding(
          padding: EdgeInsets.only(top: 4.v),
          child: Text(
            "lbl_view_all".tr,
            style: CustomTextStyles.titleSmallPrimaryBold,
          ),
        ),
      ],
    );
  }
}
