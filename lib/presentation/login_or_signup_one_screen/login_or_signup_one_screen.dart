import 'notifier/login_or_signup_one_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class LoginOrSignupOneScreen extends ConsumerStatefulWidget {
  const LoginOrSignupOneScreen({Key? key})
      : super(
          key: key,
        );

  @override
  LoginOrSignupOneScreenState createState() => LoginOrSignupOneScreenState();
}

class LoginOrSignupOneScreenState
    extends ConsumerState<LoginOrSignupOneScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray50,
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 20.h,
            vertical: 64.v,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 19.v),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.h),
                  child: Text(
                    "lbl_welcome_to_oont".tr,
                    style: CustomTextStyles.displaySmallPrimaryContainer,
                  ),
                ),
              ),
              Spacer(
                flex: 69,
              ),
              CustomImageView(
                imagePath: ImageConstant.imgOontGray50,
                height: 88.v,
                width: 305.h,
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(left: 25.h),
              ),
              Spacer(
                flex: 30,
              ),
              CustomElevatedButton(
                text: "lbl_oont_grocery".tr,
              ),
              SizedBox(height: 20.v),
              CustomElevatedButton(
                text: "lbl_oont_transport".tr,
                buttonStyle: CustomButtonStyles.fillLightBlueA,
                buttonTextStyle: CustomTextStyles.titleMediumBlack900,
              ),
              SizedBox(height: 24.v),
              CustomElevatedButton(
                text: "msg_oont_food_delivery".tr,
                buttonStyle: CustomButtonStyles.fillAmber,
                buttonTextStyle: CustomTextStyles.titleMediumBold_1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
