// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [login_or_signup_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class LoginOrSignupOneModel extends Equatable {LoginOrSignupOneModel() {  }

LoginOrSignupOneModel copyWith() { return LoginOrSignupOneModel(
); } 
@override List<Object?> get props => [];
 }
