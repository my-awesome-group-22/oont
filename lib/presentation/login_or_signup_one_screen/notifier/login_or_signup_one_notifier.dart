import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/login_or_signup_one_screen/models/login_or_signup_one_model.dart';
part 'login_or_signup_one_state.dart';

final loginOrSignupOneNotifier =
    StateNotifierProvider<LoginOrSignupOneNotifier, LoginOrSignupOneState>(
  (ref) => LoginOrSignupOneNotifier(LoginOrSignupOneState(
    loginOrSignupOneModelObj: LoginOrSignupOneModel(),
  )),
);

/// A notifier that manages the state of a LoginOrSignupOne according to the event that is dispatched to it.
class LoginOrSignupOneNotifier extends StateNotifier<LoginOrSignupOneState> {
  LoginOrSignupOneNotifier(LoginOrSignupOneState state) : super(state) {}
}
