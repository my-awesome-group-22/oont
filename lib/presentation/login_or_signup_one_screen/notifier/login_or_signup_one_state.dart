// ignore_for_file: must_be_immutable

part of 'login_or_signup_one_notifier.dart';

/// Represents the state of LoginOrSignupOne in the application.
class LoginOrSignupOneState extends Equatable {
  LoginOrSignupOneState({this.loginOrSignupOneModelObj});

  LoginOrSignupOneModel? loginOrSignupOneModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupOneModelObj,
      ];

  LoginOrSignupOneState copyWith(
      {LoginOrSignupOneModel? loginOrSignupOneModelObj}) {
    return LoginOrSignupOneState(
      loginOrSignupOneModelObj:
          loginOrSignupOneModelObj ?? this.loginOrSignupOneModelObj,
    );
  }
}
