import '../models/productcard_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class ProductcardItemWidget extends StatelessWidget {
  ProductcardItemWidget(
    this.productcardItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ProductcardItemModel productcardItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(11.h),
      decoration: AppDecoration.outlineGray20001.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder21,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(height: 14.v),
          CustomImageView(
            imagePath: productcardItemModelObj?.productImage,
            height: 57.v,
            width: 80.h,
            alignment: Alignment.center,
          ),
          SizedBox(height: 24.v),
          Container(
            width: 139.h,
            margin: EdgeInsets.only(left: 3.h),
            child: Text(
              productcardItemModelObj.productName!,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: theme.textTheme.titleMedium!.copyWith(
                height: 1.38,
              ),
            ),
          ),
          SizedBox(height: 2.v),
          Opacity(
            opacity: 0.8,
            child: Padding(
              padding: EdgeInsets.only(left: 3.h),
              child: Text(
                productcardItemModelObj.productWeight!,
                style: CustomTextStyles.titleSmallOnError,
              ),
            ),
          ),
          SizedBox(height: 9.v),
          Padding(
            padding: EdgeInsets.only(left: 3.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: 2.v,
                    bottom: 8.v,
                  ),
                  child: Text(
                    productcardItemModelObj.productPrice!,
                    style: CustomTextStyles.titleMediumBold_2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 102.h),
                  child: CustomIconButton(
                    height: 28.adaptSize,
                    width: 28.adaptSize,
                    padding: EdgeInsets.all(7.h),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgClose,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
