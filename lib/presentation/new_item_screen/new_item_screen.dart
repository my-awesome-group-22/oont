import '../new_item_screen/widgets/productcard_item_widget.dart';import 'models/productcard_item_model.dart';import 'notifier/new_item_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';class NewItemScreen extends ConsumerStatefulWidget {const NewItemScreen({Key? key}) : super(key: key);

@override NewItemScreenState createState() =>  NewItemScreenState();

 }
class NewItemScreenState extends ConsumerState<NewItemScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Padding(padding: EdgeInsets.only(left: 16.h, top: 25.v, right: 16.h), child: Consumer(builder: (context, ref, _) {return GridView.builder(shrinkWrap: true, gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(mainAxisExtent: 221.v, crossAxisCount: 2, mainAxisSpacing: 14.h, crossAxisSpacing: 14.h), physics: BouncingScrollPhysics(), itemCount: ref.watch(newItemNotifier).newItemModelObj?.productcardItemList.length ?? 0, itemBuilder: (context, index) {ProductcardItemModel model = ref.watch(newItemNotifier).newItemModelObj?.productcardItemList[index] ?? ProductcardItemModel(); return ProductcardItemWidget(model);});})))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_new_items".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
