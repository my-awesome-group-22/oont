// ignore_for_file: must_be_immutable

part of 'new_item_notifier.dart';

/// Represents the state of NewItem in the application.
class NewItemState extends Equatable {
  NewItemState({this.newItemModelObj});

  NewItemModel? newItemModelObj;

  @override
  List<Object?> get props => [
        newItemModelObj,
      ];

  NewItemState copyWith({NewItemModel? newItemModelObj}) {
    return NewItemState(
      newItemModelObj: newItemModelObj ?? this.newItemModelObj,
    );
  }
}
