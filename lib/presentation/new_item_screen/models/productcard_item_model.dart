import '../../../core/app_export.dart';/// This class is used in the [productcard_item_widget] screen.
class ProductcardItemModel {ProductcardItemModel({this.productImage, this.productName, this.productWeight, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.imgFriendsgiving13x;productName = productName  ?? "Perry’s Ice Cream\nBanana";productWeight = productWeight  ?? "800 Gm";productPrice = productPrice  ?? "13";id = id  ?? ""; }

String? productImage;

String? productName;

String? productWeight;

String? productPrice;

String? id;

 }
