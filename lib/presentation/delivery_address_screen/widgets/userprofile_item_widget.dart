import '../models/userprofile_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class UserprofileItemWidget extends StatelessWidget {
  UserprofileItemWidget(
    this.userprofileItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  UserprofileItemModel userprofileItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomImageView(
          imagePath: userprofileItemModelObj?.cityImage,
          height: 16.adaptSize,
          width: 16.adaptSize,
          margin: EdgeInsets.only(
            top: 2.v,
            bottom: 64.v,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 20.h,
            bottom: 17.v,
          ),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  userprofileItemModelObj.customText!,
                  style: theme.textTheme.titleMedium,
                ),
              ),
              SizedBox(height: 5.v),
              Text(
                userprofileItemModelObj.addressText!,
                style: CustomTextStyles.titleMediumOnError_1,
              ),
              SizedBox(height: 8.v),
              Text(
                userprofileItemModelObj.phoneNumber!,
                style: theme.textTheme.titleMedium,
              ),
            ],
          ),
        ),
        Spacer(),
        Padding(
          padding: EdgeInsets.only(
            top: 3.v,
            bottom: 17.v,
          ),
          child: Column(
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgGroup3814,
                height: 14.adaptSize,
                width: 14.adaptSize,
              ),
              SizedBox(height: 33.v),
              CustomImageView(
                imagePath: ImageConstant.imgGroup3813,
                height: 14.adaptSize,
                width: 14.adaptSize,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
