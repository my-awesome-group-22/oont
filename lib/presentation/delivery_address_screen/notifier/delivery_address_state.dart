// ignore_for_file: must_be_immutable

part of 'delivery_address_notifier.dart';

/// Represents the state of DeliveryAddress in the application.
class DeliveryAddressState extends Equatable {
  DeliveryAddressState({this.deliveryAddressModelObj});

  DeliveryAddressModel? deliveryAddressModelObj;

  @override
  List<Object?> get props => [
        deliveryAddressModelObj,
      ];

  DeliveryAddressState copyWith(
      {DeliveryAddressModel? deliveryAddressModelObj}) {
    return DeliveryAddressState(
      deliveryAddressModelObj:
          deliveryAddressModelObj ?? this.deliveryAddressModelObj,
    );
  }
}
