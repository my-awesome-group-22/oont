import '../../../core/app_export.dart';/// This class is used in the [userprofile_item_widget] screen.
class UserprofileItemModel {UserprofileItemModel({this.cityImage, this.customText, this.addressText, this.phoneNumber, this.id, }) { cityImage = cityImage  ?? ImageConstant.imgContrastPrimary;customText = customText  ?? "London";addressText = addressText  ?? "216/c East Road";phoneNumber = phoneNumber  ?? "+8801710071000";id = id  ?? ""; }

String? cityImage;

String? customText;

String? addressText;

String? phoneNumber;

String? id;

 }
