import '../delivery_address_screen/widgets/userprofile_item_widget.dart';import 'models/userprofile_item_model.dart';import 'notifier/delivery_address_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_floating_button.dart';class DeliveryAddressScreen extends ConsumerStatefulWidget {const DeliveryAddressScreen({Key? key}) : super(key: key);

@override DeliveryAddressScreenState createState() =>  DeliveryAddressScreenState();

 }
class DeliveryAddressScreenState extends ConsumerState<DeliveryAddressScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(body: SizedBox(height: 852.v, width: double.maxFinite, child: Stack(alignment: Alignment.center, children: [_buildFloatingActionButton(context), Align(alignment: Alignment.center, child: Container(decoration: AppDecoration.fillGray10003, child: Column(mainAxisSize: MainAxisSize.min, children: [_buildAppBar(context), SizedBox(height: 30.v), Container(margin: EdgeInsets.symmetric(horizontal: 16.h), padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 48.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: _buildUserProfile(context)), SizedBox(height: 56.v)])))])))); } 
/// Section Widget
Widget _buildFloatingActionButton(BuildContext context) { return CustomFloatingButton(height: 44, width: 44, backgroundColor: theme.colorScheme.primary, decoration: FloatingButtonStyleHelper.fillPrimaryTL22, alignment: Alignment.bottomRight, child: CustomImageView(imagePath: ImageConstant.imgPlusWhiteA700, height: 22.0.v, width: 22.0.h)); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "msg_delivery_address".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildUserProfile(BuildContext context) { return Consumer(builder: (context, ref, _) {return ListView.separated(physics: NeverScrollableScrollPhysics(), shrinkWrap: true, separatorBuilder: (context, index) {return Padding(padding: EdgeInsets.symmetric(vertical: 13.0.v), child: SizedBox(width: 342.h, child: Divider(height: 1.v, thickness: 1.v, color: appTheme.gray20001)));}, itemCount: ref.watch(deliveryAddressNotifier).deliveryAddressModelObj?.userprofileItemList.length ?? 0, itemBuilder: (context, index) {UserprofileItemModel model = ref.watch(deliveryAddressNotifier).deliveryAddressModelObj?.userprofileItemList[index] ?? UserprofileItemModel(); return UserprofileItemWidget(model);});}); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
