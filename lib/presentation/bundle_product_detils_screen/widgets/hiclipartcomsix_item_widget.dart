import '../models/hiclipartcomsix_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class HiclipartcomsixItemWidget extends StatelessWidget {
  HiclipartcomsixItemWidget(
    this.hiclipartcomsixItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  HiclipartcomsixItemModel hiclipartcomsixItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: EdgeInsets.all(15.h),
        decoration: AppDecoration.fillGray300011.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder24,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomImageView(
              imagePath: ImageConstant.imgHiclipartCom6,
              height: 204.v,
              width: 186.h,
              margin: EdgeInsets.only(
                top: 30.v,
                bottom: 56.v,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 35.h,
                bottom: 242.v,
              ),
              child: CustomIconButton(
                height: 48.adaptSize,
                width: 48.adaptSize,
                padding: EdgeInsets.all(14.h),
                decoration: IconButtonStyleHelper.fillWhiteA,
                child: CustomImageView(
                  imagePath: ImageConstant.imgFavorite,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
