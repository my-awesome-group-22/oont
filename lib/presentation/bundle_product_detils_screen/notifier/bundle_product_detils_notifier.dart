import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/hiclipartcomsix_item_model.dart';import 'package:oont/presentation/bundle_product_detils_screen/models/bundle_product_detils_model.dart';part 'bundle_product_detils_state.dart';final bundleProductDetilsNotifier = StateNotifierProvider<BundleProductDetilsNotifier, BundleProductDetilsState>((ref) => BundleProductDetilsNotifier(BundleProductDetilsState(sliderIndex: 0, bundleProductDetilsModelObj: BundleProductDetilsModel(hiclipartcomsixItemList: List.generate(1, (index) => HiclipartcomsixItemModel())))));
/// A notifier that manages the state of a BundleProductDetils according to the event that is dispatched to it.
class BundleProductDetilsNotifier extends StateNotifier<BundleProductDetilsState> {BundleProductDetilsNotifier(BundleProductDetilsState state) : super(state);

 }
