// ignore_for_file: must_be_immutable

part of 'bundle_product_detils_notifier.dart';

/// Represents the state of BundleProductDetils in the application.
class BundleProductDetilsState extends Equatable {
  BundleProductDetilsState({
    this.sliderIndex = 0,
    this.bundleProductDetilsModelObj,
  });

  BundleProductDetilsModel? bundleProductDetilsModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        bundleProductDetilsModelObj,
      ];

  BundleProductDetilsState copyWith({
    int? sliderIndex,
    BundleProductDetilsModel? bundleProductDetilsModelObj,
  }) {
    return BundleProductDetilsState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      bundleProductDetilsModelObj:
          bundleProductDetilsModelObj ?? this.bundleProductDetilsModelObj,
    );
  }
}
