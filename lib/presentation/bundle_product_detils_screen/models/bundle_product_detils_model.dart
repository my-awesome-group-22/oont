// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'hiclipartcomsix_item_model.dart';/// This class defines the variables used in the [bundle_product_detils_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class BundleProductDetilsModel extends Equatable {BundleProductDetilsModel({this.hiclipartcomsixItemList = const []}) {  }

List<HiclipartcomsixItemModel> hiclipartcomsixItemList;

BundleProductDetilsModel copyWith({List<HiclipartcomsixItemModel>? hiclipartcomsixItemList}) { return BundleProductDetilsModel(
hiclipartcomsixItemList : hiclipartcomsixItemList ?? this.hiclipartcomsixItemList,
); } 
@override List<Object?> get props => [hiclipartcomsixItemList];
 }
