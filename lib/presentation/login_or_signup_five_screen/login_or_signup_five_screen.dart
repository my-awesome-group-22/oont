import 'notifier/login_or_signup_five_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class LoginOrSignupFiveScreen extends ConsumerStatefulWidget {
  const LoginOrSignupFiveScreen({Key? key})
      : super(
          key: key,
        );

  @override
  LoginOrSignupFiveScreenState createState() => LoginOrSignupFiveScreenState();
}

class LoginOrSignupFiveScreenState
    extends ConsumerState<LoginOrSignupFiveScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        body: SizedBox(
          height: SizeUtils.height,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.center,
            children: [
              CustomImageView(
                imagePath: ImageConstant.imgImage38,
                height: 782.v,
                width: 414.h,
                alignment: Alignment.topCenter,
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.h,
                    vertical: 46.v,
                  ),
                  decoration: AppDecoration.gradientWhiteAToBlack,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(height: 77.v),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 10.h),
                          child: Text(
                            "lbl_welcome_to_oont".tr,
                            style: theme.textTheme.displaySmall,
                          ),
                        ),
                      ),
                      Spacer(),
                      CustomElevatedButton(
                        text: "msg_continue_with_email".tr,
                      ),
                      SizedBox(height: 20.v),
                      CustomElevatedButton(
                        text: "msg_continue_as_a_deliverer".tr,
                        buttonStyle: CustomButtonStyles.fillAmber,
                        buttonTextStyle: CustomTextStyles.titleMediumBold_1,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
