// ignore_for_file: must_be_immutable

part of 'login_or_signup_five_notifier.dart';

/// Represents the state of LoginOrSignupFive in the application.
class LoginOrSignupFiveState extends Equatable {
  LoginOrSignupFiveState({this.loginOrSignupFiveModelObj});

  LoginOrSignupFiveModel? loginOrSignupFiveModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupFiveModelObj,
      ];

  LoginOrSignupFiveState copyWith(
      {LoginOrSignupFiveModel? loginOrSignupFiveModelObj}) {
    return LoginOrSignupFiveState(
      loginOrSignupFiveModelObj:
          loginOrSignupFiveModelObj ?? this.loginOrSignupFiveModelObj,
    );
  }
}
