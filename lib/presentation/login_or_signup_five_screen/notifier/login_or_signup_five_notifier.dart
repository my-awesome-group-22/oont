import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/login_or_signup_five_screen/models/login_or_signup_five_model.dart';
part 'login_or_signup_five_state.dart';

final loginOrSignupFiveNotifier =
    StateNotifierProvider<LoginOrSignupFiveNotifier, LoginOrSignupFiveState>(
  (ref) => LoginOrSignupFiveNotifier(LoginOrSignupFiveState(
    loginOrSignupFiveModelObj: LoginOrSignupFiveModel(),
  )),
);

/// A notifier that manages the state of a LoginOrSignupFive according to the event that is dispatched to it.
class LoginOrSignupFiveNotifier extends StateNotifier<LoginOrSignupFiveState> {
  LoginOrSignupFiveNotifier(LoginOrSignupFiveState state) : super(state) {}
}
