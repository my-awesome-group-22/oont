// ignore_for_file: must_be_immutable

part of 'error_page_phone_number_notifier.dart';

/// Represents the state of ErrorPagePhoneNumber in the application.
class ErrorPagePhoneNumberState extends Equatable {
  ErrorPagePhoneNumberState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.errorPagePhoneNumberModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  ErrorPagePhoneNumberModel? errorPagePhoneNumberModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        errorPagePhoneNumberModelObj,
      ];

  ErrorPagePhoneNumberState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    ErrorPagePhoneNumberModel? errorPagePhoneNumberModelObj,
  }) {
    return ErrorPagePhoneNumberState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      errorPagePhoneNumberModelObj:
          errorPagePhoneNumberModelObj ?? this.errorPagePhoneNumberModelObj,
    );
  }
}
