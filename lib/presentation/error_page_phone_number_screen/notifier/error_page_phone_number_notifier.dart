import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/error_page_phone_number_screen/models/error_page_phone_number_model.dart';
part 'error_page_phone_number_state.dart';

final errorPagePhoneNumberNotifier = StateNotifierProvider<
    ErrorPagePhoneNumberNotifier, ErrorPagePhoneNumberState>(
  (ref) => ErrorPagePhoneNumberNotifier(ErrorPagePhoneNumberState(
    phoneNumberController: TextEditingController(),
    passwordController: TextEditingController(),
    isShowPassword: false,
    errorPagePhoneNumberModelObj: ErrorPagePhoneNumberModel(),
  )),
);

/// A notifier that manages the state of a ErrorPagePhoneNumber according to the event that is dispatched to it.
class ErrorPagePhoneNumberNotifier
    extends StateNotifier<ErrorPagePhoneNumberState> {
  ErrorPagePhoneNumberNotifier(ErrorPagePhoneNumberState state)
      : super(state) {}

  void changePasswordVisibility() {
    state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false));
  }
}
