import 'notifier/no_oreder_yet_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';class NoOrederYetScreen extends ConsumerStatefulWidget {const NoOrederYetScreen({Key? key}) : super(key: key);

@override NoOrederYetScreenState createState() =>  NoOrederYetScreenState();

 }
class NoOrederYetScreenState extends ConsumerState<NoOrederYetScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Container(width: double.maxFinite, padding: EdgeInsets.symmetric(horizontal: 30.h, vertical: 80.v), child: Column(children: [Container(height: 280.adaptSize, width: 280.adaptSize, padding: EdgeInsets.symmetric(horizontal: 57.h, vertical: 55.v), decoration: AppDecoration.fillGray30001.copyWith(borderRadius: BorderRadiusStyle.circleBorder140), child: CustomImageView(imagePath: ImageConstant.imgGroupGray900, height: 159.v, width: 160.h, alignment: Alignment.bottomLeft)), SizedBox(height: 49.v), Text("lbl_no_orders_yet".tr, style: CustomTextStyles.headlineSmall25), SizedBox(height: 22.v), SizedBox(width: 222.h, child: Text("msg_sorry_you_n_t_haven_t".tr, maxLines: 3, overflow: TextOverflow.ellipsis, textAlign: TextAlign.center, style: CustomTextStyles.titleMediumOnError_1.copyWith(height: 1.50))), Spacer(), SizedBox(height: 44.v), CustomElevatedButton(text: "lbl_go_back".tr)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_order".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
