import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/no_oreder_yet_screen/models/no_oreder_yet_model.dart';part 'no_oreder_yet_state.dart';final noOrederYetNotifier = StateNotifierProvider<NoOrederYetNotifier, NoOrederYetState>((ref) => NoOrederYetNotifier(NoOrederYetState(noOrederYetModelObj: NoOrederYetModel())));
/// A notifier that manages the state of a NoOrederYet according to the event that is dispatched to it.
class NoOrederYetNotifier extends StateNotifier<NoOrederYetState> {NoOrederYetNotifier(NoOrederYetState state) : super(state);

 }
