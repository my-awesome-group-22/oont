// ignore_for_file: must_be_immutable

part of 'no_oreder_yet_notifier.dart';

/// Represents the state of NoOrederYet in the application.
class NoOrederYetState extends Equatable {
  NoOrederYetState({this.noOrederYetModelObj});

  NoOrederYetModel? noOrederYetModelObj;

  @override
  List<Object?> get props => [
        noOrederYetModelObj,
      ];

  NoOrederYetState copyWith({NoOrederYetModel? noOrederYetModelObj}) {
    return NoOrederYetState(
      noOrederYetModelObj: noOrederYetModelObj ?? this.noOrederYetModelObj,
    );
  }
}
