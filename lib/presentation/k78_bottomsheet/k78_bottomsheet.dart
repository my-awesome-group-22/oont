import 'notifier/k78_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore_for_file: must_be_immutable
class K78Bottomsheet extends ConsumerStatefulWidget {
  const K78Bottomsheet({Key? key})
      : super(
          key: key,
        );

  @override
  K78BottomsheetState createState() => K78BottomsheetState();
}

class K78BottomsheetState extends ConsumerState<K78Bottomsheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375.h,
      padding: EdgeInsets.symmetric(
        horizontal: 171.h,
        vertical: 8.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.customBorderTL18,
      ),
      child: Container(
        height: 4.v,
        width: 32.h,
        decoration: BoxDecoration(
          color: appTheme.gray40003,
          borderRadius: BorderRadius.circular(
            2.h,
          ),
        ),
      ),
    );
  }
}
