import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/k78_bottomsheet/models/k78_model.dart';
part 'k78_state.dart';

final k78Notifier = StateNotifierProvider<K78Notifier, K78State>(
  (ref) => K78Notifier(K78State(
    k78ModelObj: K78Model(),
  )),
);

/// A notifier that manages the state of a K78 according to the event that is dispatched to it.
class K78Notifier extends StateNotifier<K78State> {
  K78Notifier(K78State state) : super(state) {}
}
