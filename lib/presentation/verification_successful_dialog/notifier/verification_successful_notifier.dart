import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/verification_successful_dialog/models/verification_successful_model.dart';
part 'verification_successful_state.dart';

final verificationSuccessfulNotifier = StateNotifierProvider<
    VerificationSuccessfulNotifier, VerificationSuccessfulState>(
  (ref) => VerificationSuccessfulNotifier(VerificationSuccessfulState(
    verificationSuccessfulModelObj: VerificationSuccessfulModel(),
  )),
);

/// A notifier that manages the state of a VerificationSuccessful according to the event that is dispatched to it.
class VerificationSuccessfulNotifier
    extends StateNotifier<VerificationSuccessfulState> {
  VerificationSuccessfulNotifier(VerificationSuccessfulState state)
      : super(state) {}
}
