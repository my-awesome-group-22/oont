// ignore_for_file: must_be_immutable

part of 'verification_successful_notifier.dart';

/// Represents the state of VerificationSuccessful in the application.
class VerificationSuccessfulState extends Equatable {
  VerificationSuccessfulState({this.verificationSuccessfulModelObj});

  VerificationSuccessfulModel? verificationSuccessfulModelObj;

  @override
  List<Object?> get props => [
        verificationSuccessfulModelObj,
      ];

  VerificationSuccessfulState copyWith(
      {VerificationSuccessfulModel? verificationSuccessfulModelObj}) {
    return VerificationSuccessfulState(
      verificationSuccessfulModelObj:
          verificationSuccessfulModelObj ?? this.verificationSuccessfulModelObj,
    );
  }
}
