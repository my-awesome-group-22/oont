import 'notifier/verification_successful_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

// ignore_for_file: must_be_immutable
class VerificationSuccessfulDialog extends ConsumerStatefulWidget {
  const VerificationSuccessfulDialog({Key? key})
      : super(
          key: key,
        );

  @override
  VerificationSuccessfulDialogState createState() =>
      VerificationSuccessfulDialogState();
}

class VerificationSuccessfulDialogState
    extends ConsumerState<VerificationSuccessfulDialog> {
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 0,
      margin: EdgeInsets.all(0),
      color: appTheme.whiteA700,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadiusStyle.roundedBorder30,
      ),
      child: Container(
        height: 520.v,
        width: 382.h,
        padding: EdgeInsets.symmetric(
          horizontal: 25.h,
          vertical: 53.v,
        ),
        decoration: AppDecoration.fillWhiteA.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder30,
        ),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            CustomImageView(
              imagePath: ImageConstant.imgGroup3832,
              height: 244.v,
              width: 217.h,
              alignment: Alignment.topCenter,
            ),
            _buildBrowseHomeSection(context),
          ],
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildBrowseHomeSection(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.only(bottom: 7.v),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "lbl_verified".tr,
              style: theme.textTheme.headlineSmall,
            ),
            SizedBox(height: 21.v),
            SizedBox(
              width: 246.h,
              child: Text(
                "msg_hurrah_you_have".tr,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: CustomTextStyles.titleMediumOnError_1.copyWith(
                  height: 1.40,
                ),
              ),
            ),
            SizedBox(height: 45.v),
            CustomElevatedButton(
              text: "lbl_browse_home".tr,
              buttonStyle: CustomButtonStyles.fillPrimaryContainer,
            ),
          ],
        ),
      ),
    );
  }
}
