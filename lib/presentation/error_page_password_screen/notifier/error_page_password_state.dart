// ignore_for_file: must_be_immutable

part of 'error_page_password_notifier.dart';

/// Represents the state of ErrorPagePassword in the application.
class ErrorPagePasswordState extends Equatable {
  ErrorPagePasswordState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.errorPagePasswordModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  ErrorPagePasswordModel? errorPagePasswordModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        errorPagePasswordModelObj,
      ];

  ErrorPagePasswordState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    ErrorPagePasswordModel? errorPagePasswordModelObj,
  }) {
    return ErrorPagePasswordState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      errorPagePasswordModelObj:
          errorPagePasswordModelObj ?? this.errorPagePasswordModelObj,
    );
  }
}
