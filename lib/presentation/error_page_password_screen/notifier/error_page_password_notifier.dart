import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/error_page_password_screen/models/error_page_password_model.dart';
part 'error_page_password_state.dart';

final errorPagePasswordNotifier =
    StateNotifierProvider<ErrorPagePasswordNotifier, ErrorPagePasswordState>(
  (ref) => ErrorPagePasswordNotifier(ErrorPagePasswordState(
    phoneNumberController: TextEditingController(),
    passwordController: TextEditingController(),
    isShowPassword: false,
    errorPagePasswordModelObj: ErrorPagePasswordModel(),
  )),
);

/// A notifier that manages the state of a ErrorPagePassword according to the event that is dispatched to it.
class ErrorPagePasswordNotifier extends StateNotifier<ErrorPagePasswordState> {
  ErrorPagePasswordNotifier(ErrorPagePasswordState state) : super(state) {}

  void changePasswordVisibility() {
    state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false));
  }
}
