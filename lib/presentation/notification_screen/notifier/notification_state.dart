// ignore_for_file: must_be_immutable

part of 'notification_notifier.dart';

/// Represents the state of Notification in the application.
class NotificationState extends Equatable {
  NotificationState({
    this.isSelectedSwitch = false,
    this.isSelectedSwitch1 = false,
    this.notificationModelObj,
  });

  NotificationModel? notificationModelObj;

  bool isSelectedSwitch;

  bool isSelectedSwitch1;

  @override
  List<Object?> get props => [
        isSelectedSwitch,
        isSelectedSwitch1,
        notificationModelObj,
      ];

  NotificationState copyWith({
    bool? isSelectedSwitch,
    bool? isSelectedSwitch1,
    NotificationModel? notificationModelObj,
  }) {
    return NotificationState(
      isSelectedSwitch: isSelectedSwitch ?? this.isSelectedSwitch,
      isSelectedSwitch1: isSelectedSwitch1 ?? this.isSelectedSwitch1,
      notificationModelObj: notificationModelObj ?? this.notificationModelObj,
    );
  }
}
