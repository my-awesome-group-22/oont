import 'notifier/notification_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_switch.dart';class NotificationScreen extends ConsumerStatefulWidget {const NotificationScreen({Key? key}) : super(key: key);

@override NotificationScreenState createState() =>  NotificationScreenState();

 }
class NotificationScreenState extends ConsumerState<NotificationScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, appBar: _buildAppBar(context), body: Container(width: 382.h, margin: EdgeInsets.fromLTRB(16.h, 30.v, 16.h, 5.v), padding: EdgeInsets.symmetric(horizontal: 17.h, vertical: 59.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [_buildNotificationList1(context), SizedBox(height: 27.v), _buildNotificationList2(context), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_notification".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildNotificationList1(BuildContext context) { return Padding(padding: EdgeInsets.only(right: 6.h), child: Column(children: [Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Padding(padding: EdgeInsets.only(bottom: 3.v), child: Text("msg_app_notification".tr, style: theme.textTheme.titleMedium)), Consumer(builder: (context, ref, _) {return CustomSwitch(value: ref.watch(notificationNotifier).isSelectedSwitch, onChange: (value) {ref.read(notificationNotifier.notifier).changeSwitchBox1(value);});})]), SizedBox(height: 15.v), Divider()])); } 
/// Section Widget
Widget _buildNotificationList2(BuildContext context) { return Padding(padding: EdgeInsets.only(right: 4.h), child: Column(children: [Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Padding(padding: EdgeInsets.only(bottom: 4.v), child: Text("msg_phone_number_notification".tr, style: theme.textTheme.titleMedium)), Consumer(builder: (context, ref, _) {return CustomSwitch(value: ref.watch(notificationNotifier).isSelectedSwitch1, onChange: (value) {ref.read(notificationNotifier.notifier).changeSwitchBox2(value);});})]), SizedBox(height: 15.v), Divider()])); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
