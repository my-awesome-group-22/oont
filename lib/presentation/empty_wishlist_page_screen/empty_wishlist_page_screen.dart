import 'notifier/empty_wishlist_page_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';class EmptyWishlistPageScreen extends ConsumerStatefulWidget {const EmptyWishlistPageScreen({Key? key}) : super(key: key);

@override EmptyWishlistPageScreenState createState() =>  EmptyWishlistPageScreenState();

 }
class EmptyWishlistPageScreenState extends ConsumerState<EmptyWishlistPageScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Container(width: double.maxFinite, padding: EdgeInsets.only(left: 30.h, top: 100.v, right: 30.h), child: Column(children: [Container(height: 280.adaptSize, width: 280.adaptSize, decoration: AppDecoration.fillGray30001.copyWith(borderRadius: BorderRadiusStyle.circleBorder140), child: CustomImageView(imagePath: ImageConstant.imgGroup3690, height: 134.v, width: 135.h, alignment: Alignment.center)), SizedBox(height: 40.v), Text("lbl_oppss".tr, style: theme.textTheme.headlineSmall), SizedBox(height: 15.v), Opacity(opacity: 0.7, child: Text("msg_sorry_you_have2".tr, style: CustomTextStyles.bodyLargeGray900_1)), SizedBox(height: 82.v), CustomElevatedButton(text: "lbl_start_adding".tr), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_wishlist_page".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
