// ignore_for_file: must_be_immutable

part of 'empty_wishlist_page_notifier.dart';

/// Represents the state of EmptyWishlistPage in the application.
class EmptyWishlistPageState extends Equatable {
  EmptyWishlistPageState({this.emptyWishlistPageModelObj});

  EmptyWishlistPageModel? emptyWishlistPageModelObj;

  @override
  List<Object?> get props => [
        emptyWishlistPageModelObj,
      ];

  EmptyWishlistPageState copyWith(
      {EmptyWishlistPageModel? emptyWishlistPageModelObj}) {
    return EmptyWishlistPageState(
      emptyWishlistPageModelObj:
          emptyWishlistPageModelObj ?? this.emptyWishlistPageModelObj,
    );
  }
}
