import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/empty_wishlist_page_screen/models/empty_wishlist_page_model.dart';part 'empty_wishlist_page_state.dart';final emptyWishlistPageNotifier = StateNotifierProvider<EmptyWishlistPageNotifier, EmptyWishlistPageState>((ref) => EmptyWishlistPageNotifier(EmptyWishlistPageState(emptyWishlistPageModelObj: EmptyWishlistPageModel())));
/// A notifier that manages the state of a EmptyWishlistPage according to the event that is dispatched to it.
class EmptyWishlistPageNotifier extends StateNotifier<EmptyWishlistPageState> {EmptyWishlistPageNotifier(EmptyWishlistPageState state) : super(state);

 }
