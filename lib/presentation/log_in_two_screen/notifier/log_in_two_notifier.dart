import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/log_in_two_screen/models/log_in_two_model.dart';part 'log_in_two_state.dart';final logInTwoNotifier = StateNotifierProvider<LogInTwoNotifier, LogInTwoState>((ref) => LogInTwoNotifier(LogInTwoState(phoneNumberController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: false, logInTwoModelObj: LogInTwoModel())));
/// A notifier that manages the state of a LogInTwo according to the event that is dispatched to it.
class LogInTwoNotifier extends StateNotifier<LogInTwoState> {LogInTwoNotifier(LogInTwoState state) : super(state);

void changePasswordVisibility() { state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false)); } 
 }
