// ignore_for_file: must_be_immutable

part of 'log_in_two_notifier.dart';

/// Represents the state of LogInTwo in the application.
class LogInTwoState extends Equatable {
  LogInTwoState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.logInTwoModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  LogInTwoModel? logInTwoModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        logInTwoModelObj,
      ];

  LogInTwoState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    LogInTwoModel? logInTwoModelObj,
  }) {
    return LogInTwoState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      logInTwoModelObj: logInTwoModelObj ?? this.logInTwoModelObj,
    );
  }
}
