import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/login_or_signup_three_screen/models/login_or_signup_three_model.dart';part 'login_or_signup_three_state.dart';final loginOrSignupThreeNotifier = StateNotifierProvider<LoginOrSignupThreeNotifier, LoginOrSignupThreeState>((ref) => LoginOrSignupThreeNotifier(LoginOrSignupThreeState(loginOrSignupThreeModelObj: LoginOrSignupThreeModel())));
/// A notifier that manages the state of a LoginOrSignupThree according to the event that is dispatched to it.
class LoginOrSignupThreeNotifier extends StateNotifier<LoginOrSignupThreeState> {LoginOrSignupThreeNotifier(LoginOrSignupThreeState state) : super(state);

 }
