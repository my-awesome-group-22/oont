// ignore_for_file: must_be_immutable

part of 'login_or_signup_three_notifier.dart';

/// Represents the state of LoginOrSignupThree in the application.
class LoginOrSignupThreeState extends Equatable {
  LoginOrSignupThreeState({this.loginOrSignupThreeModelObj});

  LoginOrSignupThreeModel? loginOrSignupThreeModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupThreeModelObj,
      ];

  LoginOrSignupThreeState copyWith(
      {LoginOrSignupThreeModel? loginOrSignupThreeModelObj}) {
    return LoginOrSignupThreeState(
      loginOrSignupThreeModelObj:
          loginOrSignupThreeModelObj ?? this.loginOrSignupThreeModelObj,
    );
  }
}
