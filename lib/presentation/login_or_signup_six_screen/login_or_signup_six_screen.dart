import 'notifier/login_or_signup_six_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_icon_button.dart';

class LoginOrSignupSixScreen extends ConsumerStatefulWidget {
  const LoginOrSignupSixScreen({Key? key})
      : super(
          key: key,
        );

  @override
  LoginOrSignupSixScreenState createState() => LoginOrSignupSixScreenState();
}

class LoginOrSignupSixScreenState
    extends ConsumerState<LoginOrSignupSixScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(
            horizontal: 20.h,
            vertical: 100.v,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(
                flex: 64,
              ),
              SizedBox(
                width: 217.h,
                child: Text(
                  "lbl_welcome_to_oont2".tr,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: CustomTextStyles.displaySmallBold,
                ),
              ),
              Spacer(
                flex: 35,
              ),
              CustomElevatedButton(
                text: "msg_login_with_email".tr,
                buttonStyle: CustomButtonStyles.fillAmber,
              ),
              SizedBox(height: 28.v),
              Text(
                "lbl_or".tr,
                style: CustomTextStyles.titleLargeBold,
              ),
              SizedBox(height: 40.v),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 22.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(right: 15.h),
                        child: CustomIconButton(
                          height: 60.adaptSize,
                          width: 60.adaptSize,
                          padding: EdgeInsets.all(15.h),
                          decoration: IconButtonStyleHelper.outlineBlueGrayTL30,
                          child: CustomImageView(
                            imagePath: ImageConstant.imgGroup3953,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.h),
                        child: CustomIconButton(
                          height: 60.adaptSize,
                          width: 60.adaptSize,
                          padding: EdgeInsets.all(15.h),
                          decoration: IconButtonStyleHelper.outlineRed,
                          child: CustomImageView(
                            imagePath: ImageConstant.imgGoogleRed600,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.h),
                        child: CustomIconButton(
                          height: 60.adaptSize,
                          width: 60.adaptSize,
                          padding: EdgeInsets.all(15.h),
                          decoration: IconButtonStyleHelper.outlineBlue,
                          child: CustomImageView(
                            imagePath: ImageConstant.imgTrash,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 15.h),
                        child: CustomIconButton(
                          height: 60.adaptSize,
                          width: 60.adaptSize,
                          padding: EdgeInsets.all(15.h),
                          decoration:
                              IconButtonStyleHelper.outlineErrorContainer,
                          child: CustomImageView(
                            imagePath: ImageConstant.imgFacebook,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
