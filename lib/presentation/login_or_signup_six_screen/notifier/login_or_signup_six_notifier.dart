import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/login_or_signup_six_screen/models/login_or_signup_six_model.dart';
part 'login_or_signup_six_state.dart';

final loginOrSignupSixNotifier =
    StateNotifierProvider<LoginOrSignupSixNotifier, LoginOrSignupSixState>(
  (ref) => LoginOrSignupSixNotifier(LoginOrSignupSixState(
    loginOrSignupSixModelObj: LoginOrSignupSixModel(),
  )),
);

/// A notifier that manages the state of a LoginOrSignupSix according to the event that is dispatched to it.
class LoginOrSignupSixNotifier extends StateNotifier<LoginOrSignupSixState> {
  LoginOrSignupSixNotifier(LoginOrSignupSixState state) : super(state) {}
}
