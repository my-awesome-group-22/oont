// ignore_for_file: must_be_immutable

part of 'login_or_signup_six_notifier.dart';

/// Represents the state of LoginOrSignupSix in the application.
class LoginOrSignupSixState extends Equatable {
  LoginOrSignupSixState({this.loginOrSignupSixModelObj});

  LoginOrSignupSixModel? loginOrSignupSixModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupSixModelObj,
      ];

  LoginOrSignupSixState copyWith(
      {LoginOrSignupSixModel? loginOrSignupSixModelObj}) {
    return LoginOrSignupSixState(
      loginOrSignupSixModelObj:
          loginOrSignupSixModelObj ?? this.loginOrSignupSixModelObj,
    );
  }
}
