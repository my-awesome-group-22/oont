import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/profile_screen/models/profile_model.dart';part 'profile_state.dart';final profileNotifier = StateNotifierProvider<ProfileNotifier, ProfileState>((ref) => ProfileNotifier(ProfileState(profileSettingNameController: TextEditingController(), profileSettingLastNameController: TextEditingController(), profileSettingPhoneNumberController: TextEditingController(), passwordController: TextEditingController(), profileModelObj: ProfileModel())));
/// A notifier that manages the state of a Profile according to the event that is dispatched to it.
class ProfileNotifier extends StateNotifier<ProfileState> {ProfileNotifier(ProfileState state) : super(state);

 }
