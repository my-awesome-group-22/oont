// ignore_for_file: must_be_immutable

part of 'profile_notifier.dart';

/// Represents the state of Profile in the application.
class ProfileState extends Equatable {
  ProfileState({
    this.profileSettingNameController,
    this.profileSettingLastNameController,
    this.profileSettingPhoneNumberController,
    this.passwordController,
    this.profileModelObj,
  });

  TextEditingController? profileSettingNameController;

  TextEditingController? profileSettingLastNameController;

  TextEditingController? profileSettingPhoneNumberController;

  TextEditingController? passwordController;

  ProfileModel? profileModelObj;

  @override
  List<Object?> get props => [
        profileSettingNameController,
        profileSettingLastNameController,
        profileSettingPhoneNumberController,
        passwordController,
        profileModelObj,
      ];

  ProfileState copyWith({
    TextEditingController? profileSettingNameController,
    TextEditingController? profileSettingLastNameController,
    TextEditingController? profileSettingPhoneNumberController,
    TextEditingController? passwordController,
    ProfileModel? profileModelObj,
  }) {
    return ProfileState(
      profileSettingNameController:
          profileSettingNameController ?? this.profileSettingNameController,
      profileSettingLastNameController: profileSettingLastNameController ??
          this.profileSettingLastNameController,
      profileSettingPhoneNumberController:
          profileSettingPhoneNumberController ??
              this.profileSettingPhoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      profileModelObj: profileModelObj ?? this.profileModelObj,
    );
  }
}
