import 'notifier/profile_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/core/utils/validation_functions.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_outlined_button.dart';import 'package:oont/widgets/custom_text_form_field.dart';class ProfileScreen extends ConsumerStatefulWidget {const ProfileScreen({Key? key}) : super(key: key);

@override ProfileScreenState createState() =>  ProfileScreenState();

 }

// ignore_for_file: must_be_immutable
class ProfileScreenState extends ConsumerState<ProfileScreen> {GlobalKey<FormState> _formKey = GlobalKey<FormState>();

@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: Form(key: _formKey, child: SingleChildScrollView(padding: EdgeInsets.only(top: 30.v), child: Container(margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 5.v), padding: EdgeInsets.symmetric(horizontal: 14.h, vertical: 29.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [_buildProfileSetting(context), SizedBox(height: 28.v), _buildProfileSetting1(context), SizedBox(height: 28.v), _buildProfileSettingPhoneNumber1(context), SizedBox(height: 28.v), _buildProfileSettingPassword(context), SizedBox(height: 29.v), _buildRequestAChange(context), SizedBox(height: 156.v), _buildSave(context), SizedBox(height: 16.v)])))))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_profile".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildProfileSettingName(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(profileNotifier).profileSettingNameController, hintText: "lbl_bilal".tr);}); } 
/// Section Widget
Widget _buildProfileSetting(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_first_name".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), _buildProfileSettingName(context)])); } 
/// Section Widget
Widget _buildProfileSettingLastName(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(profileNotifier).profileSettingLastNameController, hintText: "lbl_riaz".tr);}); } 
/// Section Widget
Widget _buildProfileSetting1(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_last_name".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), _buildProfileSettingLastName(context)])); } 
/// Section Widget
Widget _buildProfileSettingPhoneNumber(BuildContext context) { return Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(profileNotifier).profileSettingPhoneNumberController, hintText: "lbl_8801710000000".tr);}); } 
/// Section Widget
Widget _buildProfileSettingPhoneNumber1(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_phone_number".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), _buildProfileSettingPhoneNumber(context)])); } 
/// Section Widget
Widget _buildProfileSettingPassword(BuildContext context) { return Padding(padding: EdgeInsets.symmetric(horizontal: 6.h), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_password".tr, style: theme.textTheme.titleSmall), SizedBox(height: 12.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(profileNotifier).passwordController, hintText: "lbl2".tr, textInputAction: TextInputAction.done, textInputType: TextInputType.visiblePassword, validator: (value) {if (value == null || (!isValidPassword(value, isRequired: true))) {return "err_msg_please_enter_valid_password".tr;} return null;}, obscureText: true);})])); } 
/// Section Widget
Widget _buildRequestAChange(BuildContext context) { return CustomOutlinedButton(text: "msg_request_a_change".tr, buttonStyle: CustomButtonStyles.outlinePrimaryTL15, buttonTextStyle: CustomTextStyles.titleMediumBlack900Bold_1); } 
/// Section Widget
Widget _buildSave(BuildContext context) { return CustomElevatedButton(text: "lbl_save".tr); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
