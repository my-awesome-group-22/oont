import 'notifier/light_set_your_location_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/core/utils/validation_functions.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_text_form_field.dart';

// ignore_for_file: must_be_immutable
class LightSetYourLocationBottomsheet extends ConsumerStatefulWidget {
  const LightSetYourLocationBottomsheet({Key? key})
      : super(
          key: key,
        );

  @override
  LightSetYourLocationBottomsheetState createState() =>
      LightSetYourLocationBottomsheetState();
}

class LightSetYourLocationBottomsheetState
    extends ConsumerState<LightSetYourLocationBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(
        horizontal: 9.h,
        vertical: 7.v,
      ),
      decoration: AppDecoration.outlineGray.copyWith(
        borderRadius: BorderRadiusStyle.customBorderTL48,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CustomImageView(
            imagePath: ImageConstant.imgFrame,
            height: 3.v,
            width: 38.h,
          ),
          SizedBox(height: 23.v),
          Text(
            "lbl_location".tr,
            style: CustomTextStyles.headlineSmallGray90003,
          ),
          SizedBox(height: 28.v),
          Divider(
            color: appTheme.gray20002,
            indent: 14.h,
          ),
          SizedBox(height: 23.v),
          Padding(
            padding: EdgeInsets.only(left: 14.h),
            child: Consumer(
              builder: (context, ref, _) {
                return CustomTextFormField(
                  controller: ref
                      .watch(lightSetYourLocationNotifier)
                      .linkedinController,
                  hintText: "msg_london_united_kingdom".tr,
                  hintStyle: CustomTextStyles.titleSmallGray90003,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.visiblePassword,
                  suffix: InkWell(
                    onTap: () {
                      ref
                          .read(lightSetYourLocationNotifier.notifier)
                          .changePasswordVisibility();
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(30.h, 18.v, 20.h, 18.v),
                      child: CustomImageView(
                        imagePath: ImageConstant.imgLinkedinGray90003,
                        height: 20.adaptSize,
                        width: 20.adaptSize,
                      ),
                    ),
                  ),
                  suffixConstraints: BoxConstraints(
                    maxHeight: 56.v,
                  ),
                  validator: (value) {
                    if (value == null ||
                        (!isValidPassword(value, isRequired: true))) {
                      return "err_msg_please_enter_valid_password".tr;
                    }
                    return null;
                  },
                  obscureText:
                      ref.watch(lightSetYourLocationNotifier).isShowPassword,
                  contentPadding: EdgeInsets.only(
                    left: 20.h,
                    top: 20.v,
                    bottom: 20.v,
                  ),
                  borderDecoration: TextFormFieldStyleHelper.fillGrayTL16,
                  filled: true,
                  fillColor: appTheme.gray50,
                );
              },
            ),
          ),
          SizedBox(height: 24.v),
          Divider(
            color: appTheme.gray20002,
            indent: 14.h,
          ),
          SizedBox(height: 23.v),
          CustomElevatedButton(
            text: "lbl_done".tr,
            margin: EdgeInsets.only(
              left: 33.h,
              right: 19.h,
            ),
          ),
          SizedBox(height: 40.v),
        ],
      ),
    );
  }
}
