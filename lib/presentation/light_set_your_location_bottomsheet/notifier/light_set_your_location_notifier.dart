import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/light_set_your_location_bottomsheet/models/light_set_your_location_model.dart';
part 'light_set_your_location_state.dart';

final lightSetYourLocationNotifier = StateNotifierProvider<
    LightSetYourLocationNotifier, LightSetYourLocationState>(
  (ref) => LightSetYourLocationNotifier(LightSetYourLocationState(
    linkedinController: TextEditingController(),
    isShowPassword: false,
    lightSetYourLocationModelObj: LightSetYourLocationModel(),
  )),
);

/// A notifier that manages the state of a LightSetYourLocation according to the event that is dispatched to it.
class LightSetYourLocationNotifier
    extends StateNotifier<LightSetYourLocationState> {
  LightSetYourLocationNotifier(LightSetYourLocationState state)
      : super(state) {}

  void changePasswordVisibility() {
    state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false));
  }
}
