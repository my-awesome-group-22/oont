// ignore_for_file: must_be_immutable

part of 'light_set_your_location_notifier.dart';

/// Represents the state of LightSetYourLocation in the application.
class LightSetYourLocationState extends Equatable {
  LightSetYourLocationState({
    this.linkedinController,
    this.isShowPassword = true,
    this.lightSetYourLocationModelObj,
  });

  TextEditingController? linkedinController;

  LightSetYourLocationModel? lightSetYourLocationModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        linkedinController,
        isShowPassword,
        lightSetYourLocationModelObj,
      ];

  LightSetYourLocationState copyWith({
    TextEditingController? linkedinController,
    bool? isShowPassword,
    LightSetYourLocationModel? lightSetYourLocationModelObj,
  }) {
    return LightSetYourLocationState(
      linkedinController: linkedinController ?? this.linkedinController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      lightSetYourLocationModelObj:
          lightSetYourLocationModelObj ?? this.lightSetYourLocationModelObj,
    );
  }
}
