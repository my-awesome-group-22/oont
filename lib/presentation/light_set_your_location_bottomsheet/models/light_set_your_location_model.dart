// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [light_set_your_location_bottomsheet],
/// and is typically used to hold data that is passed between different parts of the application.
class LightSetYourLocationModel extends Equatable {LightSetYourLocationModel() {  }

LightSetYourLocationModel copyWith() { return LightSetYourLocationModel(
); } 
@override List<Object?> get props => [];
 }
