import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/profile_two_page/models/profile_two_model.dart';part 'profile_two_state.dart';final profileTwoNotifier = StateNotifierProvider<ProfileTwoNotifier, ProfileTwoState>((ref) => ProfileTwoNotifier(ProfileTwoState(profileTwoModelObj: ProfileTwoModel())));
/// A notifier that manages the state of a ProfileTwo according to the event that is dispatched to it.
class ProfileTwoNotifier extends StateNotifier<ProfileTwoState> {ProfileTwoNotifier(ProfileTwoState state) : super(state);

 }
