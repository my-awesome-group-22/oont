import '../models/categorygrid_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class CategorygridItemWidget extends StatelessWidget {
  CategorygridItemWidget(
    this.categorygridItemModelObj, {
    Key? key,
    this.onTapCategoreItemCard,
  }) : super(
          key: key,
        );

  CategorygridItemModel categorygridItemModelObj;

  VoidCallback? onTapCategoreItemCard;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTapCategoreItemCard!.call();
      },
      child: Column(
        children: [
          Container(
            height: 94.adaptSize,
            width: 94.adaptSize,
            padding: EdgeInsets.symmetric(
              horizontal: 31.h,
              vertical: 29.v,
            ),
            decoration: AppDecoration.fillPrimary.copyWith(
              borderRadius: BorderRadiusStyle.circleBorder47,
            ),
            child: CustomImageView(
              imagePath: categorygridItemModelObj?.userImage,
              height: 36.v,
              width: 31.h,
              alignment: Alignment.center,
            ),
          ),
          SizedBox(height: 11.v),
          Text(
            categorygridItemModelObj.fruit!,
            style: CustomTextStyles.titleSmallPrimaryBold,
          ),
        ],
      ),
    );
  }
}
