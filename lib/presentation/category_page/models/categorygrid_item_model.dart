import '../../../core/app_export.dart';/// This class is used in the [categorygrid_item_widget] screen.
class CategorygridItemModel {CategorygridItemModel({this.userImage, this.fruit, this.id, }) { userImage = userImage  ?? ImageConstant.imgUser;fruit = fruit  ?? "Vegetables";id = id  ?? ""; }

String? userImage;

String? fruit;

String? id;

 }
