import '../category_page/widgets/categorygrid_item_widget.dart';import 'models/categorygrid_item_model.dart';import 'notifier/category_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';class CategoryPage extends ConsumerStatefulWidget {const CategoryPage({Key? key}) : super(key: key);

@override CategoryPageState createState() =>  CategoryPageState();

 }
class CategoryPageState extends ConsumerState<CategoryPage> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(body: Container(width: double.maxFinite, decoration: AppDecoration.fillWhiteA, child: Container(padding: EdgeInsets.symmetric(horizontal: 31.h, vertical: 35.v), child: Column(children: [SizedBox(height: 14.v), Text("msg_choose_a_category".tr, style: CustomTextStyles.headlineLarge30), SizedBox(height: 39.v), _buildCategoryGrid(context)]))))); } 
/// Section Widget
Widget _buildCategoryGrid(BuildContext context) { return Consumer(builder: (context, ref, _) {return GridView.builder(shrinkWrap: true, gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(mainAxisExtent: 121.v, crossAxisCount: 3, mainAxisSpacing: 35.h, crossAxisSpacing: 35.h), physics: NeverScrollableScrollPhysics(), itemCount: ref.watch(categoryNotifier).categoryModelObj?.categorygridItemList.length ?? 0, itemBuilder: (context, index) {CategorygridItemModel model = ref.watch(categoryNotifier).categoryModelObj?.categorygridItemList[index] ?? CategorygridItemModel(); return CategorygridItemWidget(model, onTapCategoreItemCard: () {onTapCategoreItemCard(context);});});}); } 
/// Navigates to the vegetableCategoryScreen when the action is triggered.
onTapCategoreItemCard(BuildContext context) { NavigatorService.pushNamed(AppRoutes.vegetableCategoryScreen, ); } 
 }
