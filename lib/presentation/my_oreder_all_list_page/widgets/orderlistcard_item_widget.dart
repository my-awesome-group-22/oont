import '../models/orderlistcard_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class OrderlistcardItemWidget extends StatelessWidget {
  OrderlistcardItemWidget(
    this.orderlistcardItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  OrderlistcardItemModel orderlistcardItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20.h,
        vertical: 14.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.circleBorder8,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                orderlistcardItemModelObj.orderID!,
                style: theme.textTheme.titleSmall,
              ),
              Padding(
                padding: EdgeInsets.only(left: 3.h),
                child: Text(
                  orderlistcardItemModelObj.orderNumber!,
                  style: CustomTextStyles.titleSmallGray900_1,
                ),
              ),
              Spacer(),
              Text(
                orderlistcardItemModelObj.orderDate!,
                style: theme.textTheme.titleSmall,
              ),
            ],
          ),
          SizedBox(height: 16.v),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 34.v,
                width: 250.h,
                child: Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    CustomImageView(
                      imagePath: orderlistcardItemModelObj?.orderImage,
                      height: 14.adaptSize,
                      width: 14.adaptSize,
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(
                        top: 1.v,
                        right: 87.h,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 60.h),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 14.adaptSize,
                              width: 14.adaptSize,
                              decoration: BoxDecoration(
                                color: theme.colorScheme.primary,
                                borderRadius: BorderRadius.circular(
                                  7.h,
                                ),
                              ),
                            ),
                            SizedBox(height: 4.v),
                            Text(
                              orderlistcardItemModelObj.orderStatus!,
                              style: CustomTextStyles.titleSmallPrimary,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 250.h,
                        margin: EdgeInsets.only(bottom: 19.v),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              orderlistcardItemModelObj.status!,
                              style: theme.textTheme.bodyMedium,
                            ),
                            CustomImageView(
                              imagePath: orderlistcardItemModelObj?.status1,
                              height: 14.adaptSize,
                              width: 14.adaptSize,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomImageView(
                imagePath: orderlistcardItemModelObj?.orederID,
                height: 14.adaptSize,
                width: 14.adaptSize,
                margin: EdgeInsets.only(
                  left: 70.h,
                  bottom: 18.v,
                ),
              ),
            ],
          ),
          SizedBox(height: 4.v),
        ],
      ),
    );
  }
}
