// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'orderlistcard_item_model.dart';/// This class defines the variables used in the [my_oreder_all_list_page],
/// and is typically used to hold data that is passed between different parts of the application.
class MyOrederAllListModel extends Equatable {MyOrederAllListModel({this.orderlistcardItemList = const []}) {  }

List<OrderlistcardItemModel> orderlistcardItemList;

MyOrederAllListModel copyWith({List<OrderlistcardItemModel>? orderlistcardItemList}) { return MyOrederAllListModel(
orderlistcardItemList : orderlistcardItemList ?? this.orderlistcardItemList,
); } 
@override List<Object?> get props => [orderlistcardItemList];
 }
