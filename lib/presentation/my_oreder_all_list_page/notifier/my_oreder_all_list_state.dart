// ignore_for_file: must_be_immutable

part of 'my_oreder_all_list_notifier.dart';

/// Represents the state of MyOrederAllList in the application.
class MyOrederAllListState extends Equatable {
  MyOrederAllListState({this.myOrederAllListModelObj});

  MyOrederAllListModel? myOrederAllListModelObj;

  @override
  List<Object?> get props => [
        myOrederAllListModelObj,
      ];

  MyOrederAllListState copyWith(
      {MyOrederAllListModel? myOrederAllListModelObj}) {
    return MyOrederAllListState(
      myOrederAllListModelObj:
          myOrederAllListModelObj ?? this.myOrederAllListModelObj,
    );
  }
}
