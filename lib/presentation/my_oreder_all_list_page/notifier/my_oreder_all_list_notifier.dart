import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/orderlistcard_item_model.dart';
import 'package:oont/presentation/my_oreder_all_list_page/models/my_oreder_all_list_model.dart';
part 'my_oreder_all_list_state.dart';

final myOrederAllListNotifier =
    StateNotifierProvider<MyOrederAllListNotifier, MyOrederAllListState>(
  (ref) => MyOrederAllListNotifier(MyOrederAllListState(
    myOrederAllListModelObj: MyOrederAllListModel(orderlistcardItemList: [
      OrderlistcardItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May",
          orderImage: ImageConstant.imgMobile,
          orderStatus: "Confirmed",
          status: "Status:",
          status1: ImageConstant.imgMobile,
          orederID: ImageConstant.imgMobile),
      OrderlistcardItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      OrderlistcardItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      OrderlistcardItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      OrderlistcardItemModel(
          orderID: "Oreder ID:", orderNumber: "2324252627", orderDate: "25 May")
    ]),
  )),
);

/// A notifier that manages the state of a MyOrederAllList according to the event that is dispatched to it.
class MyOrederAllListNotifier extends StateNotifier<MyOrederAllListState> {
  MyOrederAllListNotifier(MyOrederAllListState state) : super(state) {}
}
