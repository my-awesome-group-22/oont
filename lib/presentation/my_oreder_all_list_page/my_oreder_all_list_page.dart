import '../my_oreder_all_list_page/widgets/orderlistcard_item_widget.dart';
import 'models/orderlistcard_item_model.dart';
import 'notifier/my_oreder_all_list_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore_for_file: must_be_immutable
class MyOrederAllListPage extends ConsumerStatefulWidget {
  const MyOrederAllListPage({Key? key})
      : super(
          key: key,
        );

  @override
  MyOrederAllListPageState createState() => MyOrederAllListPageState();
}

class MyOrederAllListPageState extends ConsumerState<MyOrederAllListPage>
    with AutomaticKeepAliveClientMixin<MyOrederAllListPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray10003,
        body: Container(
          width: double.maxFinite,
          decoration: AppDecoration.fillGray10003,
          child: Column(
            children: [
              SizedBox(height: 20.v),
              _buildOrderListCard(context),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildOrderListCard(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.h),
        child: Consumer(
          builder: (context, ref, _) {
            return ListView.separated(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (
                context,
                index,
              ) {
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0.v),
                  child: SizedBox(
                    width: 78.h,
                    child: Divider(
                      height: 4.v,
                      thickness: 4.v,
                      color: theme.colorScheme.primary,
                    ),
                  ),
                );
              },
              itemCount: ref
                      .watch(myOrederAllListNotifier)
                      .myOrederAllListModelObj
                      ?.orderlistcardItemList
                      .length ??
                  0,
              itemBuilder: (context, index) {
                OrderlistcardItemModel model = ref
                        .watch(myOrederAllListNotifier)
                        .myOrederAllListModelObj
                        ?.orderlistcardItemList[index] ??
                    OrderlistcardItemModel();
                return OrderlistcardItemWidget(
                  model,
                );
              },
            );
          },
        ),
      ),
    );
  }
}
