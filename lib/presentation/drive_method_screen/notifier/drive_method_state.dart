// ignore_for_file: must_be_immutable

part of 'drive_method_notifier.dart';

/// Represents the state of DriveMethod in the application.
class DriveMethodState extends Equatable {
  DriveMethodState({
    this.radioGroup = "",
    this.driveMethodModelObj,
  });

  DriveMethodModel? driveMethodModelObj;

  String radioGroup;

  @override
  List<Object?> get props => [
        radioGroup,
        driveMethodModelObj,
      ];

  DriveMethodState copyWith({
    String? radioGroup,
    DriveMethodModel? driveMethodModelObj,
  }) {
    return DriveMethodState(
      radioGroup: radioGroup ?? this.radioGroup,
      driveMethodModelObj: driveMethodModelObj ?? this.driveMethodModelObj,
    );
  }
}
