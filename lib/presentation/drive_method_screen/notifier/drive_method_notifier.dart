import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/drive_method_screen/models/drive_method_model.dart';part 'drive_method_state.dart';final driveMethodNotifier = StateNotifierProvider<DriveMethodNotifier, DriveMethodState>((ref) => DriveMethodNotifier(DriveMethodState(radioGroup: '', driveMethodModelObj: DriveMethodModel(radioList: ["lbl_car", "lbl_motorcycle", "lbl_bisycle", "lbl_other"]))));
/// A notifier that manages the state of a DriveMethod according to the event that is dispatched to it.
class DriveMethodNotifier extends StateNotifier<DriveMethodState> {DriveMethodNotifier(DriveMethodState state) : super(state);

void changeRadioButton1(String value) { state = state.copyWith(radioGroup: value); } 
 }
