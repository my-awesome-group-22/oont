import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/change_phone_number_screen/models/change_phone_number_model.dart';part 'change_phone_number_state.dart';final changePhoneNumberNotifier = StateNotifierProvider<ChangePhoneNumberNotifier, ChangePhoneNumberState>((ref) => ChangePhoneNumberNotifier(ChangePhoneNumberState(phoneNumberController: TextEditingController(), phoneNumberController1: TextEditingController(), changePhoneNumberModelObj: ChangePhoneNumberModel())));
/// A notifier that manages the state of a ChangePhoneNumber according to the event that is dispatched to it.
class ChangePhoneNumberNotifier extends StateNotifier<ChangePhoneNumberState> {ChangePhoneNumberNotifier(ChangePhoneNumberState state) : super(state);

 }
