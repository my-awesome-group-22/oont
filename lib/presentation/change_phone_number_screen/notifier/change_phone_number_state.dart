// ignore_for_file: must_be_immutable

part of 'change_phone_number_notifier.dart';

/// Represents the state of ChangePhoneNumber in the application.
class ChangePhoneNumberState extends Equatable {
  ChangePhoneNumberState({
    this.phoneNumberController,
    this.phoneNumberController1,
    this.changePhoneNumberModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? phoneNumberController1;

  ChangePhoneNumberModel? changePhoneNumberModelObj;

  @override
  List<Object?> get props => [
        phoneNumberController,
        phoneNumberController1,
        changePhoneNumberModelObj,
      ];

  ChangePhoneNumberState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? phoneNumberController1,
    ChangePhoneNumberModel? changePhoneNumberModelObj,
  }) {
    return ChangePhoneNumberState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      phoneNumberController1:
          phoneNumberController1 ?? this.phoneNumberController1,
      changePhoneNumberModelObj:
          changePhoneNumberModelObj ?? this.changePhoneNumberModelObj,
    );
  }
}
