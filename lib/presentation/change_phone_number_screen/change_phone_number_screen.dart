import 'notifier/change_phone_number_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_text_form_field.dart';class ChangePhoneNumberScreen extends ConsumerStatefulWidget {const ChangePhoneNumberScreen({Key? key}) : super(key: key);

@override ChangePhoneNumberScreenState createState() =>  ChangePhoneNumberScreenState();

 }
class ChangePhoneNumberScreenState extends ConsumerState<ChangePhoneNumberScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: Container(width: 382.h, margin: EdgeInsets.symmetric(horizontal: 16.h, vertical: 30.v), padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 58.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [_buildInputField1(context), SizedBox(height: 19.v), _buildInputField2(context), Spacer(flex: 36), CustomElevatedButton(text: "msg_update_phone_number".tr), Spacer(flex: 63)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "msg_change_phone_number".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildInputField1(BuildContext context) { return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Opacity(opacity: 0.8, child: Text("msg_new_phone_number".tr, style: CustomTextStyles.titleSmallOnError)), SizedBox(height: 10.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(changePhoneNumberNotifier).phoneNumberController, hintText: "lbl_8801710000".tr);})]); } 
/// Section Widget
Widget _buildInputField2(BuildContext context) { return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Opacity(opacity: 0.8, child: Text("msg_re_type_phone_number".tr, style: CustomTextStyles.titleSmallOnError)), SizedBox(height: 9.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(changePhoneNumberNotifier).phoneNumberController1, hintText: "lbl_88017100000".tr, textInputAction: TextInputAction.done);})]); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
