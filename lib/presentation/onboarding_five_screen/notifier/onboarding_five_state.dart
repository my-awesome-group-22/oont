// ignore_for_file: must_be_immutable

part of 'onboarding_five_notifier.dart';

/// Represents the state of OnboardingFive in the application.
class OnboardingFiveState extends Equatable {
  OnboardingFiveState({this.onboardingFiveModelObj});

  OnboardingFiveModel? onboardingFiveModelObj;

  @override
  List<Object?> get props => [
        onboardingFiveModelObj,
      ];

  OnboardingFiveState copyWith({OnboardingFiveModel? onboardingFiveModelObj}) {
    return OnboardingFiveState(
      onboardingFiveModelObj:
          onboardingFiveModelObj ?? this.onboardingFiveModelObj,
    );
  }
}
