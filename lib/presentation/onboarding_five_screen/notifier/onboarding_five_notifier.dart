import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_five_screen/models/onboarding_five_model.dart';
part 'onboarding_five_state.dart';

final onboardingFiveNotifier =
    StateNotifierProvider<OnboardingFiveNotifier, OnboardingFiveState>(
  (ref) => OnboardingFiveNotifier(OnboardingFiveState(
    onboardingFiveModelObj: OnboardingFiveModel(),
  )),
);

/// A notifier that manages the state of a OnboardingFive according to the event that is dispatched to it.
class OnboardingFiveNotifier extends StateNotifier<OnboardingFiveState> {
  OnboardingFiveNotifier(OnboardingFiveState state) : super(state) {}
}
