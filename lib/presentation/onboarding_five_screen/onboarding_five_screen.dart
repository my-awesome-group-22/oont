import 'notifier/onboarding_five_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingFiveScreen extends ConsumerStatefulWidget {
  const OnboardingFiveScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingFiveScreenState createState() => OnboardingFiveScreenState();
}

class OnboardingFiveScreenState extends ConsumerState<OnboardingFiveScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: theme.colorScheme.primaryContainer,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOontGray50,
                height: 85.v,
                width: 288.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
