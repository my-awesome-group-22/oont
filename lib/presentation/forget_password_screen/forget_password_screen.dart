import 'notifier/forget_password_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_text_form_field.dart';class ForgetPasswordScreen extends ConsumerStatefulWidget {const ForgetPasswordScreen({Key? key}) : super(key: key);

@override ForgetPasswordScreenState createState() =>  ForgetPasswordScreenState();

 }
class ForgetPasswordScreenState extends ConsumerState<ForgetPasswordScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: Container(width: 382.h, margin: EdgeInsets.fromLTRB(16.h, 30.v, 16.h, 5.v), padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 79.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [Text("msg_reset_your_password".tr, style: theme.textTheme.headlineSmall), SizedBox(height: 24.v), Container(width: 324.h, margin: EdgeInsets.only(right: 17.h), child: Text("msg_please_enter_your".tr, maxLines: 3, overflow: TextOverflow.ellipsis, style: CustomTextStyles.titleMediumOnError_1.copyWith(height: 1.40))), SizedBox(height: 25.v), _buildInputField(context), SizedBox(height: 30.v), CustomElevatedButton(text: "lbl_send_me_link".tr), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_forget_password".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildInputField(BuildContext context) { return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Opacity(opacity: 0.8, child: Text("lbl_phone_number".tr, style: CustomTextStyles.titleSmallOnError)), SizedBox(height: 10.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(forgetPasswordNotifier).phoneNumberController, hintText: "lbl_88017100000".tr, textInputAction: TextInputAction.done);})]); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
