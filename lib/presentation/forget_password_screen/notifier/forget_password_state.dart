// ignore_for_file: must_be_immutable

part of 'forget_password_notifier.dart';

/// Represents the state of ForgetPassword in the application.
class ForgetPasswordState extends Equatable {
  ForgetPasswordState({
    this.phoneNumberController,
    this.forgetPasswordModelObj,
  });

  TextEditingController? phoneNumberController;

  ForgetPasswordModel? forgetPasswordModelObj;

  @override
  List<Object?> get props => [
        phoneNumberController,
        forgetPasswordModelObj,
      ];

  ForgetPasswordState copyWith({
    TextEditingController? phoneNumberController,
    ForgetPasswordModel? forgetPasswordModelObj,
  }) {
    return ForgetPasswordState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      forgetPasswordModelObj:
          forgetPasswordModelObj ?? this.forgetPasswordModelObj,
    );
  }
}
