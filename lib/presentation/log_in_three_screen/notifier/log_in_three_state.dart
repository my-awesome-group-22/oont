// ignore_for_file: must_be_immutable

part of 'log_in_three_notifier.dart';

/// Represents the state of LogInThree in the application.
class LogInThreeState extends Equatable {
  LogInThreeState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.logInThreeModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  LogInThreeModel? logInThreeModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        logInThreeModelObj,
      ];

  LogInThreeState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    LogInThreeModel? logInThreeModelObj,
  }) {
    return LogInThreeState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      logInThreeModelObj: logInThreeModelObj ?? this.logInThreeModelObj,
    );
  }
}
