import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/log_in_three_screen/models/log_in_three_model.dart';part 'log_in_three_state.dart';final logInThreeNotifier = StateNotifierProvider<LogInThreeNotifier, LogInThreeState>((ref) => LogInThreeNotifier(LogInThreeState(phoneNumberController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: false, logInThreeModelObj: LogInThreeModel())));
/// A notifier that manages the state of a LogInThree according to the event that is dispatched to it.
class LogInThreeNotifier extends StateNotifier<LogInThreeState> {LogInThreeNotifier(LogInThreeState state) : super(state);

void changePasswordVisibility() { state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false)); } 
 }
