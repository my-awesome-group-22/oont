import '../models/productcard2_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Productcard2ItemWidget extends StatelessWidget {
  Productcard2ItemWidget(
    this.productcard2ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Productcard2ItemModel productcard2ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        CustomImageView(
          imagePath: productcard2ItemModelObj?.image,
          height: 49.v,
          width: 80.h,
          margin: EdgeInsets.only(
            top: 9.v,
            bottom: 4.v,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 133.h,
                child: Text(
                  productcard2ItemModelObj.title!,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: theme.textTheme.titleMedium!.copyWith(
                    height: 1.40,
                  ),
                ),
              ),
              SizedBox(height: 1.v),
              Text(
                productcard2ItemModelObj.quantity!,
                style: CustomTextStyles.titleMediumOnError_1,
              ),
            ],
          ),
        ),
        Spacer(),
        Padding(
          padding: EdgeInsets.only(top: 9.v),
          child: Column(
            children: [
              Text(
                productcard2ItemModelObj.price!,
                style: theme.textTheme.titleLarge,
              ),
              SizedBox(height: 18.v),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  productcard2ItemModelObj.multiplier!,
                  style: CustomTextStyles.labelLargeOnError,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
