// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'productcard2_item_model.dart';/// This class defines the variables used in the [my_order_details_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class MyOrderDetailsModel extends Equatable {MyOrderDetailsModel({this.productcard2ItemList = const []}) {  }

List<Productcard2ItemModel> productcard2ItemList;

MyOrderDetailsModel copyWith({List<Productcard2ItemModel>? productcard2ItemList}) { return MyOrderDetailsModel(
productcard2ItemList : productcard2ItemList ?? this.productcard2ItemList,
); } 
@override List<Object?> get props => [productcard2ItemList];
 }
