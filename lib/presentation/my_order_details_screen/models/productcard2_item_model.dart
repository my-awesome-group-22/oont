import '../../../core/app_export.dart';/// This class is used in the [productcard2_item_widget] screen.
class Productcard2ItemModel {Productcard2ItemModel({this.image, this.title, this.quantity, this.price, this.multiplier, this.id, }) { image = image  ?? ImageConstant.imgGroup3476;title = title  ?? "Fresh Papaya\nForm The Farmer";quantity = quantity  ?? "3KG";price = price  ?? "30";multiplier = multiplier  ?? "3x";id = id  ?? ""; }

String? image;

String? title;

String? quantity;

String? price;

String? multiplier;

String? id;

 }
