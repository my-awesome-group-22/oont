// ignore_for_file: must_be_immutable

part of 'my_order_details_notifier.dart';

/// Represents the state of MyOrderDetails in the application.
class MyOrderDetailsState extends Equatable {
  MyOrderDetailsState({this.myOrderDetailsModelObj});

  MyOrderDetailsModel? myOrderDetailsModelObj;

  @override
  List<Object?> get props => [
        myOrderDetailsModelObj,
      ];

  MyOrderDetailsState copyWith({MyOrderDetailsModel? myOrderDetailsModelObj}) {
    return MyOrderDetailsState(
      myOrderDetailsModelObj:
          myOrderDetailsModelObj ?? this.myOrderDetailsModelObj,
    );
  }
}
