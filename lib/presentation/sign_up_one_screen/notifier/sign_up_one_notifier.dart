import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/sign_up_one_screen/models/sign_up_one_model.dart';part 'sign_up_one_state.dart';final signUpOneNotifier = StateNotifierProvider<SignUpOneNotifier, SignUpOneState>((ref) => SignUpOneNotifier(SignUpOneState(nameController: TextEditingController(), phoneNumberController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: false, signUpOneModelObj: SignUpOneModel())));
/// A notifier that manages the state of a SignUpOne according to the event that is dispatched to it.
class SignUpOneNotifier extends StateNotifier<SignUpOneState> {SignUpOneNotifier(SignUpOneState state) : super(state);

void changePasswordVisibility() { state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false)); } 
 }
