// ignore_for_file: must_be_immutable

part of 'sign_up_one_notifier.dart';

/// Represents the state of SignUpOne in the application.
class SignUpOneState extends Equatable {
  SignUpOneState({
    this.nameController,
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.signUpOneModelObj,
  });

  TextEditingController? nameController;

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  SignUpOneModel? signUpOneModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        nameController,
        phoneNumberController,
        passwordController,
        isShowPassword,
        signUpOneModelObj,
      ];

  SignUpOneState copyWith({
    TextEditingController? nameController,
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    SignUpOneModel? signUpOneModelObj,
  }) {
    return SignUpOneState(
      nameController: nameController ?? this.nameController,
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      signUpOneModelObj: signUpOneModelObj ?? this.signUpOneModelObj,
    );
  }
}
