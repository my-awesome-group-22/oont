import '../search_screen/widgets/vegetablestext1_item_widget.dart';import 'models/vegetablestext1_item_model.dart';import 'notifier/search_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_title_edittext.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';class SearchScreen extends ConsumerStatefulWidget {const SearchScreen({Key? key}) : super(key: key);

@override SearchScreenState createState() =>  SearchScreenState();

 }
class SearchScreenState extends ConsumerState<SearchScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Container(width: double.maxFinite, padding: EdgeInsets.symmetric(horizontal: 12.h, vertical: 24.v), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Text("lbl_recent_searches".tr, style: CustomTextStyles.titleMedium18), SizedBox(height: 24.v), Padding(padding: EdgeInsets.symmetric(horizontal: 8.h), child: Consumer(builder: (context, ref, _) {return ListView.separated(physics: NeverScrollableScrollPhysics(), shrinkWrap: true, separatorBuilder: (context, index) {return Padding(padding: EdgeInsets.symmetric(vertical: 5.5.v), child: SizedBox(width: 374.h, child: Divider(height: 1.v, thickness: 1.v, color: appTheme.gray20001)));}, itemCount: ref.watch(searchNotifier).searchModelObj?.vegetablestext1ItemList.length ?? 0, itemBuilder: (context, index) {Vegetablestext1ItemModel model = ref.watch(searchNotifier).searchModelObj?.vegetablestext1ItemList[index] ?? Vegetablestext1ItemModel(); return Vegetablestext1ItemWidget(model);});}))])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 20.v, bottom: 20.v), onTap: () {onTapArrowLeft(context);}), title: Consumer(builder: (context, ref, _) {return AppbarTitleEdittext(margin: EdgeInsets.only(left: 20.h), hintText: "lbl_search".tr, controller: ref.watch(searchNotifier).searchController);})); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
