import '../models/vegetablestext1_item_model.dart';
import 'package:flutter/material.dart' hide SearchController;
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Vegetablestext1ItemWidget extends StatelessWidget {
  Vegetablestext1ItemWidget(
    this.vegetablestext1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Vegetablestext1ItemModel vegetablestext1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 13.v),
          child: Text(
            vegetablestext1ItemModelObj.vegetablesText!,
            style: theme.textTheme.bodyLarge,
          ),
        ),
        CustomImageView(
          imagePath: vegetablestext1ItemModelObj?.vegetablesImage,
          height: 15.adaptSize,
          width: 15.adaptSize,
          margin: EdgeInsets.only(
            top: 2.v,
            bottom: 13.v,
          ),
        ),
      ],
    );
  }
}
