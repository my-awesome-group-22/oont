// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'vegetablestext1_item_model.dart';/// This class defines the variables used in the [search_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class SearchModel extends Equatable {SearchModel({this.vegetablestext1ItemList = const []}) {  }

List<Vegetablestext1ItemModel> vegetablestext1ItemList;

SearchModel copyWith({List<Vegetablestext1ItemModel>? vegetablestext1ItemList}) { return SearchModel(
vegetablestext1ItemList : vegetablestext1ItemList ?? this.vegetablestext1ItemList,
); } 
@override List<Object?> get props => [vegetablestext1ItemList];
 }
