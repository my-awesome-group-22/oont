import '../../../core/app_export.dart';/// This class is used in the [vegetablestext1_item_widget] screen.
class Vegetablestext1ItemModel {Vegetablestext1ItemModel({this.vegetablesText, this.vegetablesImage, this.id, }) { vegetablesText = vegetablesText  ?? "Vegetables";vegetablesImage = vegetablesImage  ?? ImageConstant.imgTopLeft;id = id  ?? ""; }

String? vegetablesText;

String? vegetablesImage;

String? id;

 }
