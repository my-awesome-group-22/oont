import '../../../core/app_export.dart';/// This class is used in the [productlist_item_widget] screen.
class ProductlistItemModel {ProductlistItemModel({this.productImage, this.productName, this.productSize, this.two, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.imgSulphurFreeDo;productName = productName  ?? "Sulphurfree Bura";productSize = productSize  ?? "570 Ml";two = two  ?? "1";productPrice = productPrice  ?? "20";id = id  ?? ""; }

String? productImage;

String? productName;

String? productSize;

String? two;

String? productPrice;

String? id;

 }
