// ignore_for_file: must_be_immutable

part of 'cart_page_notifier.dart';

/// Represents the state of CartPage in the application.
class CartPageState extends Equatable {
  CartPageState({
    this.entryVoucherCodeController,
    this.cartPageModelObj,
  });

  TextEditingController? entryVoucherCodeController;

  CartPageModel? cartPageModelObj;

  @override
  List<Object?> get props => [
        entryVoucherCodeController,
        cartPageModelObj,
      ];

  CartPageState copyWith({
    TextEditingController? entryVoucherCodeController,
    CartPageModel? cartPageModelObj,
  }) {
    return CartPageState(
      entryVoucherCodeController:
          entryVoucherCodeController ?? this.entryVoucherCodeController,
      cartPageModelObj: cartPageModelObj ?? this.cartPageModelObj,
    );
  }
}
