import '../models/productlist_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class ProductlistItemWidget extends StatelessWidget {
  ProductlistItemWidget(
    this.productlistItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ProductlistItemModel productlistItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CustomImageView(
          imagePath: productlistItemModelObj?.productImage,
          height: 72.v,
          width: 54.h,
          margin: EdgeInsets.only(
            top: 6.v,
            bottom: 5.v,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 32.h),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          productlistItemModelObj.productName!,
                          style: theme.textTheme.titleMedium,
                        ),
                        SizedBox(height: 4.v),
                        Text(
                          productlistItemModelObj.productSize!,
                          style: theme.textTheme.titleSmall,
                        ),
                      ],
                    ),
                    CustomImageView(
                      imagePath: ImageConstant.imgThumbsUpGray90024x24,
                      height: 24.adaptSize,
                      width: 24.adaptSize,
                      margin: EdgeInsets.only(
                        top: 2.v,
                        bottom: 10.v,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12.v),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 109.h,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomIconButton(
                            height: 34.adaptSize,
                            width: 34.adaptSize,
                            padding: EdgeInsets.all(7.h),
                            decoration: IconButtonStyleHelper.outlineGrayTL10,
                            child: CustomImageView(
                              imagePath: ImageConstant.imgMinus,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 5.v,
                              bottom: 7.v,
                            ),
                            child: Text(
                              productlistItemModelObj.two!,
                              style: theme.textTheme.titleLarge,
                            ),
                          ),
                          CustomIconButton(
                            height: 34.adaptSize,
                            width: 34.adaptSize,
                            padding: EdgeInsets.all(7.h),
                            decoration: IconButtonStyleHelper.outlineBlueGray,
                            child: CustomImageView(
                              imagePath: ImageConstant.imgPlusPrimary,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 3.v,
                        bottom: 9.v,
                      ),
                      child: Text(
                        productlistItemModelObj.productPrice!,
                        style: theme.textTheme.titleLarge,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
