import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_four_screen/models/onboarding_four_model.dart';
part 'onboarding_four_state.dart';

final onboardingFourNotifier =
    StateNotifierProvider<OnboardingFourNotifier, OnboardingFourState>(
  (ref) => OnboardingFourNotifier(OnboardingFourState(
    onboardingFourModelObj: OnboardingFourModel(),
  )),
);

/// A notifier that manages the state of a OnboardingFour according to the event that is dispatched to it.
class OnboardingFourNotifier extends StateNotifier<OnboardingFourState> {
  OnboardingFourNotifier(OnboardingFourState state) : super(state) {}
}
