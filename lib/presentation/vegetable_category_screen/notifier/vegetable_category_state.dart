// ignore_for_file: must_be_immutable

part of 'vegetable_category_notifier.dart';

/// Represents the state of VegetableCategory in the application.
class VegetableCategoryState extends Equatable {
  VegetableCategoryState({this.vegetableCategoryModelObj});

  VegetableCategoryModel? vegetableCategoryModelObj;

  @override
  List<Object?> get props => [
        vegetableCategoryModelObj,
      ];

  VegetableCategoryState copyWith(
      {VegetableCategoryModel? vegetableCategoryModelObj}) {
    return VegetableCategoryState(
      vegetableCategoryModelObj:
          vegetableCategoryModelObj ?? this.vegetableCategoryModelObj,
    );
  }
}
