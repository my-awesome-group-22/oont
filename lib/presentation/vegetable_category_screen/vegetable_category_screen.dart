import '../vegetable_category_screen/widgets/productcard1_item_widget.dart';import 'models/productcard1_item_model.dart';import 'notifier/vegetable_category_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';class VegetableCategoryScreen extends ConsumerStatefulWidget {const VegetableCategoryScreen({Key? key}) : super(key: key);

@override VegetableCategoryScreenState createState() =>  VegetableCategoryScreenState();

 }
class VegetableCategoryScreenState extends ConsumerState<VegetableCategoryScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Padding(padding: EdgeInsets.only(top: 25.v), child: Consumer(builder: (context, ref, _) {return StaggeredGridView.countBuilder(shrinkWrap: true, primary: false, crossAxisCount: 4, crossAxisSpacing: 14.h, mainAxisSpacing: 14.h, staggeredTileBuilder: (index) {return StaggeredTile.fit(2);}, itemCount: ref.watch(vegetableCategoryNotifier).vegetableCategoryModelObj?.productcard1ItemList.length ?? 0, itemBuilder: (context, index) {Productcard1ItemModel model = ref.watch(vegetableCategoryNotifier).vegetableCategoryModelObj?.productcard1ItemList[index] ?? Productcard1ItemModel(); return Productcard1ItemWidget(model);});})))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_vagetables".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
