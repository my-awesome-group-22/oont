// ignore_for_file: must_be_immutable

part of 'create_my_pack_notifier.dart';

/// Represents the state of CreateMyPack in the application.
class CreateMyPackState extends Equatable {
  CreateMyPackState({this.createMyPackModelObj});

  CreateMyPackModel? createMyPackModelObj;

  @override
  List<Object?> get props => [
        createMyPackModelObj,
      ];

  CreateMyPackState copyWith({CreateMyPackModel? createMyPackModelObj}) {
    return CreateMyPackState(
      createMyPackModelObj: createMyPackModelObj ?? this.createMyPackModelObj,
    );
  }
}
