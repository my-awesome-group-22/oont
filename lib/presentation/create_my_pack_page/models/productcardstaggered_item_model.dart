import '../../../core/app_export.dart';/// This class is used in the [productcardstaggered_item_widget] screen.
class ProductcardstaggeredItemModel {ProductcardstaggeredItemModel({this.productImage, this.productTitle, this.productWeight, this.productPrice, this.id, }) { productImage = productImage  ?? ImageConstant.imgGroup3473;productTitle = productTitle  ?? "Cauliflower\nForm The Garden";productWeight = productWeight  ?? "1 kg";productPrice = productPrice  ?? "13";id = id  ?? ""; }

String? productImage;

String? productTitle;

String? productWeight;

String? productPrice;

String? id;

 }
