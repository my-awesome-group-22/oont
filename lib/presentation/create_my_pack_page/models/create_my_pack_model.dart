// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'productcardstaggered_item_model.dart';/// This class defines the variables used in the [create_my_pack_page],
/// and is typically used to hold data that is passed between different parts of the application.
class CreateMyPackModel extends Equatable {CreateMyPackModel({this.productcardstaggeredItemList = const []}) {  }

List<ProductcardstaggeredItemModel> productcardstaggeredItemList;

CreateMyPackModel copyWith({List<ProductcardstaggeredItemModel>? productcardstaggeredItemList}) { return CreateMyPackModel(
productcardstaggeredItemList : productcardstaggeredItemList ?? this.productcardstaggeredItemList,
); } 
@override List<Object?> get props => [productcardstaggeredItemList];
 }
