// ignore_for_file: must_be_immutable

part of 'new_password_notifier.dart';

/// Represents the state of NewPassword in the application.
class NewPasswordState extends Equatable {
  NewPasswordState({
    this.newpasswordController,
    this.passwordController,
    this.newPasswordModelObj,
  });

  TextEditingController? newpasswordController;

  TextEditingController? passwordController;

  NewPasswordModel? newPasswordModelObj;

  @override
  List<Object?> get props => [
        newpasswordController,
        passwordController,
        newPasswordModelObj,
      ];

  NewPasswordState copyWith({
    TextEditingController? newpasswordController,
    TextEditingController? passwordController,
    NewPasswordModel? newPasswordModelObj,
  }) {
    return NewPasswordState(
      newpasswordController:
          newpasswordController ?? this.newpasswordController,
      passwordController: passwordController ?? this.passwordController,
      newPasswordModelObj: newPasswordModelObj ?? this.newPasswordModelObj,
    );
  }
}
