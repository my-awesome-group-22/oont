import 'notifier/login_or_signup_two_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class LoginOrSignupTwoScreen extends ConsumerStatefulWidget {
  const LoginOrSignupTwoScreen({Key? key})
      : super(
          key: key,
        );

  @override
  LoginOrSignupTwoScreenState createState() => LoginOrSignupTwoScreenState();
}

class LoginOrSignupTwoScreenState
    extends ConsumerState<LoginOrSignupTwoScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray50,
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(vertical: 61.v),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildWelcomeToOont(context),
              Spacer(),
              CustomElevatedButton(
                text: "lbl_oont_grocery".tr,
                margin: EdgeInsets.symmetric(horizontal: 20.h),
              ),
              SizedBox(height: 20.v),
              CustomElevatedButton(
                text: "lbl_oont_transport".tr,
                margin: EdgeInsets.symmetric(horizontal: 20.h),
                buttonStyle: CustomButtonStyles.fillGray,
                buttonTextStyle: CustomTextStyles.titleMediumGray50Bold,
              ),
              SizedBox(height: 24.v),
              CustomElevatedButton(
                text: "msg_oont_food_delivery".tr,
                margin: EdgeInsets.symmetric(horizontal: 20.h),
                buttonStyle: CustomButtonStyles.fillAmber,
                buttonTextStyle: CustomTextStyles.titleMediumBold_1,
              ),
              SizedBox(height: 2.v),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildWelcomeToOont(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: IntrinsicWidth(
        child: SizedBox(
          height: 348.v,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 27.h),
                  child: Text(
                    "lbl_welcome_to_oont".tr,
                    style: CustomTextStyles.displaySmallPrimaryContainer,
                  ),
                ),
              ),
              CustomImageView(
                imagePath: ImageConstant.imgRectangle428,
                height: 180.v,
                width: 374.h,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 27.v),
              ),
              CustomImageView(
                imagePath: ImageConstant.imgRectangle2253,
                height: 180.v,
                width: 374.h,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 27.v),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: 41.v,
                  width: 60.h,
                  margin: EdgeInsets.only(
                    left: 21.h,
                    bottom: 24.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img43x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: 49.v,
                  width: 53.h,
                  margin: EdgeInsets.only(
                    left: 156.h,
                    bottom: 27.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img83x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 243.adaptSize,
                  width: 243.adaptSize,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img5973x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  height: 64.v,
                  width: 85.h,
                  margin: EdgeInsets.only(
                    right: 119.h,
                    bottom: 20.v,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.img537376GrainP,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 41.v,
                  width: 47.h,
                  margin: EdgeInsets.only(top: 141.v),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.imgAvocado3x,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  height: 153.v,
                  width: 158.h,
                  margin: EdgeInsets.only(top: 77.v),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        ImageConstant.imgFavpngFarmers,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 43.h,
                    bottom: 75.v,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 151.h,
                        child: Text(
                          "msg_order_your_daily".tr,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style:
                              CustomTextStyles.titleLargeRubikBlack900.copyWith(
                            height: 1.25,
                          ),
                        ),
                      ),
                      SizedBox(height: 7.v),
                      Text(
                        "lbl_easydelivery".tr,
                        style: CustomTextStyles.titleSmallRubikGreen500,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
