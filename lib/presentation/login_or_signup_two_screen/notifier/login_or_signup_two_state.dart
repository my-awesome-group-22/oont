// ignore_for_file: must_be_immutable

part of 'login_or_signup_two_notifier.dart';

/// Represents the state of LoginOrSignupTwo in the application.
class LoginOrSignupTwoState extends Equatable {
  LoginOrSignupTwoState({this.loginOrSignupTwoModelObj});

  LoginOrSignupTwoModel? loginOrSignupTwoModelObj;

  @override
  List<Object?> get props => [
        loginOrSignupTwoModelObj,
      ];

  LoginOrSignupTwoState copyWith(
      {LoginOrSignupTwoModel? loginOrSignupTwoModelObj}) {
    return LoginOrSignupTwoState(
      loginOrSignupTwoModelObj:
          loginOrSignupTwoModelObj ?? this.loginOrSignupTwoModelObj,
    );
  }
}
