import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/login_or_signup_two_screen/models/login_or_signup_two_model.dart';
part 'login_or_signup_two_state.dart';

final loginOrSignupTwoNotifier =
    StateNotifierProvider<LoginOrSignupTwoNotifier, LoginOrSignupTwoState>(
  (ref) => LoginOrSignupTwoNotifier(LoginOrSignupTwoState(
    loginOrSignupTwoModelObj: LoginOrSignupTwoModel(),
  )),
);

/// A notifier that manages the state of a LoginOrSignupTwo according to the event that is dispatched to it.
class LoginOrSignupTwoNotifier extends StateNotifier<LoginOrSignupTwoState> {
  LoginOrSignupTwoNotifier(LoginOrSignupTwoState state) : super(state) {}
}
