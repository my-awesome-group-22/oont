// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'package:oont/data/models/selectionPopupModel/selection_popup_model.dart';/// This class defines the variables used in the [bank_details_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class BankDetailsModel extends Equatable {BankDetailsModel({this.dropdownItemList = const []}) {  }

List<SelectionPopupModel> dropdownItemList;

BankDetailsModel copyWith({List<SelectionPopupModel>? dropdownItemList}) { return BankDetailsModel(
dropdownItemList : dropdownItemList ?? this.dropdownItemList,
); } 
@override List<Object?> get props => [dropdownItemList];
 }
