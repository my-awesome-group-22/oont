// ignore_for_file: must_be_immutable

part of 'bank_details_notifier.dart';

/// Represents the state of BankDetails in the application.
class BankDetailsState extends Equatable {
  BankDetailsState({
    this.bankAccountNameController,
    this.nameController,
    this.selectedDropDownValue,
    this.bankDetailsModelObj,
  });

  TextEditingController? bankAccountNameController;

  TextEditingController? nameController;

  SelectionPopupModel? selectedDropDownValue;

  BankDetailsModel? bankDetailsModelObj;

  @override
  List<Object?> get props => [
        bankAccountNameController,
        nameController,
        selectedDropDownValue,
        bankDetailsModelObj,
      ];

  BankDetailsState copyWith({
    TextEditingController? bankAccountNameController,
    TextEditingController? nameController,
    SelectionPopupModel? selectedDropDownValue,
    BankDetailsModel? bankDetailsModelObj,
  }) {
    return BankDetailsState(
      bankAccountNameController:
          bankAccountNameController ?? this.bankAccountNameController,
      nameController: nameController ?? this.nameController,
      selectedDropDownValue:
          selectedDropDownValue ?? this.selectedDropDownValue,
      bankDetailsModelObj: bankDetailsModelObj ?? this.bankDetailsModelObj,
    );
  }
}
