import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/bank_details_screen/models/bank_details_model.dart';part 'bank_details_state.dart';final bankDetailsNotifier = StateNotifierProvider<BankDetailsNotifier, BankDetailsState>((ref) => BankDetailsNotifier(BankDetailsState(bankAccountNameController: TextEditingController(), nameController: TextEditingController(), selectedDropDownValue: SelectionPopupModel(title: ''), bankDetailsModelObj: BankDetailsModel(dropdownItemList: [SelectionPopupModel(id: 1, title: "Item One", isSelected: true), SelectionPopupModel(id: 2, title: "Item Two"), SelectionPopupModel(id: 3, title: "Item Three")]))));
/// A notifier that manages the state of a BankDetails according to the event that is dispatched to it.
class BankDetailsNotifier extends StateNotifier<BankDetailsState> {BankDetailsNotifier(BankDetailsState state) : super(state);

 }
