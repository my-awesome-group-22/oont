import 'notifier/light_set_your_location_permission_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_outlined_button.dart';

// ignore_for_file: must_be_immutable
class LightSetYourLocationPermissionDialog extends ConsumerStatefulWidget {
  const LightSetYourLocationPermissionDialog({Key? key})
      : super(
          key: key,
        );

  @override
  LightSetYourLocationPermissionDialogState createState() =>
      LightSetYourLocationPermissionDialogState();
}

class LightSetYourLocationPermissionDialogState
    extends ConsumerState<LightSetYourLocationPermissionDialog> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 316.h,
      padding: EdgeInsets.symmetric(
        horizontal: 27.h,
        vertical: 38.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.roundedBorder24,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(height: 26.v),
          CustomImageView(
            imagePath: ImageConstant.imgLinkedin,
            height: 87.v,
            width: 71.h,
          ),
          SizedBox(height: 26.v),
          Text(
            "lbl_location".tr,
            style: CustomTextStyles.titleLargeGray90002,
          ),
          SizedBox(height: 20.v),
          SizedBox(
            width: 261.h,
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "lbl_allow_maps".tr,
                    style: CustomTextStyles.titleMediumGray700,
                  ),
                  TextSpan(
                    text: " ",
                  ),
                  TextSpan(
                    text: "msg_to_access_your_location".tr,
                    style: CustomTextStyles.titleMediumGray700.copyWith(
                      height: 1.30,
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 42.v),
          CustomElevatedButton(
            height: 44.v,
            text: "lbl_allow".tr,
            margin: EdgeInsets.symmetric(horizontal: 5.h),
            buttonStyle: CustomButtonStyles.outlineOnPrimaryContainer,
            buttonTextStyle: CustomTextStyles.titleMediumGray50,
          ),
          SizedBox(height: 15.v),
          CustomOutlinedButton(
            height: 44.v,
            text: "lbl_skip_for_now".tr,
            margin: EdgeInsets.symmetric(horizontal: 5.h),
            buttonStyle: CustomButtonStyles.outlineGray,
            buttonTextStyle: CustomTextStyles.titleMediumGray90003,
          ),
        ],
      ),
    );
  }
}
