// ignore_for_file: must_be_immutable

part of 'light_set_your_location_permission_notifier.dart';

/// Represents the state of LightSetYourLocationPermission in the application.
class LightSetYourLocationPermissionState extends Equatable {
  LightSetYourLocationPermissionState(
      {this.lightSetYourLocationPermissionModelObj});

  LightSetYourLocationPermissionModel? lightSetYourLocationPermissionModelObj;

  @override
  List<Object?> get props => [
        lightSetYourLocationPermissionModelObj,
      ];

  LightSetYourLocationPermissionState copyWith(
      {LightSetYourLocationPermissionModel?
          lightSetYourLocationPermissionModelObj}) {
    return LightSetYourLocationPermissionState(
      lightSetYourLocationPermissionModelObj:
          lightSetYourLocationPermissionModelObj ??
              this.lightSetYourLocationPermissionModelObj,
    );
  }
}
