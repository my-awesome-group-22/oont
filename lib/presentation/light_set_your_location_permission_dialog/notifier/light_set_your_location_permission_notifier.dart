import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/light_set_your_location_permission_dialog/models/light_set_your_location_permission_model.dart';
part 'light_set_your_location_permission_state.dart';

final lightSetYourLocationPermissionNotifier = StateNotifierProvider<
    LightSetYourLocationPermissionNotifier,
    LightSetYourLocationPermissionState>(
  (ref) => LightSetYourLocationPermissionNotifier(
      LightSetYourLocationPermissionState(
    lightSetYourLocationPermissionModelObj:
        LightSetYourLocationPermissionModel(),
  )),
);

/// A notifier that manages the state of a LightSetYourLocationPermission according to the event that is dispatched to it.
class LightSetYourLocationPermissionNotifier
    extends StateNotifier<LightSetYourLocationPermissionState> {
  LightSetYourLocationPermissionNotifier(
      LightSetYourLocationPermissionState state)
      : super(state) {}
}
