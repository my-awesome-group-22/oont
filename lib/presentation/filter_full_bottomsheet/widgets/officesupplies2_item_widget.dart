import '../models/officesupplies2_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Officesupplies2ItemWidget extends StatelessWidget {
  Officesupplies2ItemWidget(
    this.officesupplies2ItemModelObj, {
    Key? key,
    this.onSelectedChipView1,
  }) : super(
          key: key,
        );

  Officesupplies2ItemModel officesupplies2ItemModelObj;

  Function(bool)? onSelectedChipView1;

  @override
  Widget build(BuildContext context) {
    return RawChip(
      padding: EdgeInsets.symmetric(
        horizontal: 5.h,
        vertical: 15.v,
      ),
      showCheckmark: false,
      labelPadding: EdgeInsets.zero,
      label: Text(
        officesupplies2ItemModelObj.officeSupplies!,
        style: TextStyle(
          color: (officesupplies2ItemModelObj.isSelected ?? false)
              ? theme.colorScheme.onError.withOpacity(1)
              : appTheme.whiteA700,
          fontSize: 14.fSize,
          fontFamily: 'Epilogue',
          fontWeight: FontWeight.w500,
        ),
      ),
      selected: (officesupplies2ItemModelObj.isSelected ?? false),
      backgroundColor: theme.colorScheme.primary,
      selectedColor: Colors.transparent,
      shape: (officesupplies2ItemModelObj.isSelected ?? false)
          ? RoundedRectangleBorder(
              side: BorderSide(
                color: theme.colorScheme.primary,
                width: 1.h,
              ),
              borderRadius: BorderRadius.circular(
                8.h,
              ),
            )
          : RoundedRectangleBorder(
              side: BorderSide(
                color: theme.colorScheme.primary,
                width: 1.h,
              ),
              borderRadius: BorderRadius.circular(
                8.h,
              ),
            ),
      onSelected: (value) {
        onSelectedChipView1?.call(value);
      },
    );
  }
}
