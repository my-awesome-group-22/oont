// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class is used in the [officesupplies2_item_widget] screen.
class Officesupplies2ItemModel extends Equatable {Officesupplies2ItemModel({this.officeSupplies, this.isSelected, }) { officeSupplies = officeSupplies  ?? "Office Supplies";isSelected = isSelected  ?? false; }

String? officeSupplies;

bool? isSelected;

Officesupplies2ItemModel copyWith({String? officeSupplies, bool? isSelected, }) { return Officesupplies2ItemModel(
officeSupplies : officeSupplies ?? this.officeSupplies,
isSelected : isSelected ?? this.isSelected,
); } 
@override List<Object?> get props => [officeSupplies,isSelected];
 }
