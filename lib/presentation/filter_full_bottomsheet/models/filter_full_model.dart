// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'officesupplies2_item_model.dart';import 'any_item_model.dart';/// This class defines the variables used in the [filter_full_bottomsheet],
/// and is typically used to hold data that is passed between different parts of the application.
class FilterFullModel extends Equatable {FilterFullModel({this.officesupplies2ItemList = const [], this.anyItemList = const [], }) {  }

List<Officesupplies2ItemModel> officesupplies2ItemList;

List<AnyItemModel> anyItemList;

FilterFullModel copyWith({List<Officesupplies2ItemModel>? officesupplies2ItemList, List<AnyItemModel>? anyItemList, }) { return FilterFullModel(
officesupplies2ItemList : officesupplies2ItemList ?? this.officesupplies2ItemList,
anyItemList : anyItemList ?? this.anyItemList,
); } 
@override List<Object?> get props => [officesupplies2ItemList,anyItemList];
 }
