import '../filter_full_bottomsheet/widgets/any_item_widget.dart';
import '../filter_full_bottomsheet/widgets/officesupplies2_item_widget.dart';
import 'models/any_item_model.dart';
import 'models/officesupplies2_item_model.dart';
import 'notifier/filter_full_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_icon_button.dart';
import 'package:oont/widgets/custom_rating_bar.dart';

// ignore_for_file: must_be_immutable
class FilterFullBottomsheet extends ConsumerStatefulWidget {
  const FilterFullBottomsheet({Key? key})
      : super(
          key: key,
        );

  @override
  FilterFullBottomsheetState createState() => FilterFullBottomsheetState();
}

class FilterFullBottomsheetState extends ConsumerState<FilterFullBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(
        horizontal: 20.h,
        vertical: 24.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.customBorderTL30,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildLineFifteen(context),
          SizedBox(height: 40.v),
          CustomElevatedButton(
            text: "lbl_apply_filter".tr,
          ),
          SizedBox(height: 11.v),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildPriceRange(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_price_range".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 22.v),
        SliderTheme(
          data: SliderThemeData(
            trackShape: RoundedRectSliderTrackShape(),
            activeTrackColor: theme.colorScheme.primary,
            inactiveTrackColor: appTheme.gray300,
            thumbColor: theme.colorScheme.primary,
            thumbShape: RoundSliderThumbShape(),
          ),
          child: RangeSlider(
            values: RangeValues(
              0,
              0,
            ),
            min: 0.0,
            max: 100.0,
            onChanged: (value) {},
          ),
        ),
        SizedBox(height: 8.v),
        Padding(
          padding: EdgeInsets.only(left: 17.h),
          child: Row(
            children: [
              Text(
                "lbl_202".tr,
                style: CustomTextStyles.titleSmallGray900_1,
              ),
              Padding(
                padding: EdgeInsets.only(left: 149.h),
                child: Text(
                  "lbl_80".tr,
                  style: CustomTextStyles.titleSmallGray900_1,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildFilterButtonCategories(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_categories".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 23.v),
        Consumer(
          builder: (context, ref, _) {
            return Wrap(
              runSpacing: 13.v,
              spacing: 13.h,
              children: List<Widget>.generate(
                ref
                        .watch(filterFullNotifier)
                        .filterFullModelObj
                        ?.officesupplies2ItemList
                        .length ??
                    0,
                (index) {
                  Officesupplies2ItemModel model = ref
                          .watch(filterFullNotifier)
                          .filterFullModelObj
                          ?.officesupplies2ItemList[index] ??
                      Officesupplies2ItemModel();

                  return Officesupplies2ItemWidget(
                    model,
                    onSelectedChipView1: (value) {
                      ref
                          .read(filterFullNotifier.notifier)
                          .onSelectedChipView1(index, value);
                    },
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildCategories(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_brand".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 25.v),
        Consumer(
          builder: (context, ref, _) {
            return Wrap(
              runSpacing: 13.v,
              spacing: 13.h,
              children: List<Widget>.generate(
                ref
                        .watch(filterFullNotifier)
                        .filterFullModelObj
                        ?.anyItemList
                        .length ??
                    0,
                (index) {
                  AnyItemModel model = ref
                          .watch(filterFullNotifier)
                          .filterFullModelObj
                          ?.anyItemList[index] ??
                      AnyItemModel();

                  return AnyItemWidget(
                    model,
                    onSelectedChipView2: (value) {
                      ref
                          .read(filterFullNotifier.notifier)
                          .onSelectedChipView2(index, value);
                    },
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildRatingStar(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "lbl_rating_star".tr,
          style: CustomTextStyles.titleMediumBold_2,
        ),
        SizedBox(height: 18.v),
        CustomRatingBar(
          initialRating: 0,
          itemSize: 30,
        ),
      ],
    );
  }

  /// Section Widget
  Widget _buildLineFifteen(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 50.h,
          child: Divider(
            color: appTheme.gray900,
          ),
        ),
        SizedBox(height: 26.v),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomIconButton(
              height: 40.adaptSize,
              width: 40.adaptSize,
              padding: EdgeInsets.all(10.h),
              decoration: IconButtonStyleHelper.fillGray,
              child: CustomImageView(
                imagePath: ImageConstant.imgArrowRight,
              ),
            ),
            Spacer(
              flex: 53,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 6.v,
                bottom: 14.v,
              ),
              child: Text(
                "lbl_filter".tr,
                style: CustomTextStyles.titleMediumBold,
              ),
            ),
            Spacer(
              flex: 46,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 6.v,
                bottom: 14.v,
              ),
              child: Text(
                "lbl_reset".tr,
                style: CustomTextStyles.titleMedium18,
              ),
            ),
          ],
        ),
        SizedBox(height: 28.v),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "lbl_sort_by".tr,
              style: CustomTextStyles.titleMediumBold_2,
            ),
            Spacer(),
            Text(
              "lbl_popularity".tr,
              style: CustomTextStyles.titleMediumBold_2,
            ),
            CustomImageView(
              imagePath: ImageConstant.imgArrowDown,
              height: 15.adaptSize,
              width: 15.adaptSize,
              margin: EdgeInsets.only(left: 2.h),
            ),
          ],
        ),
        SizedBox(height: 32.v),
        _buildPriceRange(context),
        SizedBox(height: 32.v),
        _buildFilterButtonCategories(context),
        SizedBox(height: 27.v),
        _buildCategories(context),
        SizedBox(height: 29.v),
        _buildRatingStar(context),
      ],
    );
  }
}
