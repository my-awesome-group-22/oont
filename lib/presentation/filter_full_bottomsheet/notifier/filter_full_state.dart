// ignore_for_file: must_be_immutable

part of 'filter_full_notifier.dart';

/// Represents the state of FilterFull in the application.
class FilterFullState extends Equatable {
  FilterFullState({this.filterFullModelObj});

  FilterFullModel? filterFullModelObj;

  @override
  List<Object?> get props => [
        filterFullModelObj,
      ];

  FilterFullState copyWith({FilterFullModel? filterFullModelObj}) {
    return FilterFullState(
      filterFullModelObj: filterFullModelObj ?? this.filterFullModelObj,
    );
  }
}
