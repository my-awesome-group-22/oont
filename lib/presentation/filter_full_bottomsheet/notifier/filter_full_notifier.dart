import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/officesupplies2_item_model.dart';
import '../models/any_item_model.dart';
import 'package:oont/presentation/filter_full_bottomsheet/models/filter_full_model.dart';
part 'filter_full_state.dart';

final filterFullNotifier =
    StateNotifierProvider<FilterFullNotifier, FilterFullState>(
  (ref) => FilterFullNotifier(FilterFullState(
    filterFullModelObj: FilterFullModel(
        officesupplies2ItemList:
            List.generate(4, (index) => Officesupplies2ItemModel()),
        anyItemList: List.generate(4, (index) => AnyItemModel())),
  )),
);

/// A notifier that manages the state of a FilterFull according to the event that is dispatched to it.
class FilterFullNotifier extends StateNotifier<FilterFullState> {
  FilterFullNotifier(FilterFullState state) : super(state) {}

  void onSelectedChipView1(
    int index,
    bool value,
  ) {
    List<Officesupplies2ItemModel> newList =
        List<Officesupplies2ItemModel>.from(
            state.filterFullModelObj!.officesupplies2ItemList);
    newList[index] = newList[index].copyWith(isSelected: value);
    state = state.copyWith(
        filterFullModelObj: state.filterFullModelObj
            ?.copyWith(officesupplies2ItemList: newList));
  }

  void onSelectedChipView2(
    int index,
    bool value,
  ) {
    List<AnyItemModel> newList =
        List<AnyItemModel>.from(state.filterFullModelObj!.anyItemList);
    newList[index] = newList[index].copyWith(isSelected: value);
    state = state.copyWith(
        filterFullModelObj:
            state.filterFullModelObj?.copyWith(anyItemList: newList));
  }
}
