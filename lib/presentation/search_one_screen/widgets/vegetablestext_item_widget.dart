import '../models/vegetablestext_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class VegetablestextItemWidget extends StatelessWidget {
  VegetablestextItemWidget(
    this.vegetablestextItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  VegetablestextItemModel vegetablestextItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 13.v),
          child: Text(
            vegetablestextItemModelObj.vegetablesText!,
            style: theme.textTheme.bodyLarge,
          ),
        ),
        CustomImageView(
          imagePath: ImageConstant.imgTopLeft,
          height: 15.adaptSize,
          width: 15.adaptSize,
          margin: EdgeInsets.only(
            top: 2.v,
            bottom: 13.v,
          ),
        ),
      ],
    );
  }
}
