// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'vegetablestext_item_model.dart';/// This class defines the variables used in the [search_one_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class SearchOneModel extends Equatable {SearchOneModel({this.vegetablestextItemList = const []}) {  }

List<VegetablestextItemModel> vegetablestextItemList;

SearchOneModel copyWith({List<VegetablestextItemModel>? vegetablestextItemList}) { return SearchOneModel(
vegetablestextItemList : vegetablestextItemList ?? this.vegetablestextItemList,
); } 
@override List<Object?> get props => [vegetablestextItemList];
 }
