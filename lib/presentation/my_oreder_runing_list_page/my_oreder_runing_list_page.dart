import '../my_oreder_runing_list_page/widgets/orderlistcard1_item_widget.dart';
import 'models/orderlistcard1_item_model.dart';
import 'notifier/my_oreder_runing_list_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore_for_file: must_be_immutable
class MyOrederRuningListPage extends ConsumerStatefulWidget {
  const MyOrederRuningListPage({Key? key})
      : super(
          key: key,
        );

  @override
  MyOrederRuningListPageState createState() => MyOrederRuningListPageState();
}

class MyOrederRuningListPageState extends ConsumerState<MyOrederRuningListPage>
    with AutomaticKeepAliveClientMixin<MyOrederRuningListPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray10003,
        body: Container(
          width: double.maxFinite,
          decoration: AppDecoration.fillGray10003,
          child: Column(
            children: [
              SizedBox(height: 20.v),
              _buildOrderListCard(context),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildOrderListCard(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.h),
        child: Consumer(
          builder: (context, ref, _) {
            return ListView.separated(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (
                context,
                index,
              ) {
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0.v),
                  child: SizedBox(
                    width: 78.h,
                    child: Divider(
                      height: 4.v,
                      thickness: 4.v,
                      color: theme.colorScheme.primary,
                    ),
                  ),
                );
              },
              itemCount: ref
                      .watch(myOrederRuningListNotifier)
                      .myOrederRuningListModelObj
                      ?.orderlistcard1ItemList
                      .length ??
                  0,
              itemBuilder: (context, index) {
                Orderlistcard1ItemModel model = ref
                        .watch(myOrederRuningListNotifier)
                        .myOrederRuningListModelObj
                        ?.orderlistcard1ItemList[index] ??
                    Orderlistcard1ItemModel();
                return Orderlistcard1ItemWidget(
                  model,
                );
              },
            );
          },
        ),
      ),
    );
  }
}
