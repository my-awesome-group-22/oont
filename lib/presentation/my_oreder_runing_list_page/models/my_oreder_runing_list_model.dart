// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'orderlistcard1_item_model.dart';/// This class defines the variables used in the [my_oreder_runing_list_page],
/// and is typically used to hold data that is passed between different parts of the application.
class MyOrederRuningListModel extends Equatable {MyOrederRuningListModel({this.orderlistcard1ItemList = const []}) {  }

List<Orderlistcard1ItemModel> orderlistcard1ItemList;

MyOrederRuningListModel copyWith({List<Orderlistcard1ItemModel>? orderlistcard1ItemList}) { return MyOrederRuningListModel(
orderlistcard1ItemList : orderlistcard1ItemList ?? this.orderlistcard1ItemList,
); } 
@override List<Object?> get props => [orderlistcard1ItemList];
 }
