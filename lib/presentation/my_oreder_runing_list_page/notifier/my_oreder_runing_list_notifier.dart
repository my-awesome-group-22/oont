import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/orderlistcard1_item_model.dart';
import 'package:oont/presentation/my_oreder_runing_list_page/models/my_oreder_runing_list_model.dart';
part 'my_oreder_runing_list_state.dart';

final myOrederRuningListNotifier =
    StateNotifierProvider<MyOrederRuningListNotifier, MyOrederRuningListState>(
  (ref) => MyOrederRuningListNotifier(MyOrederRuningListState(
    myOrederRuningListModelObj:
        MyOrederRuningListModel(orderlistcard1ItemList: [
      Orderlistcard1ItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May",
          orderImage: ImageConstant.imgMobile,
          orderStatus: "Confirmed",
          status: "Status:",
          status1: ImageConstant.imgMobile,
          orederID: ImageConstant.imgMobile),
      Orderlistcard1ItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      Orderlistcard1ItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      Orderlistcard1ItemModel(
          orderID: "Oreder ID:",
          orderNumber: "2324252627",
          orderDate: "25 May"),
      Orderlistcard1ItemModel(
          orderID: "Oreder ID:", orderNumber: "2324252627", orderDate: "25 May")
    ]),
  )),
);

/// A notifier that manages the state of a MyOrederRuningList according to the event that is dispatched to it.
class MyOrederRuningListNotifier
    extends StateNotifier<MyOrederRuningListState> {
  MyOrederRuningListNotifier(MyOrederRuningListState state) : super(state) {}
}
