// ignore_for_file: must_be_immutable

part of 'my_oreder_runing_list_notifier.dart';

/// Represents the state of MyOrederRuningList in the application.
class MyOrederRuningListState extends Equatable {
  MyOrederRuningListState({this.myOrederRuningListModelObj});

  MyOrederRuningListModel? myOrederRuningListModelObj;

  @override
  List<Object?> get props => [
        myOrederRuningListModelObj,
      ];

  MyOrederRuningListState copyWith(
      {MyOrederRuningListModel? myOrederRuningListModelObj}) {
    return MyOrederRuningListState(
      myOrederRuningListModelObj:
          myOrederRuningListModelObj ?? this.myOrederRuningListModelObj,
    );
  }
}
