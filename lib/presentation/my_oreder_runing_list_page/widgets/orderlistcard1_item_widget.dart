import '../models/orderlistcard1_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Orderlistcard1ItemWidget extends StatelessWidget {
  Orderlistcard1ItemWidget(
    this.orderlistcard1ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Orderlistcard1ItemModel orderlistcard1ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20.h,
        vertical: 14.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.circleBorder8,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                orderlistcard1ItemModelObj.orderID!,
                style: theme.textTheme.titleSmall,
              ),
              Padding(
                padding: EdgeInsets.only(left: 3.h),
                child: Text(
                  orderlistcard1ItemModelObj.orderNumber!,
                  style: CustomTextStyles.titleSmallGray900_1,
                ),
              ),
              Spacer(),
              Text(
                orderlistcard1ItemModelObj.orderDate!,
                style: theme.textTheme.titleSmall,
              ),
            ],
          ),
          SizedBox(height: 16.v),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 34.v,
                width: 250.h,
                child: Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    CustomImageView(
                      imagePath: orderlistcard1ItemModelObj?.orderImage,
                      height: 14.adaptSize,
                      width: 14.adaptSize,
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(
                        top: 1.v,
                        right: 87.h,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Padding(
                        padding: EdgeInsets.only(left: 60.h),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 14.adaptSize,
                              width: 14.adaptSize,
                              decoration: BoxDecoration(
                                color: theme.colorScheme.primary,
                                borderRadius: BorderRadius.circular(
                                  7.h,
                                ),
                              ),
                            ),
                            SizedBox(height: 4.v),
                            Text(
                              orderlistcard1ItemModelObj.orderStatus!,
                              style: CustomTextStyles.titleSmallPrimary,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: 250.h,
                        margin: EdgeInsets.only(bottom: 19.v),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              orderlistcard1ItemModelObj.status!,
                              style: theme.textTheme.bodyMedium,
                            ),
                            CustomImageView(
                              imagePath: orderlistcard1ItemModelObj?.status1,
                              height: 14.adaptSize,
                              width: 14.adaptSize,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomImageView(
                imagePath: orderlistcard1ItemModelObj?.orederID,
                height: 14.adaptSize,
                width: 14.adaptSize,
                margin: EdgeInsets.only(
                  left: 70.h,
                  bottom: 18.v,
                ),
              ),
            ],
          ),
          SizedBox(height: 4.v),
        ],
      ),
    );
  }
}
