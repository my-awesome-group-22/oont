// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [taxi_menu_draweritem],
/// and is typically used to hold data that is passed between different parts of the application.
class TaxiMenuModel extends Equatable {TaxiMenuModel() {  }

TaxiMenuModel copyWith() { return TaxiMenuModel(
); } 
@override List<Object?> get props => [];
 }
