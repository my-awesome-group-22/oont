import 'notifier/taxi_menu_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore_for_file: must_be_immutable
class TaxiMenuDraweritem extends ConsumerStatefulWidget {
  const TaxiMenuDraweritem({Key? key})
      : super(
          key: key,
        );

  @override
  TaxiMenuDraweritemState createState() => TaxiMenuDraweritemState();
}

class TaxiMenuDraweritemState extends ConsumerState<TaxiMenuDraweritem> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        width: 311.h,
        padding: EdgeInsets.symmetric(
          horizontal: 24.h,
          vertical: 80.v,
        ),
        decoration: AppDecoration.fillWhiteA,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: ImageConstant.imgImage,
                  height: 58.adaptSize,
                  width: 58.adaptSize,
                  radius: BorderRadius.circular(
                    29.h,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 24.h,
                    top: 2.v,
                    bottom: 5.v,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          "lbl_jeihun".tr,
                          style: CustomTextStyles.titleMediumBlack900Bold,
                        ),
                      ),
                      SizedBox(height: 17.v),
                      Text(
                        "lbl_10az100".tr,
                        style: CustomTextStyles.bodyMediumBlack900,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 81.v),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: ImageConstant.imgClockBlack900,
                  height: 24.adaptSize,
                  width: 24.adaptSize,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 12.h,
                    bottom: 5.v,
                  ),
                  child: Text(
                    "lbl_activities".tr,
                    style: CustomTextStyles.titleMediumBlack900Bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 42.v),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: ImageConstant.imgFavoriteBlack900,
                  height: 24.adaptSize,
                  width: 24.adaptSize,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 12.h,
                    top: 3.v,
                    bottom: 3.v,
                  ),
                  child: Text(
                    "lbl_saving_pack".tr,
                    style: CustomTextStyles.titleMediumBlack900Bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 42.v),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: ImageConstant.imgTelevisionBlack900,
                  height: 24.adaptSize,
                  width: 24.adaptSize,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 12.h,
                    top: 3.v,
                    bottom: 3.v,
                  ),
                  child: Text(
                    "lbl_payment_method".tr,
                    style: CustomTextStyles.titleMediumBlack900Bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 42.v),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomImageView(
                  imagePath: ImageConstant.imgProfile,
                  height: 24.adaptSize,
                  width: 24.adaptSize,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 12.h,
                    top: 3.v,
                    bottom: 4.v,
                  ),
                  child: Text(
                    "lbl_help".tr,
                    style: CustomTextStyles.titleMediumBlack900Bold,
                  ),
                ),
              ],
            ),
            Spacer(),
            Text(
              "msg_about_the_application".tr,
              style: CustomTextStyles.titleMediumBlack900Bold,
            ),
            SizedBox(height: 16.v),
            Text(
              "lbl_version_4_43_1".tr,
              style: CustomTextStyles.bodyMediumBlack900,
            ),
            SizedBox(height: 5.v),
          ],
        ),
      ),
    );
  }
}
