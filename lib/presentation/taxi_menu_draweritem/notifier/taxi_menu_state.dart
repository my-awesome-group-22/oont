// ignore_for_file: must_be_immutable

part of 'taxi_menu_notifier.dart';

/// Represents the state of TaxiMenu in the application.
class TaxiMenuState extends Equatable {
  TaxiMenuState({this.taxiMenuModelObj});

  TaxiMenuModel? taxiMenuModelObj;

  @override
  List<Object?> get props => [
        taxiMenuModelObj,
      ];

  TaxiMenuState copyWith({TaxiMenuModel? taxiMenuModelObj}) {
    return TaxiMenuState(
      taxiMenuModelObj: taxiMenuModelObj ?? this.taxiMenuModelObj,
    );
  }
}
