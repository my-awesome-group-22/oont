import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/taxi_menu_draweritem/models/taxi_menu_model.dart';
part 'taxi_menu_state.dart';

final taxiMenuNotifier = StateNotifierProvider<TaxiMenuNotifier, TaxiMenuState>(
  (ref) => TaxiMenuNotifier(TaxiMenuState(
    taxiMenuModelObj: TaxiMenuModel(),
  )),
);

/// A notifier that manages the state of a TaxiMenu according to the event that is dispatched to it.
class TaxiMenuNotifier extends StateNotifier<TaxiMenuState> {
  TaxiMenuNotifier(TaxiMenuState state) : super(state) {}
}
