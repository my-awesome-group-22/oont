// ignore_for_file: must_be_immutable

part of 'order_tracking_notifier.dart';

/// Represents the state of OrderTracking in the application.
class OrderTrackingState extends Equatable {
  OrderTrackingState({this.orderTrackingModelObj});

  OrderTrackingModel? orderTrackingModelObj;

  @override
  List<Object?> get props => [
        orderTrackingModelObj,
      ];

  OrderTrackingState copyWith({OrderTrackingModel? orderTrackingModelObj}) {
    return OrderTrackingState(
      orderTrackingModelObj:
          orderTrackingModelObj ?? this.orderTrackingModelObj,
    );
  }
}
