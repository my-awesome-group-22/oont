import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/order_tracking_bottomsheet/models/order_tracking_model.dart';
part 'order_tracking_state.dart';

final orderTrackingNotifier =
    StateNotifierProvider<OrderTrackingNotifier, OrderTrackingState>(
  (ref) => OrderTrackingNotifier(OrderTrackingState(
    orderTrackingModelObj: OrderTrackingModel(),
  )),
);

/// A notifier that manages the state of a OrderTracking according to the event that is dispatched to it.
class OrderTrackingNotifier extends StateNotifier<OrderTrackingState> {
  OrderTrackingNotifier(OrderTrackingState state) : super(state) {}
}
