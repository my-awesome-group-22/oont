import 'notifier/order_tracking_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_floating_button.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore_for_file: must_be_immutable
class OrderTrackingBottomsheet extends ConsumerStatefulWidget {
  const OrderTrackingBottomsheet({Key? key})
      : super(
          key: key,
        );

  @override
  OrderTrackingBottomsheetState createState() =>
      OrderTrackingBottomsheetState();
}

class OrderTrackingBottomsheetState
    extends ConsumerState<OrderTrackingBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 852.v,
      width: double.maxFinite,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildOrderTracking(context),
              ],
            ),
          ),
          _buildFloatingActionButton(context),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildOrderTracking(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 19.h,
        vertical: 30.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.customBorderTL30,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 50.h,
            child: Divider(
              color: appTheme.gray900,
            ),
          ),
          SizedBox(height: 46.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 1.h),
              child: Row(
                children: [
                  Container(
                    height: 40.adaptSize,
                    width: 40.adaptSize,
                    padding: EdgeInsets.all(8.h),
                    decoration: AppDecoration.fillPrimary.copyWith(
                      borderRadius: BorderRadiusStyle.circleBorder8,
                    ),
                    child: CustomIconButton(
                      height: 24.adaptSize,
                      width: 24.adaptSize,
                      padding: EdgeInsets.all(4.h),
                      decoration: IconButtonStyleHelper.fillWhiteATL12,
                      alignment: Alignment.center,
                      child: CustomImageView(
                        imagePath: ImageConstant.imgVector,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20.h,
                      bottom: 3.v,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "msg_your_delivery_time".tr,
                          style: CustomTextStyles.titleMediumOnError_1,
                        ),
                        SizedBox(height: 1.v),
                        Text(
                          "lbl_30_min".tr,
                          style: theme.textTheme.titleMedium,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 30.v),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 1.h),
              child: Row(
                children: [
                  SizedBox(
                    height: 40.adaptSize,
                    width: 40.adaptSize,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        CustomImageView(
                          imagePath: ImageConstant.imgUserPrimary,
                          height: 40.adaptSize,
                          width: 40.adaptSize,
                          alignment: Alignment.center,
                        ),
                        CustomImageView(
                          imagePath: ImageConstant.imgLinkedinWhiteA700,
                          height: 24.adaptSize,
                          width: 24.adaptSize,
                          alignment: Alignment.center,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 20.h,
                      bottom: 3.v,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "msg_your_delivery_address".tr,
                          style: CustomTextStyles.titleMediumOnError_1,
                        ),
                        SizedBox(height: 1.v),
                        Text(
                          "lbl_london".tr,
                          style: theme.textTheme.titleMedium,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 30.v),
          Divider(),
          SizedBox(height: 24.v),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 40.adaptSize,
                width: 40.adaptSize,
                decoration: AppDecoration.fillAmber.copyWith(
                  borderRadius: BorderRadiusStyle.roundedBorder21,
                ),
                child: CustomImageView(
                  imagePath: ImageConstant.imgPlay40x40,
                  height: 40.adaptSize,
                  width: 40.adaptSize,
                  alignment: Alignment.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "lbl_your_shipper".tr,
                      style: theme.textTheme.titleSmall,
                    ),
                    SizedBox(height: 6.v),
                    Text(
                      "lbl_jondy_carlos".tr,
                      style: CustomTextStyles.titleMediumBold_3,
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 15.v),
        ],
      ),
    );
  }

  /// Section Widget
  Widget _buildFloatingActionButton(BuildContext context) {
    return CustomFloatingButton(
      height: 40,
      width: 40,
      backgroundColor: theme.colorScheme.primary,
      decoration: FloatingButtonStyleHelper.fillPrimaryTL7,
      alignment: Alignment.bottomRight,
      child: CustomImageView(
        imagePath: ImageConstant.imgGroup3170,
        height: 20.0.v,
        width: 20.0.h,
      ),
    );
  }
}
