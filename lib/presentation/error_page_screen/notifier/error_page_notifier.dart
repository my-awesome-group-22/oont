import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/error_page_screen/models/error_page_model.dart';part 'error_page_state.dart';final errorPageNotifier = StateNotifierProvider<ErrorPageNotifier, ErrorPageState>((ref) => ErrorPageNotifier(ErrorPageState(errorPageModelObj: ErrorPageModel())));
/// A notifier that manages the state of a ErrorPage according to the event that is dispatched to it.
class ErrorPageNotifier extends StateNotifier<ErrorPageState> {ErrorPageNotifier(ErrorPageState state) : super(state);

 }
