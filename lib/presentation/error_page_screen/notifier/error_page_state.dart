// ignore_for_file: must_be_immutable

part of 'error_page_notifier.dart';

/// Represents the state of ErrorPage in the application.
class ErrorPageState extends Equatable {
  ErrorPageState({this.errorPageModelObj});

  ErrorPageModel? errorPageModelObj;

  @override
  List<Object?> get props => [
        errorPageModelObj,
      ];

  ErrorPageState copyWith({ErrorPageModel? errorPageModelObj}) {
    return ErrorPageState(
      errorPageModelObj: errorPageModelObj ?? this.errorPageModelObj,
    );
  }
}
