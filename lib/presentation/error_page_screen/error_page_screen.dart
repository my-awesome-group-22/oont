import 'notifier/error_page_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';class ErrorPageScreen extends ConsumerStatefulWidget {const ErrorPageScreen({Key? key}) : super(key: key);

@override ErrorPageScreenState createState() =>  ErrorPageScreenState();

 }
class ErrorPageScreenState extends ConsumerState<ErrorPageScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Container(width: double.maxFinite, padding: EdgeInsets.only(left: 30.h, top: 100.v, right: 30.h), child: Column(children: [Container(margin: EdgeInsets.symmetric(horizontal: 37.h), padding: EdgeInsets.symmetric(horizontal: 18.h, vertical: 39.v), decoration: AppDecoration.fillGray30001.copyWith(borderRadius: BorderRadiusStyle.circleBorder140), child: Column(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [SizedBox(height: 3.v), CustomImageView(imagePath: ImageConstant.imgImage85, height: 162.v, width: 243.h, radius: BorderRadius.circular(140.h)), SizedBox(height: 4.v), Text("lbl_404".tr, style: CustomTextStyles.headlineLargeGreen500)])), SizedBox(height: 29.v), Text("msg_oppss_something".tr, style: theme.textTheme.headlineSmall), SizedBox(height: 19.v), SizedBox(width: 233.h, child: Text("msg_sorry_something".tr, maxLines: 2, overflow: TextOverflow.ellipsis, textAlign: TextAlign.center, style: CustomTextStyles.titleMediumOnError_1.copyWith(height: 1.40))), SizedBox(height: 61.v), CustomElevatedButton(text: "lbl_try_again".tr), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_cart_page".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
