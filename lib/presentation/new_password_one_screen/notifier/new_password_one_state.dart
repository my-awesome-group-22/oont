// ignore_for_file: must_be_immutable

part of 'new_password_one_notifier.dart';

/// Represents the state of NewPasswordOne in the application.
class NewPasswordOneState extends Equatable {
  NewPasswordOneState({
    this.newpasswordController,
    this.passwordController,
    this.newPasswordOneModelObj,
  });

  TextEditingController? newpasswordController;

  TextEditingController? passwordController;

  NewPasswordOneModel? newPasswordOneModelObj;

  @override
  List<Object?> get props => [
        newpasswordController,
        passwordController,
        newPasswordOneModelObj,
      ];

  NewPasswordOneState copyWith({
    TextEditingController? newpasswordController,
    TextEditingController? passwordController,
    NewPasswordOneModel? newPasswordOneModelObj,
  }) {
    return NewPasswordOneState(
      newpasswordController:
          newpasswordController ?? this.newpasswordController,
      passwordController: passwordController ?? this.passwordController,
      newPasswordOneModelObj:
          newPasswordOneModelObj ?? this.newPasswordOneModelObj,
    );
  }
}
