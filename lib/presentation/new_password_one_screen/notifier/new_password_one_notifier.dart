import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/new_password_one_screen/models/new_password_one_model.dart';part 'new_password_one_state.dart';final newPasswordOneNotifier = StateNotifierProvider<NewPasswordOneNotifier, NewPasswordOneState>((ref) => NewPasswordOneNotifier(NewPasswordOneState(newpasswordController: TextEditingController(), passwordController: TextEditingController(), newPasswordOneModelObj: NewPasswordOneModel())));
/// A notifier that manages the state of a NewPasswordOne according to the event that is dispatched to it.
class NewPasswordOneNotifier extends StateNotifier<NewPasswordOneState> {NewPasswordOneNotifier(NewPasswordOneState state) : super(state);

 }
