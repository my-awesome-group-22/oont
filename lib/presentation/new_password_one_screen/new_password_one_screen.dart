import 'notifier/new_password_one_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/core/utils/validation_functions.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_text_form_field.dart';class NewPasswordOneScreen extends ConsumerStatefulWidget {const NewPasswordOneScreen({Key? key}) : super(key: key);

@override NewPasswordOneScreenState createState() =>  NewPasswordOneScreenState();

 }

// ignore_for_file: must_be_immutable
class NewPasswordOneScreenState extends ConsumerState<NewPasswordOneScreen> {GlobalKey<FormState> _formKey = GlobalKey<FormState>();

@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: Form(key: _formKey, child: Container(width: 382.h, margin: EdgeInsets.fromLTRB(16.h, 30.v, 16.h, 5.v), padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 79.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [Align(alignment: Alignment.centerLeft, child: Text("msg_add_new_password".tr, style: theme.textTheme.headlineSmall)), SizedBox(height: 23.v), _buildInputField(context), SizedBox(height: 29.v), _buildInputField1(context), SizedBox(height: 30.v), CustomElevatedButton(text: "lbl_done".tr, buttonStyle: CustomButtonStyles.fillAmber), SizedBox(height: 5.v)]))))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_new_password".tr), styleType: Style.bgFill); } 
/// Section Widget
Widget _buildInputField(BuildContext context) { return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Opacity(opacity: 0.8, child: Text("lbl_new_password".tr, style: CustomTextStyles.titleSmallOnError)), SizedBox(height: 10.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newPasswordOneNotifier).newpasswordController, hintText: "msg".tr, textInputType: TextInputType.visiblePassword, validator: (value) {if (value == null || (!isValidPassword(value, isRequired: true))) {return "err_msg_please_enter_valid_password".tr;} return null;}, obscureText: true);})]); } 
/// Section Widget
Widget _buildInputField1(BuildContext context) { return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [Opacity(opacity: 0.8, child: Text("lbl_retype_password".tr, style: CustomTextStyles.titleSmallOnError)), SizedBox(height: 9.v), Consumer(builder: (context, ref, _) {return CustomTextFormField(controller: ref.watch(newPasswordOneNotifier).passwordController, hintText: "msg".tr, textInputAction: TextInputAction.done, textInputType: TextInputType.visiblePassword, validator: (value) {if (value == null || (!isValidPassword(value, isRequired: true))) {return "err_msg_please_enter_valid_password".tr;} return null;}, obscureText: true);})]); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
