import 'notifier/login_or_signup_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';

class LoginOrSignupScreen extends ConsumerStatefulWidget {
  const LoginOrSignupScreen({Key? key})
      : super(
          key: key,
        );

  @override
  LoginOrSignupScreenState createState() => LoginOrSignupScreenState();
}

class LoginOrSignupScreenState extends ConsumerState<LoginOrSignupScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        backgroundColor: appTheme.gray100,
        body: Container(
          width: SizeUtils.width,
          height: SizeUtils.height,
          decoration: BoxDecoration(
            color: appTheme.gray100,
            image: DecorationImage(
              image: AssetImage(
                ImageConstant.imgLoginOrSignup,
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: SizedBox(
            width: double.maxFinite,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20.h,
                vertical: 46.v,
              ),
              decoration: AppDecoration.gradientWhiteAToBlack900,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 77.v),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.h),
                      child: Text(
                        "lbl_welcome_to_oont".tr,
                        style: theme.textTheme.displaySmall,
                      ),
                    ),
                  ),
                  Spacer(),
                  CustomElevatedButton(
                    text: "msg_continue_as_a_customer".tr,
                  ),
                  SizedBox(height: 20.v),
                  CustomElevatedButton(
                    text: "msg_continue_as_a_deliverer".tr,
                    buttonStyle: CustomButtonStyles.fillAmber,
                    buttonTextStyle: CustomTextStyles.titleMediumBold_1,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
