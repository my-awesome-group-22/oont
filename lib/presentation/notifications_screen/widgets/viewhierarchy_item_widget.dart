import '../models/viewhierarchy_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class ViewhierarchyItemWidget extends StatelessWidget {
  ViewhierarchyItemWidget(
    this.viewhierarchyItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  ViewhierarchyItemModel viewhierarchyItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: 1.v,
            bottom: 44.v,
          ),
          child: CustomIconButton(
            height: 50.adaptSize,
            width: 50.adaptSize,
            padding: EdgeInsets.all(13.h),
            decoration: IconButtonStyleHelper.fillBlueA,
            child: CustomImageView(
              imagePath: viewhierarchyItemModelObj?.thumbsUpImage,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(
              left: 20.h,
              bottom: 11.v,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  viewhierarchyItemModelObj.giftsOfferText!,
                  style: CustomTextStyles.titleMediumBold_2,
                ),
                SizedBox(height: 7.v),
                SizedBox(
                  width: 283.h,
                  child: Text(
                    viewhierarchyItemModelObj.hotDealBuyOneText!,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: CustomTextStyles.titleMediumOnError_1.copyWith(
                      height: 1.20,
                    ),
                  ),
                ),
                SizedBox(height: 2.v),
                Opacity(
                  opacity: 0.8,
                  child: Text(
                    viewhierarchyItemModelObj.nowText!,
                    style: CustomTextStyles.bodyMediumOnError,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
