import '../../../core/app_export.dart';/// This class is used in the [viewhierarchy_item_widget] screen.
class ViewhierarchyItemModel {ViewhierarchyItemModel({this.thumbsUpImage, this.giftsOfferText, this.hotDealBuyOneText, this.nowText, this.id, }) { thumbsUpImage = thumbsUpImage  ?? ImageConstant.imgThumbsUpWhiteA700;giftsOfferText = giftsOfferText  ?? "Gifts Offer";hotDealBuyOneText = hotDealBuyOneText  ?? "Hot Deal Buy one get free one Offer Hery...";nowText = nowText  ?? "Now";id = id  ?? ""; }

String? thumbsUpImage;

String? giftsOfferText;

String? hotDealBuyOneText;

String? nowText;

String? id;

 }
