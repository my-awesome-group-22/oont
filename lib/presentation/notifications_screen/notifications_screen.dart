import '../notifications_screen/widgets/viewhierarchy_item_widget.dart';import 'models/viewhierarchy_item_model.dart';import 'notifier/notifications_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';class NotificationsScreen extends ConsumerStatefulWidget {const NotificationsScreen({Key? key}) : super(key: key);

@override NotificationsScreenState createState() =>  NotificationsScreenState();

 }
class NotificationsScreenState extends ConsumerState<NotificationsScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Padding(padding: EdgeInsets.only(left: 20.h, top: 28.v, right: 20.h), child: Consumer(builder: (context, ref, _) {return ListView.separated(physics: BouncingScrollPhysics(), shrinkWrap: true, separatorBuilder: (context, index) {return Padding(padding: EdgeInsets.symmetric(vertical: 9.5.v), child: SizedBox(width: 304.h, child: Divider(height: 1.v, thickness: 1.v, color: appTheme.gray20001)));}, itemCount: ref.watch(notificationsNotifier).notificationsModelObj?.viewhierarchyItemList.length ?? 0, itemBuilder: (context, index) {ViewhierarchyItemModel model = ref.watch(notificationsNotifier).notificationsModelObj?.viewhierarchyItemList[index] ?? ViewhierarchyItemModel(); return ViewhierarchyItemWidget(model);});})))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_notification".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
