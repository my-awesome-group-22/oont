import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/my_order_previous_list_tab_container_screen/models/my_order_previous_list_tab_container_model.dart';part 'my_order_previous_list_tab_container_state.dart';final myOrderPreviousListTabContainerNotifier = StateNotifierProvider<MyOrderPreviousListTabContainerNotifier, MyOrderPreviousListTabContainerState>((ref) => MyOrderPreviousListTabContainerNotifier(MyOrderPreviousListTabContainerState(myOrderPreviousListTabContainerModelObj: MyOrderPreviousListTabContainerModel())));
/// A notifier that manages the state of a MyOrderPreviousListTabContainer according to the event that is dispatched to it.
class MyOrderPreviousListTabContainerNotifier extends StateNotifier<MyOrderPreviousListTabContainerState> {MyOrderPreviousListTabContainerNotifier(MyOrderPreviousListTabContainerState state) : super(state);

 }
