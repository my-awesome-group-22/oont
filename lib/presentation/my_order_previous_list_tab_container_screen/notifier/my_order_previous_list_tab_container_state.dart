// ignore_for_file: must_be_immutable

part of 'my_order_previous_list_tab_container_notifier.dart';

/// Represents the state of MyOrderPreviousListTabContainer in the application.
class MyOrderPreviousListTabContainerState extends Equatable {
  MyOrderPreviousListTabContainerState(
      {this.myOrderPreviousListTabContainerModelObj});

  MyOrderPreviousListTabContainerModel? myOrderPreviousListTabContainerModelObj;

  @override
  List<Object?> get props => [
        myOrderPreviousListTabContainerModelObj,
      ];

  MyOrderPreviousListTabContainerState copyWith(
      {MyOrderPreviousListTabContainerModel?
          myOrderPreviousListTabContainerModelObj}) {
    return MyOrderPreviousListTabContainerState(
      myOrderPreviousListTabContainerModelObj:
          myOrderPreviousListTabContainerModelObj ??
              this.myOrderPreviousListTabContainerModelObj,
    );
  }
}
