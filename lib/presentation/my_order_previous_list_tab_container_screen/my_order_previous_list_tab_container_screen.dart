import 'notifier/my_order_previous_list_tab_container_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/presentation/my_order_previous_list_page/my_order_previous_list_page.dart';import 'package:oont/presentation/my_oreder_all_list_page/my_oreder_all_list_page.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';class MyOrderPreviousListTabContainerScreen extends ConsumerStatefulWidget {const MyOrderPreviousListTabContainerScreen({Key? key}) : super(key: key);

@override MyOrderPreviousListTabContainerScreenState createState() =>  MyOrderPreviousListTabContainerScreenState();

 }

// ignore_for_file: must_be_immutable
class MyOrderPreviousListTabContainerScreenState extends ConsumerState<MyOrderPreviousListTabContainerScreen> with  TickerProviderStateMixin {late TabController tabviewController;

@override void initState() { super.initState(); tabviewController = TabController(length: 6, vsync: this); } 
@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, body: SizedBox(width: double.maxFinite, child: Column(children: [_buildTabView(context), Expanded(child: SizedBox(height: 738.v, child: TabBarView(controller: tabviewController, children: [MyOrderPreviousListPage(), MyOrderPreviousListPage(), MyOrederAllListPage(), MyOrederAllListPage(), MyOrderPreviousListPage(), MyOrderPreviousListPage()])))])))); } 
/// Section Widget
Widget _buildTabView(BuildContext context) { return Container(decoration: AppDecoration.fillWhiteA, child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [SizedBox(height: 16.v), CustomAppBar(height: 19.v, leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 1.v, bottom: 1.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_my_orders".tr)), SizedBox(height: 44.v), Container(height: 33.v, width: 378.h, margin: EdgeInsets.only(right: 7.h), child: TabBar(controller: tabviewController, labelPadding: EdgeInsets.zero, labelColor: theme.colorScheme.primary, labelStyle: TextStyle(fontSize: 16.fSize, fontFamily: 'Epilogue', fontWeight: FontWeight.w700), unselectedLabelColor: theme.colorScheme.primary, unselectedLabelStyle: TextStyle(fontSize: 12.fSize, fontFamily: 'Epilogue', fontWeight: FontWeight.w500), indicatorColor: theme.colorScheme.primary, tabs: [Tab(child: Text("lbl_all".tr)), Tab(child: Text("lbl_58".tr)), Tab(child: Text("lbl_in_progress".tr)), Tab(child: Text("lbl_142".tr)), Tab(child: Text("lbl_previous".tr)), Tab(child: Text("lbl_44".tr))]))])); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
