import 'notifier/onboarding_two_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingTwoScreen extends ConsumerStatefulWidget {
  const OnboardingTwoScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingTwoScreenState createState() => OnboardingTwoScreenState();
}

class OnboardingTwoScreenState extends ConsumerState<OnboardingTwoScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: theme.colorScheme.primary,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOont,
                height: 99.v,
                width: 242.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
