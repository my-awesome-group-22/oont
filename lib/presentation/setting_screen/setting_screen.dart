import 'notifier/setting_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';class SettingScreen extends ConsumerStatefulWidget {const SettingScreen({Key? key}) : super(key: key);

@override SettingScreenState createState() =>  SettingScreenState();

 }
class SettingScreenState extends ConsumerState<SettingScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(backgroundColor: appTheme.gray10003, appBar: _buildAppBar(context), body: Container(width: 382.h, margin: EdgeInsets.fromLTRB(16.h, 30.v, 16.h, 5.v), padding: EdgeInsets.symmetric(horizontal: 20.h, vertical: 60.v), decoration: AppDecoration.fillWhiteA.copyWith(borderRadius: BorderRadiusStyle.roundedBorder21), child: Column(mainAxisSize: MainAxisSize.min, children: [_buildDrawerPageList(context), SizedBox(height: 17.v), _buildDrawerPageList(context), SizedBox(height: 17.v), _buildDrawerPageList(context), SizedBox(height: 19.v), _buildDrawerPageList(context), SizedBox(height: 17.v), _buildDrawerPageList(context), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_settings".tr), styleType: Style.bgFill); } 
/// Common widget
Widget _buildDrawerPageList(BuildContext context) { return Column(children: [Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Padding(padding: EdgeInsets.only(bottom: 1.v), child: Text("msg_edit_home_address".tr, style: theme.textTheme.titleMedium)), CustomImageView(imagePath: ImageConstant.imgStroke165, height: 15.adaptSize, width: 15.adaptSize, margin: EdgeInsets.only(top: 3.v))]), SizedBox(height: 17.v), Divider()]); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
