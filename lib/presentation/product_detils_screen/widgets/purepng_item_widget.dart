import '../models/purepng_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class PurepngItemWidget extends StatelessWidget {
  PurepngItemWidget(
    this.purepngItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  PurepngItemModel purepngItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: EdgeInsets.all(15.h),
        decoration: AppDecoration.fillGray300011.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder24,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomImageView(
              imagePath: ImageConstant.imgPurepng,
              height: 225.v,
              width: 203.h,
              margin: EdgeInsets.only(
                top: 15.v,
                bottom: 50.v,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 26.h,
                bottom: 242.v,
              ),
              child: CustomIconButton(
                height: 48.adaptSize,
                width: 48.adaptSize,
                padding: EdgeInsets.all(14.h),
                decoration: IconButtonStyleHelper.fillWhiteA,
                child: CustomImageView(
                  imagePath: ImageConstant.imgFavoriteGray900,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
