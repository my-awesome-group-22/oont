import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import '../models/purepng_item_model.dart';import 'package:oont/presentation/product_detils_screen/models/product_detils_model.dart';part 'product_detils_state.dart';final productDetilsNotifier = StateNotifierProvider<ProductDetilsNotifier, ProductDetilsState>((ref) => ProductDetilsNotifier(ProductDetilsState(sliderIndex: 0, productDetilsModelObj: ProductDetilsModel(purepngItemList: List.generate(1, (index) => PurepngItemModel())))));
/// A notifier that manages the state of a ProductDetils according to the event that is dispatched to it.
class ProductDetilsNotifier extends StateNotifier<ProductDetilsState> {ProductDetilsNotifier(ProductDetilsState state) : super(state);

 }
