// ignore_for_file: must_be_immutable

part of 'product_detils_notifier.dart';

/// Represents the state of ProductDetils in the application.
class ProductDetilsState extends Equatable {
  ProductDetilsState({
    this.sliderIndex = 0,
    this.productDetilsModelObj,
  });

  ProductDetilsModel? productDetilsModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        productDetilsModelObj,
      ];

  ProductDetilsState copyWith({
    int? sliderIndex,
    ProductDetilsModel? productDetilsModelObj,
  }) {
    return ProductDetilsState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      productDetilsModelObj:
          productDetilsModelObj ?? this.productDetilsModelObj,
    );
  }
}
