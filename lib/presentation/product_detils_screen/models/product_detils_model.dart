// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import 'purepng_item_model.dart';/// This class defines the variables used in the [product_detils_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class ProductDetilsModel extends Equatable {ProductDetilsModel({this.purepngItemList = const []}) {  }

List<PurepngItemModel> purepngItemList;

ProductDetilsModel copyWith({List<PurepngItemModel>? purepngItemList}) { return ProductDetilsModel(
purepngItemList : purepngItemList ?? this.purepngItemList,
); } 
@override List<Object?> get props => [purepngItemList];
 }
