import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/log_in_one_screen/models/log_in_one_model.dart';part 'log_in_one_state.dart';final logInOneNotifier = StateNotifierProvider<LogInOneNotifier, LogInOneState>((ref) => LogInOneNotifier(LogInOneState(phoneNumberController: TextEditingController(), passwordController: TextEditingController(), isShowPassword: false, logInOneModelObj: LogInOneModel())));
/// A notifier that manages the state of a LogInOne according to the event that is dispatched to it.
class LogInOneNotifier extends StateNotifier<LogInOneState> {LogInOneNotifier(LogInOneState state) : super(state);

void changePasswordVisibility() { state = state.copyWith(isShowPassword: !(state.isShowPassword ?? false)); } 
 }
