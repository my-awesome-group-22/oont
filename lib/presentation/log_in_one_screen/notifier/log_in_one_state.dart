// ignore_for_file: must_be_immutable

part of 'log_in_one_notifier.dart';

/// Represents the state of LogInOne in the application.
class LogInOneState extends Equatable {
  LogInOneState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.logInOneModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  LogInOneModel? logInOneModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        logInOneModelObj,
      ];

  LogInOneState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    LogInOneModel? logInOneModelObj,
  }) {
    return LogInOneState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      logInOneModelObj: logInOneModelObj ?? this.logInOneModelObj,
    );
  }
}
