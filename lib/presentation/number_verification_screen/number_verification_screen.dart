import 'notifier/number_verification_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_elevated_button.dart';
import 'package:oont/widgets/custom_pin_code_text_field.dart';

class NumberVerificationScreen extends ConsumerStatefulWidget {
  const NumberVerificationScreen({Key? key})
      : super(
          key: key,
        );

  @override
  NumberVerificationScreenState createState() =>
      NumberVerificationScreenState();
}

class NumberVerificationScreenState
    extends ConsumerState<NumberVerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        resizeToAvoidBottomInset: false,
        body: Container(
          width: double.maxFinite,
          padding: EdgeInsets.symmetric(horizontal: 16.h),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 5.v),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 25.h,
                    vertical: 43.v,
                  ),
                  decoration: AppDecoration.fillWhiteA.copyWith(
                    borderRadius: BorderRadiusStyle.roundedBorder30,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 12.v),
                      Text(
                        "msg_entry_your_4_digit".tr,
                        style: CustomTextStyles.titleMedium18,
                      ),
                      SizedBox(height: 23.v),
                      Container(
                        height: 160.v,
                        width: 240.h,
                        padding: EdgeInsets.symmetric(vertical: 30.v),
                        decoration: AppDecoration.fillGray30001.copyWith(
                          borderRadius: BorderRadiusStyle.roundedBorder17,
                        ),
                        child: CustomImageView(
                          imagePath: ImageConstant.imgBookmark,
                          height: 100.adaptSize,
                          width: 100.adaptSize,
                          alignment: Alignment.center,
                        ),
                      ),
                      SizedBox(height: 60.v),
                      Consumer(
                        builder: (context, ref, _) {
                          return CustomPinCodeTextField(
                            context: context,
                            controller: ref
                                .watch(numberVerificationNotifier)
                                .otpController,
                            onChanged: (value) {
                              ref
                                  .watch(numberVerificationNotifier)
                                  .otpController
                                  ?.text = value;
                            },
                          );
                        },
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 1.v),
                            child: Text(
                              "msg_did_you_don_t_get".tr,
                              style: theme.textTheme.titleSmall,
                            ),
                          ),
                          Text(
                            "lbl_resend".tr,
                            style: CustomTextStyles.titleSmallPrimaryContainer,
                          ),
                        ],
                      ),
                      SizedBox(height: 45.v),
                      SizedBox(
                        height: 50.v,
                        width: 325.h,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: Padding(
                                padding: EdgeInsets.only(top: 14.v),
                                child: Text(
                                  "lbl_verify_now".tr,
                                  style: CustomTextStyles.titleSmallWhiteA700,
                                ),
                              ),
                            ),
                            CustomElevatedButton(
                              height: 50.v,
                              width: 325.h,
                              text: "lbl_verify".tr,
                              buttonStyle:
                                  CustomButtonStyles.fillPrimaryContainer,
                              alignment: Alignment.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
