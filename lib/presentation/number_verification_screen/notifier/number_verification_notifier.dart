import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/number_verification_screen/models/number_verification_model.dart';
import 'package:sms_autofill/sms_autofill.dart';
part 'number_verification_state.dart';

final numberVerificationNotifier =
    StateNotifierProvider<NumberVerificationNotifier, NumberVerificationState>(
  (ref) => NumberVerificationNotifier(NumberVerificationState(
    otpController: TextEditingController(),
    numberVerificationModelObj: NumberVerificationModel(),
  )),
);

/// A notifier that manages the state of a NumberVerification according to the event that is dispatched to it.
class NumberVerificationNotifier extends StateNotifier<NumberVerificationState>
    with CodeAutoFill {
  NumberVerificationNotifier(NumberVerificationState state) : super(state) {}

  @override
  void codeUpdated() {
    state.otpController?.text = code ?? '';
  }
}
