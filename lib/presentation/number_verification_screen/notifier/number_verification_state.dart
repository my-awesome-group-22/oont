// ignore_for_file: must_be_immutable

part of 'number_verification_notifier.dart';

/// Represents the state of NumberVerification in the application.
class NumberVerificationState extends Equatable {
  NumberVerificationState({
    this.otpController,
    this.numberVerificationModelObj,
  });

  TextEditingController? otpController;

  NumberVerificationModel? numberVerificationModelObj;

  @override
  List<Object?> get props => [
        otpController,
        numberVerificationModelObj,
      ];

  NumberVerificationState copyWith({
    TextEditingController? otpController,
    NumberVerificationModel? numberVerificationModelObj,
  }) {
    return NumberVerificationState(
      otpController: otpController ?? this.otpController,
      numberVerificationModelObj:
          numberVerificationModelObj ?? this.numberVerificationModelObj,
    );
  }
}
