// ignore_for_file: must_be_immutable

part of 'onboarding_eight_notifier.dart';

/// Represents the state of OnboardingEight in the application.
class OnboardingEightState extends Equatable {
  OnboardingEightState({this.onboardingEightModelObj});

  OnboardingEightModel? onboardingEightModelObj;

  @override
  List<Object?> get props => [
        onboardingEightModelObj,
      ];

  OnboardingEightState copyWith(
      {OnboardingEightModel? onboardingEightModelObj}) {
    return OnboardingEightState(
      onboardingEightModelObj:
          onboardingEightModelObj ?? this.onboardingEightModelObj,
    );
  }
}
