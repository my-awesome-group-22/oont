import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/onboarding_eight_screen/models/onboarding_eight_model.dart';
part 'onboarding_eight_state.dart';

final onboardingEightNotifier =
    StateNotifierProvider<OnboardingEightNotifier, OnboardingEightState>(
  (ref) => OnboardingEightNotifier(OnboardingEightState(
    onboardingEightModelObj: OnboardingEightModel(),
  )),
);

/// A notifier that manages the state of a OnboardingEight according to the event that is dispatched to it.
class OnboardingEightNotifier extends StateNotifier<OnboardingEightState> {
  OnboardingEightNotifier(OnboardingEightState state) : super(state) {}
}
