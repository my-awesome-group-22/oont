import 'notifier/onboarding_eight_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingEightScreen extends ConsumerStatefulWidget {
  const OnboardingEightScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingEightScreenState createState() => OnboardingEightScreenState();
}

class OnboardingEightScreenState extends ConsumerState<OnboardingEightScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: theme.colorScheme.primary,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOontGreen500,
                height: 85.v,
                width: 288.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
