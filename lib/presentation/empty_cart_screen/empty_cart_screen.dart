import 'notifier/empty_cart_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';class EmptyCartScreen extends ConsumerStatefulWidget {const EmptyCartScreen({Key? key}) : super(key: key);

@override EmptyCartScreenState createState() =>  EmptyCartScreenState();

 }
class EmptyCartScreenState extends ConsumerState<EmptyCartScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: Container(width: double.maxFinite, padding: EdgeInsets.only(left: 30.h, top: 100.v, right: 30.h), child: Column(children: [Container(height: 280.adaptSize, width: 280.adaptSize, decoration: AppDecoration.fillGray30001.copyWith(borderRadius: BorderRadiusStyle.circleBorder140), child: CustomImageView(imagePath: ImageConstant.imgCartGreen500, height: 134.v, width: 128.h, alignment: Alignment.center)), SizedBox(height: 40.v), Text("msg_your_cart_is_empty".tr, style: theme.textTheme.headlineSmall), SizedBox(height: 15.v), Opacity(opacity: 0.7, child: Text("msg_sorry_you_have2".tr, style: CustomTextStyles.bodyLargeGray900_1)), SizedBox(height: 82.v), CustomElevatedButton(text: "lbl_start_adding".tr, onPressed: () {onTapStartAdding(context);}), SizedBox(height: 5.v)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_empty_cart".tr), styleType: Style.bgFill); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
/// Navigates to the homeContainerScreen when the action is triggered.
onTapStartAdding(BuildContext context) { NavigatorService.pushNamed(AppRoutes.homeContainerScreen, ); } 
 }
