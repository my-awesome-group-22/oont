// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';/// This class defines the variables used in the [empty_cart_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class EmptyCartModel extends Equatable {EmptyCartModel() {  }

EmptyCartModel copyWith() { return EmptyCartModel(
); } 
@override List<Object?> get props => [];
 }
