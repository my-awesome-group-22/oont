// ignore_for_file: must_be_immutable

part of 'empty_cart_notifier.dart';

/// Represents the state of EmptyCart in the application.
class EmptyCartState extends Equatable {
  EmptyCartState({this.emptyCartModelObj});

  EmptyCartModel? emptyCartModelObj;

  @override
  List<Object?> get props => [
        emptyCartModelObj,
      ];

  EmptyCartState copyWith({EmptyCartModel? emptyCartModelObj}) {
    return EmptyCartState(
      emptyCartModelObj: emptyCartModelObj ?? this.emptyCartModelObj,
    );
  }
}
