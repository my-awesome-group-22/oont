import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/empty_cart_screen/models/empty_cart_model.dart';part 'empty_cart_state.dart';final emptyCartNotifier = StateNotifierProvider<EmptyCartNotifier, EmptyCartState>((ref) => EmptyCartNotifier(EmptyCartState(emptyCartModelObj: EmptyCartModel())));
/// A notifier that manages the state of a EmptyCart according to the event that is dispatched to it.
class EmptyCartNotifier extends StateNotifier<EmptyCartState> {EmptyCartNotifier(EmptyCartState state) : super(state);

 }
