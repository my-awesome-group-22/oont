// ignore_for_file: must_be_immutable

part of 'log_in_notifier.dart';

/// Represents the state of LogIn in the application.
class LogInState extends Equatable {
  LogInState({
    this.phoneNumberController,
    this.passwordController,
    this.isShowPassword = true,
    this.logInModelObj,
  });

  TextEditingController? phoneNumberController;

  TextEditingController? passwordController;

  LogInModel? logInModelObj;

  bool isShowPassword;

  @override
  List<Object?> get props => [
        phoneNumberController,
        passwordController,
        isShowPassword,
        logInModelObj,
      ];

  LogInState copyWith({
    TextEditingController? phoneNumberController,
    TextEditingController? passwordController,
    bool? isShowPassword,
    LogInModel? logInModelObj,
  }) {
    return LogInState(
      phoneNumberController:
          phoneNumberController ?? this.phoneNumberController,
      passwordController: passwordController ?? this.passwordController,
      isShowPassword: isShowPassword ?? this.isShowPassword,
      logInModelObj: logInModelObj ?? this.logInModelObj,
    );
  }
}
