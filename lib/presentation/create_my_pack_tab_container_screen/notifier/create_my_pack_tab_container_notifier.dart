import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/create_my_pack_tab_container_screen/models/create_my_pack_tab_container_model.dart';part 'create_my_pack_tab_container_state.dart';final createMyPackTabContainerNotifier = StateNotifierProvider<CreateMyPackTabContainerNotifier, CreateMyPackTabContainerState>((ref) => CreateMyPackTabContainerNotifier(CreateMyPackTabContainerState(searchController: TextEditingController(), createMyPackTabContainerModelObj: CreateMyPackTabContainerModel())));
/// A notifier that manages the state of a CreateMyPackTabContainer according to the event that is dispatched to it.
class CreateMyPackTabContainerNotifier extends StateNotifier<CreateMyPackTabContainerState> {CreateMyPackTabContainerNotifier(CreateMyPackTabContainerState state) : super(state);

 }
