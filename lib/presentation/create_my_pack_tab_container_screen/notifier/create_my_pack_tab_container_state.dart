// ignore_for_file: must_be_immutable

part of 'create_my_pack_tab_container_notifier.dart';

/// Represents the state of CreateMyPackTabContainer in the application.
class CreateMyPackTabContainerState extends Equatable {
  CreateMyPackTabContainerState({
    this.searchController,
    this.createMyPackTabContainerModelObj,
  });

  TextEditingController? searchController;

  CreateMyPackTabContainerModel? createMyPackTabContainerModelObj;

  @override
  List<Object?> get props => [
        searchController,
        createMyPackTabContainerModelObj,
      ];

  CreateMyPackTabContainerState copyWith({
    TextEditingController? searchController,
    CreateMyPackTabContainerModel? createMyPackTabContainerModelObj,
  }) {
    return CreateMyPackTabContainerState(
      searchController: searchController ?? this.searchController,
      createMyPackTabContainerModelObj: createMyPackTabContainerModelObj ??
          this.createMyPackTabContainerModelObj,
    );
  }
}
