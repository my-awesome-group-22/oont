import 'notifier/create_my_pack_tab_container_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_search_view.dart';class CreateMyPackTabContainerScreen extends ConsumerStatefulWidget {const CreateMyPackTabContainerScreen({Key? key}) : super(key: key);

@override CreateMyPackTabContainerScreenState createState() =>  CreateMyPackTabContainerScreenState();

 }

// ignore_for_file: must_be_immutable
class CreateMyPackTabContainerScreenState extends ConsumerState<CreateMyPackTabContainerScreen> with  TickerProviderStateMixin {late TabController tabviewController;

@override void initState() { super.initState(); tabviewController = TabController(length: 4, vsync: this); } 
@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(resizeToAvoidBottomInset: false, appBar: _buildAppBar(context), body: SizedBox(height: 894.v, width: double.maxFinite, child: Stack(alignment: Alignment.topCenter, children: [Container(margin: EdgeInsets.only(top: 130.v), height: 784.v, child: TabBarView(controller: tabviewController, children: [])), _buildSearchColumn(context)])))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_create_my_pack".tr)); } 
/// Section Widget
Widget _buildSearchColumn(BuildContext context) { return Align(alignment: Alignment.topCenter, child: Padding(padding: EdgeInsets.only(left: 20.h, top: 20.v, right: 6.h), child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [Padding(padding: EdgeInsets.only(right: 14.h), child: Consumer(builder: (context, ref, _) {return CustomSearchView(controller: ref.watch(createMyPackTabContainerNotifier).searchController, hintText: "lbl_search_product".tr);})), SizedBox(height: 20.v), Container(height: 40.v, width: 388.h, child: TabBar(controller: tabviewController, labelPadding: EdgeInsets.zero, labelColor: appTheme.whiteA700, labelStyle: TextStyle(fontSize: 14.fSize, fontFamily: 'Epilogue', fontWeight: FontWeight.w700), unselectedLabelColor: theme.colorScheme.onError.withOpacity(1), unselectedLabelStyle: TextStyle(fontSize: 14.fSize, fontFamily: 'Epilogue', fontWeight: FontWeight.w500), indicator: BoxDecoration(color: theme.colorScheme.primary, borderRadius: BorderRadius.circular(20.h)), tabs: [Tab(child: Text("lbl_vegetables".tr)), Tab(child: Text("lbl_meat_fish".tr)), Tab(child: Text("lbl_medicine".tr)), Tab(child: Text("lbl_baby_care".tr))]))]))); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
 }
