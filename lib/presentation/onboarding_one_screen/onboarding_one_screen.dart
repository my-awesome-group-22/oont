import 'notifier/onboarding_one_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class OnboardingOneScreen extends ConsumerStatefulWidget {
  const OnboardingOneScreen({Key? key})
      : super(
          key: key,
        );

  @override
  OnboardingOneScreenState createState() => OnboardingOneScreenState();
}

class OnboardingOneScreenState extends ConsumerState<OnboardingOneScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray100,
        body: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 1.v),
              CustomImageView(
                imagePath: ImageConstant.imgOontPrimary,
                height: 99.v,
                width: 242.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
