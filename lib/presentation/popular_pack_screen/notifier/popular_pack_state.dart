// ignore_for_file: must_be_immutable

part of 'popular_pack_notifier.dart';

/// Represents the state of PopularPack in the application.
class PopularPackState extends Equatable {
  PopularPackState({this.popularPackModelObj});

  PopularPackModel? popularPackModelObj;

  @override
  List<Object?> get props => [
        popularPackModelObj,
      ];

  PopularPackState copyWith({PopularPackModel? popularPackModelObj}) {
    return PopularPackState(
      popularPackModelObj: popularPackModelObj ?? this.popularPackModelObj,
    );
  }
}
