import '../../../core/app_export.dart';/// This class is used in the [bundlepackcardgrid_item_widget] screen.
class BundlepackcardgridItemModel {BundlepackcardgridItemModel({this.image, this.text, this.text1, this.text2, this.text3, this.id, }) { image = image  ?? ImageConstant.imgHiclipartCom6;text = text  ?? "Medium Spice Pack";text1 = text1  ?? "Onion,Oil,Salt...";text2 = text2  ?? "35";text3 = text3  ?? "50.32";id = id  ?? ""; }

String? image;

String? text;

String? text1;

String? text2;

String? text3;

String? id;

 }
