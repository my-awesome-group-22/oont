// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'bundlepackcardgrid_item_model.dart';/// This class defines the variables used in the [popular_pack_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class PopularPackModel extends Equatable {PopularPackModel({this.bundlepackcardgridItemList = const []}) {  }

List<BundlepackcardgridItemModel> bundlepackcardgridItemList;

PopularPackModel copyWith({List<BundlepackcardgridItemModel>? bundlepackcardgridItemList}) { return PopularPackModel(
bundlepackcardgridItemList : bundlepackcardgridItemList ?? this.bundlepackcardgridItemList,
); } 
@override List<Object?> get props => [bundlepackcardgridItemList];
 }
