import '../models/bundlepackcardgrid_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_icon_button.dart';

// ignore: must_be_immutable
class BundlepackcardgridItemWidget extends StatelessWidget {
  BundlepackcardgridItemWidget(
    this.bundlepackcardgridItemModelObj, {
    Key? key,
    this.onTapBundlePackCard,
  }) : super(
          key: key,
        );

  BundlepackcardgridItemModel bundlepackcardgridItemModelObj;

  VoidCallback? onTapBundlePackCard;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTapBundlePackCard!.call();
      },
      child: Container(
        padding: EdgeInsets.all(11.h),
        decoration: AppDecoration.outlineGray20001.copyWith(
          borderRadius: BorderRadiusStyle.roundedBorder21,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 3.v),
            CustomImageView(
              imagePath: bundlepackcardgridItemModelObj?.image,
              height: 92.v,
              width: 84.h,
            ),
            SizedBox(height: 12.v),
            Text(
              bundlepackcardgridItemModelObj.text!,
              style: theme.textTheme.titleMedium,
            ),
            SizedBox(height: 6.v),
            Align(
              alignment: Alignment.centerLeft,
              child: Opacity(
                opacity: 0.8,
                child: Padding(
                  padding: EdgeInsets.only(left: 3.h),
                  child: Text(
                    bundlepackcardgridItemModelObj.text1!,
                    style: CustomTextStyles.titleSmallOnError,
                  ),
                ),
              ),
            ),
            SizedBox(height: 7.v),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 84.h,
                  margin: EdgeInsets.only(
                    top: 6.v,
                    bottom: 4.v,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        bundlepackcardgridItemModelObj.text2!,
                        style: CustomTextStyles.titleMediumBold_2,
                      ),
                      Opacity(
                        opacity: 0.6,
                        child: Text(
                          bundlepackcardgridItemModelObj.text3!,
                          style: CustomTextStyles.bodyLargeOnError.copyWith(
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 44.h),
                  child: CustomIconButton(
                    height: 28.adaptSize,
                    width: 28.adaptSize,
                    padding: EdgeInsets.all(7.h),
                    child: CustomImageView(
                      imagePath: ImageConstant.imgClose,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
