import 'package:equatable/equatable.dart';import 'package:flutter/material.dart';import '/core/app_export.dart';import 'package:oont/presentation/taxi_activities_screen/models/taxi_activities_model.dart';part 'taxi_activities_state.dart';final taxiActivitiesNotifier = StateNotifierProvider<TaxiActivitiesNotifier, TaxiActivitiesState>((ref) => TaxiActivitiesNotifier(TaxiActivitiesState(taxiActivitiesModelObj: TaxiActivitiesModel())));
/// A notifier that manages the state of a TaxiActivities according to the event that is dispatched to it.
class TaxiActivitiesNotifier extends StateNotifier<TaxiActivitiesState> {TaxiActivitiesNotifier(TaxiActivitiesState state) : super(state);

 }
