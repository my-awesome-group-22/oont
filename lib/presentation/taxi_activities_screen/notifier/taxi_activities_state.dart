// ignore_for_file: must_be_immutable

part of 'taxi_activities_notifier.dart';

/// Represents the state of TaxiActivities in the application.
class TaxiActivitiesState extends Equatable {
  TaxiActivitiesState({this.taxiActivitiesModelObj});

  TaxiActivitiesModel? taxiActivitiesModelObj;

  @override
  List<Object?> get props => [
        taxiActivitiesModelObj,
      ];

  TaxiActivitiesState copyWith({TaxiActivitiesModel? taxiActivitiesModelObj}) {
    return TaxiActivitiesState(
      taxiActivitiesModelObj:
          taxiActivitiesModelObj ?? this.taxiActivitiesModelObj,
    );
  }
}
