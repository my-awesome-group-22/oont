import '../checkout_screen/widgets/cardtextlist_item_widget.dart';import '../checkout_screen/widgets/userprofilelist_item_widget.dart';import 'models/cardtextlist_item_model.dart';import 'models/userprofilelist_item_model.dart';import 'notifier/checkout_notifier.dart';import 'package:flutter/material.dart';import 'package:oont/core/app_export.dart';import 'package:oont/widgets/app_bar/appbar_leading_image.dart';import 'package:oont/widgets/app_bar/appbar_subtitle_one.dart';import 'package:oont/widgets/app_bar/custom_app_bar.dart';import 'package:oont/widgets/custom_elevated_button.dart';import 'package:oont/widgets/custom_switch.dart';class CheckoutScreen extends ConsumerStatefulWidget {const CheckoutScreen({Key? key}) : super(key: key);

@override CheckoutScreenState createState() =>  CheckoutScreenState();

 }
class CheckoutScreenState extends ConsumerState<CheckoutScreen> {@override Widget build(BuildContext context) { return SafeArea(child: Scaffold(appBar: _buildAppBar(context), body: SizedBox(width: double.maxFinite, child: Column(children: [SizedBox(height: 25.v), Expanded(child: SingleChildScrollView(child: Padding(padding: EdgeInsets.only(left: 20.h, bottom: 5.v), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [_buildSelectDeliveryRow(context), SizedBox(height: 16.v), _buildUserProfileList(context), SizedBox(height: 20.v), Text("msg_select_payment_system".tr, style: theme.textTheme.titleMedium), SizedBox(height: 16.v), _buildCardTextList(context), SizedBox(height: 305.v), _buildRememberMyCardRow(context)]))))])), bottomNavigationBar: _buildPayNowButton(context))); } 
/// Section Widget
PreferredSizeWidget _buildAppBar(BuildContext context) { return CustomAppBar(leadingWidth: 45.h, leading: AppbarLeadingImage(imagePath: ImageConstant.imgArrowLeftGray900, margin: EdgeInsets.only(left: 20.h, top: 18.v, bottom: 22.v), onTap: () {onTapArrowLeft(context);}), centerTitle: true, title: AppbarSubtitleOne(text: "lbl_checkout".tr)); } 
/// Section Widget
Widget _buildSelectDeliveryRow(BuildContext context) { return Padding(padding: EdgeInsets.only(right: 20.h), child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text("msg_select_delivery".tr, style: theme.textTheme.titleMedium), Text("lbl_add_new".tr, style: CustomTextStyles.titleSmallPrimaryBold)])); } 
/// Section Widget
Widget _buildUserProfileList(BuildContext context) { return Padding(padding: EdgeInsets.only(right: 20.h), child: Consumer(builder: (context, ref, _) {return ListView.separated(physics: NeverScrollableScrollPhysics(), shrinkWrap: true, separatorBuilder: (context, index) {return SizedBox(height: 20.v);}, itemCount: ref.watch(checkoutNotifier).checkoutModelObj?.userprofilelistItemList.length ?? 0, itemBuilder: (context, index) {UserprofilelistItemModel model = ref.watch(checkoutNotifier).checkoutModelObj?.userprofilelistItemList[index] ?? UserprofilelistItemModel(); return UserprofilelistItemWidget(model);});})); } 
/// Section Widget
Widget _buildCardTextList(BuildContext context) { return SizedBox(height: 66.v, child: Consumer(builder: (context, ref, _) {return ListView.separated(scrollDirection: Axis.horizontal, separatorBuilder: (context, index) {return SizedBox(width: 20.h);}, itemCount: ref.watch(checkoutNotifier).checkoutModelObj?.cardtextlistItemList.length ?? 0, itemBuilder: (context, index) {CardtextlistItemModel model = ref.watch(checkoutNotifier).checkoutModelObj?.cardtextlistItemList[index] ?? CardtextlistItemModel(); return CardtextlistItemWidget(model);});})); } 
/// Section Widget
Widget _buildRememberMyCardRow(BuildContext context) { return Padding(padding: EdgeInsets.only(right: 20.h), child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.start, children: [Padding(padding: EdgeInsets.only(top: 3.v, bottom: 5.v), child: Text("msg_remeber_my_card".tr, style: theme.textTheme.titleMedium)), Consumer(builder: (context, ref, _) {return CustomSwitch(value: ref.watch(checkoutNotifier).isSelectedSwitch, onChange: (value) {ref.read(checkoutNotifier.notifier).changeSwitchBox1(value);});})])); } 
/// Section Widget
Widget _buildPayNowButton(BuildContext context) { return CustomElevatedButton(text: "lbl_pay_now".tr, margin: EdgeInsets.only(left: 20.h, right: 20.h, bottom: 30.v), onPressed: () {onTapPayNowButton(context);}); } 

/// Navigates back to the previous screen.
onTapArrowLeft(BuildContext context) { NavigatorService.goBack(); } 
/// Navigates to the orderSuccessfullyScreen when the action is triggered.
onTapPayNowButton(BuildContext context) { NavigatorService.pushNamed(AppRoutes.orderSuccessfullyScreen, ); } 
 }
