import '../models/cardtextlist_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class CardtextlistItemWidget extends StatelessWidget {
  CardtextlistItemWidget(
    this.cardtextlistItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  CardtextlistItemModel cardtextlistItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 27.h,
        vertical: 8.v,
      ),
      decoration: AppDecoration.outlineGreen.copyWith(
        borderRadius: BorderRadiusStyle.circleBorder8,
      ),
      width: 135.h,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomImageView(
            imagePath: cardtextlistItemModelObj?.cardImage,
            height: 30.adaptSize,
            width: 30.adaptSize,
          ),
          SizedBox(height: 4.v),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              cardtextlistItemModelObj.cardText!,
              style: CustomTextStyles.labelLargeGray900,
            ),
          ),
        ],
      ),
    );
  }
}
