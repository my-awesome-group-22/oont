import '../models/userprofilelist_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class UserprofilelistItemWidget extends StatelessWidget {
  UserprofilelistItemWidget(
    this.userprofilelistItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  UserprofilelistItemModel userprofilelistItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 14.h,
        vertical: 13.v,
      ),
      decoration: AppDecoration.outlineGreen.copyWith(
        borderRadius: BorderRadiusStyle.circleBorder8,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomImageView(
            imagePath: userprofilelistItemModelObj?.userImage,
            height: 16.adaptSize,
            width: 16.adaptSize,
            margin: EdgeInsets.only(bottom: 46.v),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 20.h,
              bottom: 2.v,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userprofilelistItemModelObj.homeAddress!,
                  style: CustomTextStyles.titleSmallGray900,
                ),
                SizedBox(height: 8.v),
                Text(
                  userprofilelistItemModelObj.phoneNumber!,
                  style: theme.textTheme.titleSmall,
                ),
                SizedBox(height: 7.v),
                Text(
                  userprofilelistItemModelObj.location!,
                  style: theme.textTheme.titleSmall,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
