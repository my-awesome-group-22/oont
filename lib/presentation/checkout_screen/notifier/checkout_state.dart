// ignore_for_file: must_be_immutable

part of 'checkout_notifier.dart';

/// Represents the state of Checkout in the application.
class CheckoutState extends Equatable {
  CheckoutState({
    this.isSelectedSwitch = false,
    this.checkoutModelObj,
  });

  CheckoutModel? checkoutModelObj;

  bool isSelectedSwitch;

  @override
  List<Object?> get props => [
        isSelectedSwitch,
        checkoutModelObj,
      ];

  CheckoutState copyWith({
    bool? isSelectedSwitch,
    CheckoutModel? checkoutModelObj,
  }) {
    return CheckoutState(
      isSelectedSwitch: isSelectedSwitch ?? this.isSelectedSwitch,
      checkoutModelObj: checkoutModelObj ?? this.checkoutModelObj,
    );
  }
}
