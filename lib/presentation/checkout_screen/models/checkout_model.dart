// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'userprofilelist_item_model.dart';import 'cardtextlist_item_model.dart';/// This class defines the variables used in the [checkout_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class CheckoutModel extends Equatable {CheckoutModel({this.userprofilelistItemList = const [], this.cardtextlistItemList = const [], }) {  }

List<UserprofilelistItemModel> userprofilelistItemList;

List<CardtextlistItemModel> cardtextlistItemList;

CheckoutModel copyWith({List<UserprofilelistItemModel>? userprofilelistItemList, List<CardtextlistItemModel>? cardtextlistItemList, }) { return CheckoutModel(
userprofilelistItemList : userprofilelistItemList ?? this.userprofilelistItemList,
cardtextlistItemList : cardtextlistItemList ?? this.cardtextlistItemList,
); } 
@override List<Object?> get props => [userprofilelistItemList,cardtextlistItemList];
 }
