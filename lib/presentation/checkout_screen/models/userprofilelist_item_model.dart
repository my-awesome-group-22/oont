import '../../../core/app_export.dart';/// This class is used in the [userprofilelist_item_widget] screen.
class UserprofilelistItemModel {UserprofilelistItemModel({this.userImage, this.homeAddress, this.phoneNumber, this.location, this.id, }) { userImage = userImage  ?? ImageConstant.imgClock;homeAddress = homeAddress  ?? "Home Address";phoneNumber = phoneNumber  ?? "(309) 071-9396-939";location = location  ?? "UK, London";id = id  ?? ""; }

String? userImage;

String? homeAddress;

String? phoneNumber;

String? location;

String? id;

 }
