import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import 'package:oont/presentation/k77_bottomsheet/models/k77_model.dart';
part 'k77_state.dart';

final k77Notifier = StateNotifierProvider<K77Notifier, K77State>(
  (ref) => K77Notifier(K77State(
    k77ModelObj: K77Model(),
  )),
);

/// A notifier that manages the state of a K77 according to the event that is dispatched to it.
class K77Notifier extends StateNotifier<K77State> {
  K77Notifier(K77State state) : super(state) {}
}
