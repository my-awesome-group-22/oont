// ignore_for_file: must_be_immutable

part of 'k77_notifier.dart';

/// Represents the state of K77 in the application.
class K77State extends Equatable {
  K77State({this.k77ModelObj});

  K77Model? k77ModelObj;

  @override
  List<Object?> get props => [
        k77ModelObj,
      ];

  K77State copyWith({K77Model? k77ModelObj}) {
    return K77State(
      k77ModelObj: k77ModelObj ?? this.k77ModelObj,
    );
  }
}
