import '../my_order_previous_list_page/widgets/orderlistcard2_item_widget.dart';
import 'models/orderlistcard2_item_model.dart';
import 'notifier/my_order_previous_list_notifier.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore_for_file: must_be_immutable
class MyOrderPreviousListPage extends ConsumerStatefulWidget {
  const MyOrderPreviousListPage({Key? key})
      : super(
          key: key,
        );

  @override
  MyOrderPreviousListPageState createState() => MyOrderPreviousListPageState();
}

class MyOrderPreviousListPageState
    extends ConsumerState<MyOrderPreviousListPage>
    with AutomaticKeepAliveClientMixin<MyOrderPreviousListPage> {
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: appTheme.gray10003,
        body: Container(
          width: double.maxFinite,
          decoration: AppDecoration.fillGray10003,
          child: Column(
            children: [
              SizedBox(height: 20.v),
              _buildOrderListCard(context),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildOrderListCard(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.h),
        child: Consumer(
          builder: (context, ref, _) {
            return ListView.separated(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (
                context,
                index,
              ) {
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0.v),
                  child: SizedBox(
                    width: 78.h,
                    child: Divider(
                      height: 4.v,
                      thickness: 4.v,
                      color: theme.colorScheme.primary,
                    ),
                  ),
                );
              },
              itemCount: ref
                      .watch(myOrderPreviousListNotifier)
                      .myOrderPreviousListModelObj
                      ?.orderlistcard2ItemList
                      .length ??
                  0,
              itemBuilder: (context, index) {
                Orderlistcard2ItemModel model = ref
                        .watch(myOrderPreviousListNotifier)
                        .myOrderPreviousListModelObj
                        ?.orderlistcard2ItemList[index] ??
                    Orderlistcard2ItemModel();
                return Orderlistcard2ItemWidget(
                  model,
                );
              },
            );
          },
        ),
      ),
    );
  }
}
