import '../models/orderlistcard2_item_model.dart';
import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

// ignore: must_be_immutable
class Orderlistcard2ItemWidget extends StatelessWidget {
  Orderlistcard2ItemWidget(
    this.orderlistcard2ItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  Orderlistcard2ItemModel orderlistcard2ItemModelObj;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20.h,
        vertical: 14.v,
      ),
      decoration: AppDecoration.fillWhiteA.copyWith(
        borderRadius: BorderRadiusStyle.circleBorder8,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                orderlistcard2ItemModelObj.orderID!,
                style: theme.textTheme.titleSmall,
              ),
              Padding(
                padding: EdgeInsets.only(left: 3.h),
                child: Text(
                  orderlistcard2ItemModelObj.mobileNo!,
                  style: CustomTextStyles.titleSmallGray900_1,
                ),
              ),
              Spacer(),
              Text(
                orderlistcard2ItemModelObj.month!,
                style: theme.textTheme.titleSmall,
              ),
            ],
          ),
          SizedBox(height: 16.v),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 15.v,
                width: 250.h,
                margin: EdgeInsets.only(bottom: 19.v),
                child: Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    CustomImageView(
                      imagePath: ImageConstant.imgPlayPrimary,
                      height: 14.adaptSize,
                      width: 14.adaptSize,
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(right: 87.h),
                    ),
                    CustomImageView(
                      imagePath: ImageConstant.imgPlayPrimary,
                      height: 14.adaptSize,
                      width: 14.adaptSize,
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(left: 60.h),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 250.h,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              orderlistcard2ItemModelObj.statusLabel!,
                              style: theme.textTheme.bodyMedium,
                            ),
                            CustomImageView(
                              imagePath: ImageConstant.imgPlayPrimary,
                              height: 14.adaptSize,
                              width: 14.adaptSize,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 18.h),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        height: 14.adaptSize,
                        width: 14.adaptSize,
                        decoration: BoxDecoration(
                          color: theme.colorScheme.primary,
                          borderRadius: BorderRadius.circular(
                            7.h,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 4.v),
                    Text(
                      orderlistcard2ItemModelObj.deliveryText!,
                      style: CustomTextStyles.titleSmallPrimary,
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 4.v),
        ],
      ),
    );
  }
}
