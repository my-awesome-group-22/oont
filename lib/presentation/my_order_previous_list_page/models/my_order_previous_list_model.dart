// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';import '../../../core/app_export.dart';import 'orderlistcard2_item_model.dart';/// This class defines the variables used in the [my_order_previous_list_page],
/// and is typically used to hold data that is passed between different parts of the application.
class MyOrderPreviousListModel extends Equatable {MyOrderPreviousListModel({this.orderlistcard2ItemList = const []}) {  }

List<Orderlistcard2ItemModel> orderlistcard2ItemList;

MyOrderPreviousListModel copyWith({List<Orderlistcard2ItemModel>? orderlistcard2ItemList}) { return MyOrderPreviousListModel(
orderlistcard2ItemList : orderlistcard2ItemList ?? this.orderlistcard2ItemList,
); } 
@override List<Object?> get props => [orderlistcard2ItemList];
 }
