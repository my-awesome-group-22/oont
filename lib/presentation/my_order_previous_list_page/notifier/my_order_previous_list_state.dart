// ignore_for_file: must_be_immutable

part of 'my_order_previous_list_notifier.dart';

/// Represents the state of MyOrderPreviousList in the application.
class MyOrderPreviousListState extends Equatable {
  MyOrderPreviousListState({this.myOrderPreviousListModelObj});

  MyOrderPreviousListModel? myOrderPreviousListModelObj;

  @override
  List<Object?> get props => [
        myOrderPreviousListModelObj,
      ];

  MyOrderPreviousListState copyWith(
      {MyOrderPreviousListModel? myOrderPreviousListModelObj}) {
    return MyOrderPreviousListState(
      myOrderPreviousListModelObj:
          myOrderPreviousListModelObj ?? this.myOrderPreviousListModelObj,
    );
  }
}
