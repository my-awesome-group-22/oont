import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/orderlistcard2_item_model.dart';
import 'package:oont/presentation/my_order_previous_list_page/models/my_order_previous_list_model.dart';
part 'my_order_previous_list_state.dart';

final myOrderPreviousListNotifier = StateNotifierProvider<
    MyOrderPreviousListNotifier, MyOrderPreviousListState>(
  (ref) => MyOrderPreviousListNotifier(MyOrderPreviousListState(
    myOrderPreviousListModelObj:
        MyOrderPreviousListModel(orderlistcard2ItemList: [
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:",
          mobileNo: "2324252627",
          month: "25 May",
          statusLabel: "Status:",
          deliveryText: "Delivered"),
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:", mobileNo: "2324252627", month: "25 May"),
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:", mobileNo: "2324252627", month: "25 May"),
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:", mobileNo: "2324252627", month: "25 May"),
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:", mobileNo: "2324252627", month: "25 May"),
      Orderlistcard2ItemModel(
          orderID: "Oreder ID:", mobileNo: "2324252627", month: "25 May")
    ]),
  )),
);

/// A notifier that manages the state of a MyOrderPreviousList according to the event that is dispatched to it.
class MyOrderPreviousListNotifier
    extends StateNotifier<MyOrderPreviousListState> {
  MyOrderPreviousListNotifier(MyOrderPreviousListState state) : super(state) {}
}
