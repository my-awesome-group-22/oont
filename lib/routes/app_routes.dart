import 'package:flutter/material.dart';
import 'package:oont/presentation/login_or_signup_three_screen/login_or_signup_three_screen.dart';
import 'package:oont/presentation/onboarding_two_screen/onboarding_two_screen.dart';
import 'package:oont/presentation/onboarding_one_screen/onboarding_one_screen.dart';
import 'package:oont/presentation/onboarding_three_screen/onboarding_three_screen.dart';
import 'package:oont/presentation/onboarding_screen/onboarding_screen.dart';
import 'package:oont/presentation/onboarding_four_screen/onboarding_four_screen.dart';
import 'package:oont/presentation/login_or_signup_screen/login_or_signup_screen.dart';
import 'package:oont/presentation/login_or_signup_two_screen/login_or_signup_two_screen.dart';
import 'package:oont/presentation/login_or_signup_one_screen/login_or_signup_one_screen.dart';
import 'package:oont/presentation/log_in_screen/log_in_screen.dart';
import 'package:oont/presentation/sign_up_screen/sign_up_screen.dart';
import 'package:oont/presentation/number_verification_screen/number_verification_screen.dart';
import 'package:oont/presentation/log_in_one_screen/log_in_one_screen.dart';
import 'package:oont/presentation/error_page_password_screen/error_page_password_screen.dart';
import 'package:oont/presentation/error_page_phone_number_screen/error_page_phone_number_screen.dart';
import 'package:oont/presentation/forget_password_screen/forget_password_screen.dart';
import 'package:oont/presentation/new_password_screen/new_password_screen.dart';
import 'package:oont/presentation/new_item_screen/new_item_screen.dart';
import 'package:oont/presentation/popular_pack_screen/popular_pack_screen.dart';
import 'package:oont/presentation/bundle_product_detils_screen/bundle_product_detils_screen.dart';
import 'package:oont/presentation/create_my_pack_tab_container_screen/create_my_pack_tab_container_screen.dart';
import 'package:oont/presentation/bundle_details_page_screen/bundle_details_page_screen.dart';
import 'package:oont/presentation/empty_cart_page_screen/empty_cart_page_screen.dart';
import 'package:oont/presentation/checkout_screen/checkout_screen.dart';
import 'package:oont/presentation/order_successfully_screen/order_successfully_screen.dart';
import 'package:oont/presentation/sorry_order_is_failed_screen/sorry_order_is_failed_screen.dart';
import 'package:oont/presentation/no_oreder_yet_screen/no_oreder_yet_screen.dart';
import 'package:oont/presentation/vegetable_category_screen/vegetable_category_screen.dart';
import 'package:oont/presentation/search_one_screen/search_one_screen.dart';
import 'package:oont/presentation/search_screen/search_screen.dart';
import 'package:oont/presentation/filter_popup_screen/filter_popup_screen.dart';
import 'package:oont/presentation/search_result_screen/search_result_screen.dart';
import 'package:oont/presentation/home_container_screen/home_container_screen.dart';
import 'package:oont/presentation/product_detils_screen/product_detils_screen.dart';
import 'package:oont/presentation/cart_page_screen/cart_page_screen.dart';
import 'package:oont/presentation/empty_wishlist_page_screen/empty_wishlist_page_screen.dart';
import 'package:oont/presentation/empty_cart_screen/empty_cart_screen.dart';
import 'package:oont/presentation/taxi_activities_screen/taxi_activities_screen.dart';
import 'package:oont/presentation/notifications_screen/notifications_screen.dart';
import 'package:oont/presentation/my_order_previous_list_tab_container_screen/my_order_previous_list_tab_container_screen.dart';
import 'package:oont/presentation/delivery_address_screen/delivery_address_screen.dart';
import 'package:oont/presentation/new_addres_screen/new_addres_screen.dart';
import 'package:oont/presentation/profile_screen/profile_screen.dart';
import 'package:oont/presentation/notification_screen/notification_screen.dart';
import 'package:oont/presentation/setting_screen/setting_screen.dart';
import 'package:oont/presentation/change_password_screen/change_password_screen.dart';
import 'package:oont/presentation/change_phone_number_screen/change_phone_number_screen.dart';
import 'package:oont/presentation/my_order_details_screen/my_order_details_screen.dart';
import 'package:oont/presentation/error_page_screen/error_page_screen.dart';
import 'package:oont/presentation/payment_method_one_screen/payment_method_one_screen.dart';
import 'package:oont/presentation/onboarding_five_screen/onboarding_five_screen.dart';
import 'package:oont/presentation/onboarding_nine_screen/onboarding_nine_screen.dart';
import 'package:oont/presentation/drive_method_screen/drive_method_screen.dart';
import 'package:oont/presentation/bank_details_screen/bank_details_screen.dart';
import 'package:oont/presentation/onboarding_eight_screen/onboarding_eight_screen.dart';
import 'package:oont/presentation/onboarding_white_screen/onboarding_white_screen.dart';
import 'package:oont/presentation/onboarding_six_screen/onboarding_six_screen.dart';
import 'package:oont/presentation/onboarding_ten_screen/onboarding_ten_screen.dart';
import 'package:oont/presentation/onboarding_seven_screen/onboarding_seven_screen.dart';
import 'package:oont/presentation/login_or_signup_four_screen/login_or_signup_four_screen.dart';
import 'package:oont/presentation/login_or_signup_five_screen/login_or_signup_five_screen.dart';
import 'package:oont/presentation/log_in_two_screen/log_in_two_screen.dart';
import 'package:oont/presentation/login_or_signup_six_screen/login_or_signup_six_screen.dart';
import 'package:oont/presentation/sign_up_one_screen/sign_up_one_screen.dart';
import 'package:oont/presentation/log_in_three_screen/log_in_three_screen.dart';
import 'package:oont/presentation/error_page_password_one_screen/error_page_password_one_screen.dart';
import 'package:oont/presentation/error_page_phone_number_one_screen/error_page_phone_number_one_screen.dart';
import 'package:oont/presentation/forget_password_one_screen/forget_password_one_screen.dart';
import 'package:oont/presentation/new_password_one_screen/new_password_one_screen.dart';
import 'package:oont/presentation/app_navigation_screen/app_navigation_screen.dart';

class AppRoutes {
  static const String loginOrSignupThreeScreen =
      '/login_or_signup_three_screen';

  static const String onboardingTwoScreen = '/onboarding_two_screen';

  static const String onboardingOneScreen = '/onboarding_one_screen';

  static const String onboardingThreeScreen = '/onboarding_three_screen';

  static const String onboardingScreen = '/onboarding_screen';

  static const String onboardingFourScreen = '/onboarding_four_screen';

  static const String loginOrSignupScreen = '/login_or_signup_screen';

  static const String loginOrSignupTwoScreen = '/login_or_signup_two_screen';

  static const String loginOrSignupOneScreen = '/login_or_signup_one_screen';

  static const String logInScreen = '/log_in_screen';

  static const String signUpScreen = '/sign_up_screen';

  static const String numberVerificationScreen = '/number_verification_screen';

  static const String logInOneScreen = '/log_in_one_screen';

  static const String errorPagePasswordScreen = '/error_page_password_screen';

  static const String errorPagePhoneNumberScreen =
      '/error_page_phone_number_screen';

  static const String forgetPasswordScreen = '/forget_password_screen';

  static const String newPasswordScreen = '/new_password_screen';

  static const String newItemScreen = '/new_item_screen';

  static const String popularPackScreen = '/popular_pack_screen';

  static const String bundleProductDetilsScreen =
      '/bundle_product_detils_screen';

  static const String createMyPackPage = '/create_my_pack_page';

  static const String createMyPackTabContainerScreen =
      '/create_my_pack_tab_container_screen';

  static const String bundleDetailsPageScreen = '/bundle_details_page_screen';

  static const String emptyCartPageScreen = '/empty_cart_page_screen';

  static const String checkoutScreen = '/checkout_screen';

  static const String orderSuccessfullyScreen = '/order_successfully_screen';

  static const String sorryOrderIsFailedScreen =
      '/sorry_order_is_failed_screen';

  static const String noOrederYetScreen = '/no_oreder_yet_screen';

  static const String categoryPage = '/category_page';

  static const String vegetableCategoryScreen = '/vegetable_category_screen';

  static const String searchOneScreen = '/search_one_screen';

  static const String searchScreen = '/search_screen';

  static const String filterPopupScreen = '/filter_popup_screen';

  static const String searchResultScreen = '/search_result_screen';

  static const String homePage = '/home_page';

  static const String homeContainerScreen = '/home_container_screen';

  static const String productDetilsScreen = '/product_detils_screen';

  static const String cartPageScreen = '/cart_page_screen';

  static const String favoriteListPage = '/favorite_list_page';

  static const String emptyWishlistPageScreen = '/empty_wishlist_page_screen';

  static const String emptyCartScreen = '/empty_cart_screen';

  static const String taxiActivitiesScreen = '/taxi_activities_screen';

  static const String notificationsScreen = '/notifications_screen';

  static const String profileTwoPage = '/profile_two_page';

  static const String myOrederAllListPage = '/my_oreder_all_list_page';

  static const String myOrederRuningListPage = '/my_oreder_runing_list_page';

  static const String myOrderPreviousListPage = '/my_order_previous_list_page';

  static const String myOrderPreviousListTabContainerScreen =
      '/my_order_previous_list_tab_container_screen';

  static const String deliveryAddressScreen = '/delivery_address_screen';

  static const String newAddresScreen = '/new_addres_screen';

  static const String profileScreen = '/profile_screen';

  static const String notificationScreen = '/notification_screen';

  static const String settingScreen = '/setting_screen';

  static const String changePasswordScreen = '/change_password_screen';

  static const String changePhoneNumberScreen = '/change_phone_number_screen';

  static const String myOrderDetailsScreen = '/my_order_details_screen';

  static const String errorPageScreen = '/error_page_screen';

  static const String paymentMethodOneScreen = '/payment_method_one_screen';

  static const String onboardingFiveScreen = '/onboarding_five_screen';

  static const String onboardingNineScreen = '/onboarding_nine_screen';

  static const String driveMethodScreen = '/drive_method_screen';

  static const String bankDetailsScreen = '/bank_details_screen';

  static const String onboardingEightScreen = '/onboarding_eight_screen';

  static const String onboardingWhiteScreen = '/onboarding_white_screen';

  static const String onboardingSixScreen = '/onboarding_six_screen';

  static const String onboardingTenScreen = '/onboarding_ten_screen';

  static const String onboardingSevenScreen = '/onboarding_seven_screen';

  static const String loginOrSignupFourScreen = '/login_or_signup_four_screen';

  static const String loginOrSignupFiveScreen = '/login_or_signup_five_screen';

  static const String logInTwoScreen = '/log_in_two_screen';

  static const String loginOrSignupSixScreen = '/login_or_signup_six_screen';

  static const String signUpOneScreen = '/sign_up_one_screen';

  static const String logInThreeScreen = '/log_in_three_screen';

  static const String errorPagePasswordOneScreen =
      '/error_page_password_one_screen';

  static const String errorPagePhoneNumberOneScreen =
      '/error_page_phone_number_one_screen';

  static const String forgetPasswordOneScreen = '/forget_password_one_screen';

  static const String newPasswordOneScreen = '/new_password_one_screen';

  static const String appNavigationScreen = '/app_navigation_screen';

  static const String initialRoute = '/initialRoute';

  static Map<String, WidgetBuilder> routes = {
    loginOrSignupThreeScreen: (context) => LoginOrSignupThreeScreen(),
    onboardingTwoScreen: (context) => OnboardingTwoScreen(),
    onboardingOneScreen: (context) => OnboardingOneScreen(),
    onboardingThreeScreen: (context) => OnboardingThreeScreen(),
    onboardingScreen: (context) => OnboardingScreen(),
    onboardingFourScreen: (context) => OnboardingFourScreen(),
    loginOrSignupScreen: (context) => LoginOrSignupScreen(),
    loginOrSignupTwoScreen: (context) => LoginOrSignupTwoScreen(),
    loginOrSignupOneScreen: (context) => LoginOrSignupOneScreen(),
    logInScreen: (context) => LogInScreen(),
    signUpScreen: (context) => SignUpScreen(),
    numberVerificationScreen: (context) => NumberVerificationScreen(),
    logInOneScreen: (context) => LogInOneScreen(),
    errorPagePasswordScreen: (context) => ErrorPagePasswordScreen(),
    errorPagePhoneNumberScreen: (context) => ErrorPagePhoneNumberScreen(),
    forgetPasswordScreen: (context) => ForgetPasswordScreen(),
    newPasswordScreen: (context) => NewPasswordScreen(),
    newItemScreen: (context) => NewItemScreen(),
    popularPackScreen: (context) => PopularPackScreen(),
    bundleProductDetilsScreen: (context) => BundleProductDetilsScreen(),
    createMyPackTabContainerScreen: (context) =>
        CreateMyPackTabContainerScreen(),
    bundleDetailsPageScreen: (context) => BundleDetailsPageScreen(),
    emptyCartPageScreen: (context) => EmptyCartPageScreen(),
    checkoutScreen: (context) => CheckoutScreen(),
    orderSuccessfullyScreen: (context) => OrderSuccessfullyScreen(),
    sorryOrderIsFailedScreen: (context) => SorryOrderIsFailedScreen(),
    noOrederYetScreen: (context) => NoOrederYetScreen(),
    vegetableCategoryScreen: (context) => VegetableCategoryScreen(),
    searchOneScreen: (context) => SearchOneScreen(),
    searchScreen: (context) => SearchScreen(),
    filterPopupScreen: (context) => FilterPopupScreen(),
    searchResultScreen: (context) => SearchResultScreen(),
    homeContainerScreen: (context) => HomeContainerScreen(),
    productDetilsScreen: (context) => ProductDetilsScreen(),
    cartPageScreen: (context) => CartPageScreen(),
    emptyWishlistPageScreen: (context) => EmptyWishlistPageScreen(),
    emptyCartScreen: (context) => EmptyCartScreen(),
    taxiActivitiesScreen: (context) => TaxiActivitiesScreen(),
    notificationsScreen: (context) => NotificationsScreen(),
    myOrderPreviousListTabContainerScreen: (context) =>
        MyOrderPreviousListTabContainerScreen(),
    deliveryAddressScreen: (context) => DeliveryAddressScreen(),
    newAddresScreen: (context) => NewAddresScreen(),
    profileScreen: (context) => ProfileScreen(),
    notificationScreen: (context) => NotificationScreen(),
    settingScreen: (context) => SettingScreen(),
    changePasswordScreen: (context) => ChangePasswordScreen(),
    changePhoneNumberScreen: (context) => ChangePhoneNumberScreen(),
    myOrderDetailsScreen: (context) => MyOrderDetailsScreen(),
    errorPageScreen: (context) => ErrorPageScreen(),
    paymentMethodOneScreen: (context) => PaymentMethodOneScreen(),
    onboardingFiveScreen: (context) => OnboardingFiveScreen(),
    onboardingNineScreen: (context) => OnboardingNineScreen(),
    driveMethodScreen: (context) => DriveMethodScreen(),
    bankDetailsScreen: (context) => BankDetailsScreen(),
    onboardingEightScreen: (context) => OnboardingEightScreen(),
    onboardingWhiteScreen: (context) => OnboardingWhiteScreen(),
    onboardingSixScreen: (context) => OnboardingSixScreen(),
    onboardingTenScreen: (context) => OnboardingTenScreen(),
    onboardingSevenScreen: (context) => OnboardingSevenScreen(),
    loginOrSignupFourScreen: (context) => LoginOrSignupFourScreen(),
    loginOrSignupFiveScreen: (context) => LoginOrSignupFiveScreen(),
    logInTwoScreen: (context) => LogInTwoScreen(),
    loginOrSignupSixScreen: (context) => LoginOrSignupSixScreen(),
    signUpOneScreen: (context) => SignUpOneScreen(),
    logInThreeScreen: (context) => LogInThreeScreen(),
    errorPagePasswordOneScreen: (context) => ErrorPagePasswordOneScreen(),
    errorPagePhoneNumberOneScreen: (context) => ErrorPagePhoneNumberOneScreen(),
    forgetPasswordOneScreen: (context) => ForgetPasswordOneScreen(),
    newPasswordOneScreen: (context) => NewPasswordOneScreen(),
    appNavigationScreen: (context) => AppNavigationScreen(),
    initialRoute: (context) => LoginOrSignupThreeScreen(),
  };
}
