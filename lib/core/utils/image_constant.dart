class ImageConstant {
  // Image folder path
  static String imagePath = 'assets/images';

  // Onboarding Two images
  static String imgOont = '$imagePath/img_oont.svg';

  // Onboarding One images
  static String imgOontPrimary = '$imagePath/img_oont_primary.svg';

  // Onboarding Four images
  static String img100Primary = '$imagePath/img_100_primary.svg';

  // Number verification images
  static String imgBookmark = '$imagePath/img_bookmark.svg';

  // Verification Successful images
  static String imgGroup3832 = '$imagePath/img_group_3832.svg';

  // 16_Light_set your location-permission images
  static String imgLinkedin = '$imagePath/img_linkedin.svg';

  // 16_Light_set your location images
  static String imgFrame = '$imagePath/img_frame.svg';

  static String imgLinkedinGray90003 =
      '$imagePath/img_linkedin_gray_900_03.svg';

  // New Item images
  static String imgUnnamed3x1 = '$imagePath/img_unnamed_3x_1.png';

  // Popular Pack images
  static String imgHiclipartCom3 = '$imagePath/img_hiclipart_com_3.png';

  static String imgHiclipartCom4 = '$imagePath/img_hiclipart_com_4.png';

  static String imgKisspngOrganic = '$imagePath/img_kisspng_organic.png';

  static String imgClipartkey2734627 = '$imagePath/img_clipartkey_2734627.png';

  static String img540189GroceryFreeHqImage =
      '$imagePath/img_54018_9_grocery_free_hq_image.png';

  static String imgPngitem5186608 = '$imagePath/img_pngitem_5186608.png';

  static String imgBag = '$imagePath/img_bag.svg';

  // Bundle Product detils images
  static String imgFavorite = '$imagePath/img_favorite.svg';

  // Create My Pack images
  static String imgGroup686 = '$imagePath/img_group_686.svg';

  static String imgArrowLeftPrimary = '$imagePath/img_arrow_left_primary.svg';

  //  Bundle Details Page images
  static String imgRipePapaya3x1 = '$imagePath/img_ripe_papaya_3x_1.png';

  // Empty Cart Page images
  static String imgGroupLime200 = '$imagePath/img_group_lime_200.svg';

  // Checkout images
  static String imgClock = '$imagePath/img_clock.svg';

  static String imgContrast = '$imagePath/img_contrast.svg';

  static String imgGroup3462 = '$imagePath/img_group_3462.svg';

  static String imgGroup3450 = '$imagePath/img_group_3450.svg';

  static String imgCloseBlueA100 = '$imagePath/img_close_blue_a100.svg';

  // Order Successfully images
  static String imgGroup3826 = '$imagePath/img_group_3826.svg';

  // Sorry Order is Failed  images
  static String imgGroup3828 = '$imagePath/img_group_3828.svg';

  // No oreder yet images
  static String imgGroupGray900 = '$imagePath/img_group_gray_900.svg';

  // Category images
  static String imgUser = '$imagePath/img_user.svg';

  static String imgTelevision = '$imagePath/img_television.svg';

  static String imgTelevisionGray900 = '$imagePath/img_television_gray_900.svg';

  static String imgThumbsUp = '$imagePath/img_thumbs_up.svg';

  static String imgThumbsUpGray900 = '$imagePath/img_thumbs_up_gray_900.svg';

  static String imgBagGray900 = '$imagePath/img_bag_gray_900.svg';

  static String imgThumbsUpGray90036x29 =
      '$imagePath/img_thumbs_up_gray_900_36x29.svg';

  static String imgSettings = '$imagePath/img_settings.svg';

  static String imgSettingsGray900 = '$imagePath/img_settings_gray_900.svg';

  static String imgSettingsGray90036x49 =
      '$imagePath/img_settings_gray_900_36x49.svg';

  static String imgBagGray90036x30 = '$imagePath/img_bag_gray_900_36x30.svg';

  static String imgSettingsGray9008x36 =
      '$imagePath/img_settings_gray_900_8x36.svg';

  static String imgNavCategories = '$imagePath/img_nav_categories.svg';

  // Product detils images
  static String imgFavoriteGray900 = '$imagePath/img_favorite_gray_900.svg';

  // Cart page images
  static String imgMinus = '$imagePath/img_minus.svg';

  static String imgPlusPrimary = '$imagePath/img_plus_primary.svg';

  // Favorite list images
  static String img00018000117090A1c106003x80x58 =
      '$imagePath/img_0001800011709_0_a1c1_0600_3x_80x58.png';

  static String imgNavFavouritesPrimary =
      '$imagePath/img_nav_favourites_primary.svg';

  // Empty wishlist Page images
  static String imgGroup3690 = '$imagePath/img_group_3690.svg';

  // Empty Cart images
  static String imgCartGreen500 = '$imagePath/img_cart_green_500.svg';

  // Taxi/Menu images
  static String imgImage = '$imagePath/img_image.png';

  static String imgClockBlack900 = '$imagePath/img_clock_black_900.svg';

  static String imgFavoriteBlack900 = '$imagePath/img_favorite_black_900.svg';

  static String imgTelevisionBlack900 =
      '$imagePath/img_television_black_900.svg';

  static String imgProfile = '$imagePath/img_profile.svg';

  // Taxi/Activities images
  static String imgArrowLeftBlack900 =
      '$imagePath/img_arrow_left_black_900.svg';

  static String imgCar = '$imagePath/img_car.svg';

  static String imgCloseWhiteA700 = '$imagePath/img_close_white_a700.svg';

  static String imgLinkedinAmber700 = '$imagePath/img_linkedin_amber_700.svg';

  static String imgCarAmber700 = '$imagePath/img_car_amber_700.svg';

  // Notifications images
  static String imgThumbsUpWhiteA700 =
      '$imagePath/img_thumbs_up_white_a700.svg';

  static String imgTelevisionWhiteA70050x50 =
      '$imagePath/img_television_white_a700_50x50.svg';

  static String imgTelevision50x50 = '$imagePath/img_television_50x50.svg';

  static String imgCart = '$imagePath/img_cart.svg';

  static String imgTelevision1 = '$imagePath/img_television_1.svg';

  static String imgSettingsWhiteA700 = '$imagePath/img_settings_white_a700.svg';

  // Profile Two images
  static String imgEllipse361 = '$imagePath/img_ellipse_361.png';

  static String imgArrowLeftWhiteA700 =
      '$imagePath/img_arrow_left_white_a700.svg';

  static String imgPlay = '$imagePath/img_play.svg';

  static String imgLock = '$imagePath/img_lock.svg';

  static String imgVectorPrimary = '$imagePath/img_vector_primary.svg';

  static String imgBell = '$imagePath/img_bell.svg';

  static String imgVector573 = '$imagePath/img_vector_573.svg';

  static String imgSearch = '$imagePath/img_search.svg';

  static String imgUserGray900 = '$imagePath/img_user_gray_900.svg';

  static String imgThumbsUpGray90024x22 =
      '$imagePath/img_thumbs_up_gray_900_24x22.svg';

  static String imgNavProfileGreen500 =
      '$imagePath/img_nav_profile_green_500.svg';

  // Delivery Address images
  static String imgPlusWhiteA700 = '$imagePath/img_plus_white_a700.svg';

  static String imgGroup3814 = '$imagePath/img_group_3814.svg';

  static String imgGroup3813 = '$imagePath/img_group_3813.svg';

  static String imgContrastGray70001 =
      '$imagePath/img_contrast_gray_700_01.svg';

  // Order Tracking images
  static String imgVector = '$imagePath/img_vector.svg';

  static String imgUserPrimary = '$imagePath/img_user_primary.svg';

  static String imgLinkedinWhiteA700 = '$imagePath/img_linkedin_white_a700.svg';

  static String imgPlay40x40 = '$imagePath/img_play_40x40.png';

  static String imgGroup3170 = '$imagePath/img_group_3170.svg';

  // My Order Details images
  static String imgGroup3812 = '$imagePath/img_group_3812.svg';

  static String imgClockWhiteA700 = '$imagePath/img_clock_white_a700.svg';

  static String imgGroup3931 = '$imagePath/img_group_3931.svg';

  static String imgGroup3813WhiteA700 =
      '$imagePath/img_group_3813_white_a700.svg';

  // Deliverred Successful images
  static String imgGroup3832Green5001 =
      '$imagePath/img_group_3832_green_50_01.svg';

  // Error Page images
  static String imgImage85 = '$imagePath/img_image_85.png';

  // Payment  Method One images
  static String img48Information = '$imagePath/img_48_information.svg';

  static String imgCirclesHide = '$imagePath/img_circles_hide.svg';

  static String imgUserPrimary31x50 = '$imagePath/img_user_primary_31x50.svg';

  // Onboarding Nine images
  static String imgOontPrimarycontainer =
      '$imagePath/img_oont_primarycontainer.svg';

  // Drive method images
  static String imgContrastAmber700 = '$imagePath/img_contrast_amber_700.svg';

  static String imgContrastGray200 = '$imagePath/img_contrast_gray_200.svg';

  // Bank details images
  static String imgArrowdownGray800 = '$imagePath/img_arrowdown_gray_800.svg';

  // Onboarding Eight images
  static String imgOontGreen500 = '$imagePath/img_oont_green_500.svg';

  // Onboarding-white images
  static String imgOontPrimary85x288 = '$imagePath/img_oont_primary_85x288.svg';

  // Onboarding Seven images
  static String img100Amber700 = '$imagePath/img_100_amber_700.svg';

  // Log In Two images
  static String imgGoogle = '$imagePath/img_google.svg';

  static String imgUserWhiteA700 = '$imagePath/img_user_white_a700.svg';

  // Login or signup Six images
  static String imgGroup3953 = '$imagePath/img_group_3953.svg';

  static String imgGoogleRed600 = '$imagePath/img_google_red_600.svg';

  static String imgTrash = '$imagePath/img_trash.svg';

  static String imgFacebook = '$imagePath/img_facebook.svg';

  // Verification Successful One images
  static String imgGroup3832Gray30001 =
      '$imagePath/img_group_3832_gray_300_01.svg';

  // Common images
  static String imgImage38 = '$imagePath/img_image_38.png';

  static String imgGroup57 = '$imagePath/img_group_57.png';

  static String img100 = '$imagePath/img_100.svg';

  static String img250 = '$imagePath/img_25_0.svg';

  static String imgItemsOutline = '$imagePath/img_items_outline.svg';

  static String img750 = '$imagePath/img_75_0.svg';

  static String img625 = '$imagePath/img_62_5.svg';

  static String img500 = '$imagePath/img_50_0.svg';

  static String imgGroup297 = '$imagePath/img_group_297.svg';

  static String imgGroup = '$imagePath/img_group.svg';

  static String imgGroupOrange100 = '$imagePath/img_group_orange_100.svg';

  static String img750Primary = '$imagePath/img_75_0_primary.svg';

  static String img625Primary = '$imagePath/img_62_5_primary.svg';

  static String img500Primary = '$imagePath/img_50_0_primary.svg';

  static String imgGroupGray30001 = '$imagePath/img_group_gray_300_01.svg';

  static String imgLoginOrSignup = '$imagePath/img_login_or_signup.png';

  static String imgRectangle428 = '$imagePath/img_rectangle_428.svg';

  static String imgRectangle2253 = '$imagePath/img_rectangle_2253.svg';

  static String img43x = '$imagePath/img_4_3x.png';

  static String img83x = '$imagePath/img_8_3x.png';

  static String img5973x = '$imagePath/img_597_3x.png';

  static String img537376GrainP = '$imagePath/img_53737_6_grain_p.png';

  static String imgAvocado3x = '$imagePath/img_avocado_3x.png';

  static String imgFavpngFarmers = '$imagePath/img_favpng_farmers.png';

  static String imgOontGray50 = '$imagePath/img_oont_gray_50.svg';

  static String imgEye = '$imagePath/img_eye.svg';

  static String imgArrowleft = '$imagePath/img_arrowleft.svg';

  static String imgArrowLeftGray900 = '$imagePath/img_arrow_left_gray_900.svg';

  static String imgFriendsgiving13x = '$imagePath/img_friendsgiving_1_3x.png';

  static String imgClose = '$imagePath/img_close.svg';

  static String imgEa73c67081b84 = '$imagePath/img_ea73c670_81b8_4.png';

  static String imgILoveSushi3x = '$imagePath/img_i_love_sushi_3x.png';

  static String img18tfmbs140Inli = '$imagePath/img_18tfmbs140_inli.png';

  static String imgDrypersClassic = '$imagePath/img_drypers_classic.png';

  static String imgFilter13x1 = '$imagePath/img_filter_1_3x_1.png';

  static String img00018000117090A1c106003x =
      '$imagePath/img_0001800011709_0_a1c1_0600_3x.png';

  static String imgHiclipartCom6 = '$imagePath/img_hiclipart_com_6.png';

  static String imgNicepngGrocery = '$imagePath/img_nicepng_grocery.png';

  static String imgImage5 = '$imagePath/img_image_5.png';

  static String imgMinus1 = '$imagePath/img_minus_1.svg';

  static String imgPlus3 = '$imagePath/img_plus_3.svg';

  static String imgGroup3473 = '$imagePath/img_group_3473.png';

  static String imgGroup3474 = '$imagePath/img_group_3474.png';

  static String imgGroup3475 = '$imagePath/img_group_3475.png';

  static String imgGroup3476 = '$imagePath/img_group_3476.png';

  static String imgGroup3478 = '$imagePath/img_group_3478.png';

  static String imgStroke165 = '$imagePath/img_stroke_165.svg';

  static String imgSearchGray900 = '$imagePath/img_search_gray_900.svg';

  static String imgPurepng = '$imagePath/img_purepng.png';

  static String img71s6oqqva5l1 = '$imagePath/img_71s6oqqva5l_1.png';

  static String imgUnnamed3x173x74 = '$imagePath/img_unnamed_3x_1_73x74.png';

  static String imgBottleGourd13x = '$imagePath/img_bottle_gourd_1_3x.png';

  static String imgSearchGray90024x24 =
      '$imagePath/img_search_gray_900_24x24.svg';

  static String imgGroup702 = '$imagePath/img_group_702.svg';

  static String imgNavHome = '$imagePath/img_nav_home.svg';

  static String imgNavFavourites = '$imagePath/img_nav_favourites.svg';

  static String imgNavProfile = '$imagePath/img_nav_profile.svg';

  static String imgTelevisionWhiteA700 =
      '$imagePath/img_television_white_a700.svg';

  static String imgRewind = '$imagePath/img_rewind.svg';

  static String imgTelevisionWhiteA70024x24 =
      '$imagePath/img_television_white_a700_24x24.svg';

  static String imgTopLeft = '$imagePath/img_top_left.svg';

  static String imgArrowRight = '$imagePath/img_arrow_right.svg';

  static String imgArrowDown = '$imagePath/img_arrow_down.svg';

  static String imgSulphurFreeDo = '$imagePath/img_sulphur_free_do.png';

  static String img41d0da986d1fKi = '$imagePath/img_41d0da986d1f_ki.png';

  static String imgTelevisionGray90040x40 =
      '$imagePath/img_television_gray_900_40x40.svg';

  static String imgLinkedinOnprimary = '$imagePath/img_linkedin_onprimary.svg';

  static String imgArrowDownGreen500 =
      '$imagePath/img_arrow_down_green_500.svg';

  static String imgPlus = '$imagePath/img_plus.svg';

  static String img6804425Biscuit = '$imagePath/img_6804425_biscuit.png';

  static String imgShapesMultip = '$imagePath/img_shapes_multip.png';

  static String imgNachoCheese144013x =
      '$imagePath/img_nacho_cheese_1440_1_3x.png';

  static String imgLauGroud3x1 = '$imagePath/img_lau_groud_3x_1.png';

  static String img0002197Pointed = '$imagePath/img_0002197_pointed.png';

  static String imgPetersOriginal = '$imagePath/img_peters_original.png';

  static String imgUnnamed13x1 = '$imagePath/img_unnamed_1_3x_1.png';

  static String imgNavHomePrimary = '$imagePath/img_nav_home_primary.svg';

  static String imgNavCategoriesGray900 =
      '$imagePath/img_nav_categories_gray_900.svg';

  static String imgThumbsUpGray90024x24 =
      '$imagePath/img_thumbs_up_gray_900_24x24.svg';

  static String imgMobile = '$imagePath/img_mobile.svg';

  static String imgPlayPrimary = '$imagePath/img_play_primary.svg';

  static String imgCloseRedA400 = '$imagePath/img_close_red_a400.svg';

  static String imgMobileGray10003 = '$imagePath/img_mobile_gray_100_03.svg';

  static String imgContrastPrimary = '$imagePath/img_contrast_primary.svg';

  static String imgArrowLeftAmber700 =
      '$imagePath/img_arrow_left_amber_700.svg';

  static String img250Amber700 = '$imagePath/img_25_0_amber_700.svg';

  static String imgItemsOutlineAmber700 =
      '$imagePath/img_items_outline_amber_700.svg';

  static String imgGroup317 = '$imagePath/img_group_317.svg';

  static String img750Amber700 = '$imagePath/img_75_0_amber_700.svg';

  static String img625Amber700 = '$imagePath/img_62_5_amber_700.svg';

  static String img500Amber700 = '$imagePath/img_50_0_amber_700.svg';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
