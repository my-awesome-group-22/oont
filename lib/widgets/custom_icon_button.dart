import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';

class CustomIconButton extends StatelessWidget {
  CustomIconButton({
    Key? key,
    this.alignment,
    this.height,
    this.width,
    this.padding,
    this.decoration,
    this.child,
    this.onTap,
  }) : super(
          key: key,
        );

  final Alignment? alignment;

  final double? height;

  final double? width;

  final EdgeInsetsGeometry? padding;

  final BoxDecoration? decoration;

  final Widget? child;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return alignment != null
        ? Align(
            alignment: alignment ?? Alignment.center,
            child: iconButtonWidget,
          )
        : iconButtonWidget;
  }

  Widget get iconButtonWidget => SizedBox(
        height: height ?? 0,
        width: width ?? 0,
        child: IconButton(
          padding: EdgeInsets.zero,
          icon: Container(
            height: height ?? 0,
            width: width ?? 0,
            padding: padding ?? EdgeInsets.zero,
            decoration: decoration ??
                BoxDecoration(
                  color: theme.colorScheme.primary,
                  borderRadius: BorderRadius.circular(14.h),
                ),
            child: child,
          ),
          onPressed: onTap,
        ),
      );
}

/// Extension on [CustomIconButton] to facilitate inclusion of all types of border style etc
extension IconButtonStyleHelper on CustomIconButton {
  static BoxDecoration get fillWhiteA => BoxDecoration(
        color: appTheme.whiteA700,
        borderRadius: BorderRadius.circular(24.h),
      );
  static BoxDecoration get outlineBlueGray => BoxDecoration(
        borderRadius: BorderRadius.circular(8.h),
        border: Border.all(
          color: appTheme.blueGray10001,
          width: 1.h,
        ),
      );
  static BoxDecoration get fillGray => BoxDecoration(
        color: appTheme.gray100,
        borderRadius: BorderRadius.circular(8.h),
      );
  static BoxDecoration get outlineGray => BoxDecoration(
        borderRadius: BorderRadius.circular(16.h),
        border: Border.all(
          color: appTheme.gray20001,
          width: 1.h,
        ),
      );
  static BoxDecoration get fillGrayTL20 => BoxDecoration(
        color: appTheme.gray10002,
        borderRadius: BorderRadius.circular(20.h),
      );
  static BoxDecoration get outlineGrayTL10 => BoxDecoration(
        borderRadius: BorderRadius.circular(10.h),
        border: Border.all(
          color: appTheme.gray20001,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineBlack => BoxDecoration(
        borderRadius: BorderRadius.circular(20.h),
        border: Border.all(
          color: appTheme.black900.withOpacity(0.05),
          width: 1.h,
        ),
      );
  static BoxDecoration get fillBlueA => BoxDecoration(
        color: appTheme.blueA40001,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillAmber => BoxDecoration(
        color: appTheme.amber50001,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillTeal => BoxDecoration(
        color: appTheme.teal400,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillRed => BoxDecoration(
        color: appTheme.red60001,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillBlueATL25 => BoxDecoration(
        color: appTheme.blueA400,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillPink => BoxDecoration(
        color: appTheme.pink500,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get fillWhiteATL12 => BoxDecoration(
        color: appTheme.whiteA700,
        borderRadius: BorderRadius.circular(12.h),
      );
  static BoxDecoration get fillPrimaryTL9 => BoxDecoration(
        color: theme.colorScheme.primary,
        borderRadius: BorderRadius.circular(9.h),
      );
  static BoxDecoration get fillGrayTL25 => BoxDecoration(
        color: appTheme.gray900,
        borderRadius: BorderRadius.circular(25.h),
      );
  static BoxDecoration get outlineBlueGrayTL30 => BoxDecoration(
        borderRadius: BorderRadius.circular(30.h),
        border: Border.all(
          color: appTheme.blueGray900,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineRed => BoxDecoration(
        borderRadius: BorderRadius.circular(30.h),
        border: Border.all(
          color: appTheme.red600,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineBlue => BoxDecoration(
        borderRadius: BorderRadius.circular(30.h),
        border: Border.all(
          color: appTheme.blue300,
          width: 1.h,
        ),
      );
  static BoxDecoration get outlineErrorContainer => BoxDecoration(
        borderRadius: BorderRadius.circular(30.h),
        border: Border.all(
          color: theme.colorScheme.errorContainer,
          width: 1.h,
        ),
      );
}
