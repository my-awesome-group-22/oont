import 'package:flutter/material.dart';
import 'package:oont/core/app_export.dart';
import 'package:oont/widgets/custom_text_form_field.dart';

// ignore: must_be_immutable
class AppbarTitleEdittext extends StatelessWidget {
  AppbarTitleEdittext({
    Key? key,
    this.hintText,
    this.controller,
    this.margin,
  }) : super(
          key: key,
        );

  String? hintText;

  TextEditingController? controller;

  EdgeInsetsGeometry? margin;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: CustomTextFormField(
        width: 329.h,
        controller: controller,
        hintText: "lbl_search".tr,
        hintStyle: CustomTextStyles.bodyLargeGray900_2,
        prefix: Container(
          margin: EdgeInsets.fromLTRB(15.h, 13.v, 10.h, 13.v),
          child: CustomImageView(
            imagePath: ImageConstant.imgRewind,
            height: 24.adaptSize,
            width: 24.adaptSize,
          ),
        ),
        prefixConstraints: BoxConstraints(
          maxHeight: 50.v,
        ),
        suffix: Container(
          margin: EdgeInsets.fromLTRB(30.h, 13.v, 13.h, 13.v),
          child: CustomImageView(
            imagePath: ImageConstant.imgTelevisionWhiteA70024x24,
            height: 24.adaptSize,
            width: 24.adaptSize,
          ),
        ),
        suffixConstraints: BoxConstraints(
          maxHeight: 50.v,
        ),
        borderDecoration: TextFormFieldStyleHelper.fillGray,
        filled: true,
        fillColor: appTheme.gray100,
      ),
    );
  }
}
